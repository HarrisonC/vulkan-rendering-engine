#include "ModelManager.h"
#include "Model.h"
#include "AnimatedModel.h"

ModelManager::ModelManager()
{
}

ModelManager::~ModelManager()
{
}

//-------------------------------------------------------------------------------------------------
void ModelManager::LoadModels()
{
	m_models.reserve(10);
	m_models.emplace("Assets/Models/sphere.obj",		std::make_unique<StaticModel>( "Assets/Models/sphere.obj" ));
	m_models.emplace("Assets/Models/cube.obj",			std::make_unique<StaticModel>( "Assets/Models/cube.obj" ));
	m_models.emplace("Assets/Models/cowboy.dae",		std::make_unique<AnimatedModel>("Assets/Models/cowboy.dae"));
	m_models.emplace("Assets/Models/Pilot/Pilot_LP_Animated.fbx", std::make_unique<AnimatedModel>("Assets/Models/Pilot/Pilot_LP_Animated.fbx"));
	//m_models.emplace("Assets/Models/Stormtrooper/Stormtrooper.fbx",		std::make_unique<AnimatedModel>("Assets/Models/Stormtrooper/Stormtrooper.fbx"));
	//m_models.emplace("Assets/Models/Doom/doomxxtt.fbx",		std::make_unique<AnimatedModel>("Assets/Models/Doom/doomxxtt.fbx"));
	//m_models.emplace("Plane",							StaticModel{ PlaneGenerator{ 100, 100, 8 } });
	//m_models.emplace("Assets/Models/chalet.obj",		Model{ "Assets/Models/chalet.obj" });	//Load time is massive so commenting out until needeed (hopefully never!)
}

//-------------------------------------------------------------------------------------------------
const Model& ModelManager::GetModel(const std::string& modelPath) const
{
	return *m_models.at(modelPath);
}

//-------------------------------------------------------------------------------------------------
void ModelManager::CleanUp()
{
	for (auto& model : m_models)
	{
		model.second->CleanUp();
	}
}