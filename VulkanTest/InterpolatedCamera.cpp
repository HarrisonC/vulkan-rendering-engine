#include "InterpolatedCamera.h"

#include "ImGui/imgui_helper_functions.h"
#include "ImGui/imgui.h"

#include <algorithm>

namespace
{
	//obv move this to a utility space at some point
	float lerp(const float a, const float b, const float t)
	{
		return a + t * (b - a);
	}

	glm::vec3 lerp(const glm::vec3& a, const glm::vec3 b, const float t)
	{
		return { lerp(a.x, b.x, t), lerp(a.y, b.y, t), lerp(a.z, b.z, t) };
	}
}

InterpolatedCamera::InterpolatedCamera(const bool loopWhenFinished) :
	Camera(),
	m_keyFrames(),
	m_currentFrameIdx(-1),
	m_currentLerpTime(0.0f),
	m_loopWhenFinished(loopWhenFinished)
{
}

InterpolatedCamera::~InterpolatedCamera()
{
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::Update(const double dt)
{
	if (m_currentFrameIdx != -1 && m_keyFrames.empty() == false)
	{
		m_currentLerpTime += static_cast<float>(dt);
		const InterpolateKeyFrame& currentFrame = m_keyFrames[m_currentFrameIdx];
		if (m_currentLerpTime > currentFrame.length)
		{
			GoToNextFrame();
		}
		else
		{
			m_position = lerp(currentFrame.startPosition, currentFrame.endPosition, m_currentLerpTime / currentFrame.length);

			if (m_hasFocusPoint == false)
			{
				m_rotation = lerp(currentFrame.startRotation, currentFrame.endRotation, m_currentLerpTime / currentFrame.length);
				UpdateFront();
			}
			else
			{
				LookAtPoint(m_focusPoint);
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::GoToNextFrame()
{
	m_currentLerpTime = 0.0f;

	//If we've reached the end then stop
	if (++m_currentFrameIdx >= m_keyFrames.size())
	{
		//Unless we're looping then set back to start (0)
		m_currentFrameIdx = m_loopWhenFinished ? 0 : -1;
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::AddToImGui()
{
	auto CreateEntry = [](const char* txt, glm::vec3& data) -> bool
	{
		ImGui::Text(txt);
		ImGui::NextColumn();
		const bool hasChanged = ImGuiHelper::CreateSameLineBoxForVector(txt, data);
		ImGui::NextColumn();
		ImGui::Separator();

		return hasChanged;
	};

	auto AddKeyFrameToImGui = [&](InterpolateKeyFrame& frame)
	{
		ImGui::Columns(2);
		ImGui::Separator();

		CreateEntry("Start Position",	frame.startPosition);
		CreateEntry("End Position",		frame.endPosition);
		CreateEntry("Start Rotation",	frame.startRotation);
		CreateEntry("End Rotation",		frame.endRotation);

		ImGui::Text("Frame Length");
		ImGui::NextColumn();
		ImGui::InputFloat("##flength", &frame.length);
		ImGui::NextColumn();
		ImGui::Separator();

		ImGui::Columns(1);
	};


	Camera::AddToImGui();

	ImGui::Columns(2);

	ImGui::Text("Current Frame Index");
	ImGui::NextColumn();
	if (ImGui::InputInt("##frameindex", &m_currentFrameIdx))
	{
		m_currentFrameIdx = std::clamp(m_currentFrameIdx, -1, (int)(m_keyFrames.size() - 1));
	}
	ImGui::NextColumn();
	ImGui::Separator();

	ImGui::Text("Current Lerp Time");
	ImGui::NextColumn();
	ImGui::Text(std::to_string(m_currentLerpTime).c_str());
	ImGui::NextColumn();
	ImGui::Separator();

	ImGui::Text("Loop When Finished");
	ImGui::NextColumn();
	ImGui::Checkbox("##islooping", &m_loopWhenFinished);
	ImGui::NextColumn();
	ImGui::Separator();

	ImGui::Columns(1);

	if (m_keyFrames.empty() == false)
	{
		if (ImGui::TreeNode("KeyFrames"))
		{
			for (int i = 0; i < m_keyFrames.size(); ++i)
			{
				const std::string id = "Key Frame " + std::to_string(i);
				if (ImGui::TreeNode(id.c_str()))
				{
					AddKeyFrameToImGui(m_keyFrames[i]);
					ImGui::TreePop();
				}
			}

			ImGui::TreePop();
		}
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::AddKeyFrame(InterpolateKeyFrame&& frame)
{
	m_keyFrames.push_back(std::move(frame));
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::StartInterpolation()
{
	m_currentFrameIdx = 0;
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::StopInterpolation()
{
	m_currentFrameIdx = -1;
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::ChangePosition(const glm::vec3& delta)
{
	if (IsInterpolating() == false)
	{
		Camera::ChangePosition(delta);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::SetPosition(const glm::vec3& new_pos)
{
	if (IsInterpolating() == false)
	{
		Camera::SetPosition(new_pos);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::AddYaw(const float delta)
{
	if (IsInterpolating() == false)
	{
		Camera::AddYaw(delta);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::AddPitch(const float delta)
{
	if (IsInterpolating() == false)
	{
		Camera::AddPitch(delta);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::AddRoll(const float delta)
{
	if (IsInterpolating() == false)
	{
		Camera::AddRoll(delta);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::SetYaw(const float newYaw)
{
	if (IsInterpolating() == false)
	{
		Camera::SetYaw(newYaw);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::SetPitch(const float newPitch)
{
	if (IsInterpolating() == false)
	{
		Camera::SetPitch(newPitch);
	}
}

//-------------------------------------------------------------------------------------------------
void InterpolatedCamera::SetRoll(const float newRoll)
{
	if (IsInterpolating() == false)
	{
		Camera::SetRoll(newRoll);
	}
}