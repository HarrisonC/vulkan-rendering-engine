#include "VulkanCore.h"
#include <iostream>
#include <string>

int main()
{
	VulkanCore::Singleton().Initialise();
	VulkanCore::Singleton().Update();
	VulkanCore::Singleton().DeleteSingleton();

	return 0;
}