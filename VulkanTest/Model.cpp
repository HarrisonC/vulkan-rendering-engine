#include "Model.h"
#include "VulkanCore.h"
#include "Vertex.h"

#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

Model::Model(const std::string& filepath) :
	m_device(VulkanCore::Singleton().Device()),
	m_vertexBuffer(VulkanCore::Singleton().Device()),
	m_indicesBuffer(VulkanCore::Singleton().Device()),
	m_filepath(filepath)
{
}

Model::Model(std::vector<uint32_t>&& indicies) :
	m_device(VulkanCore::Singleton().Device()),
	m_vertexBuffer(VulkanCore::Singleton().Device()),
	m_indicesBuffer(VulkanCore::Singleton().Device()),
	m_filepath("Manually Created Model"),
	m_indices(std::move(indicies))
{
}

Model::Model(Model&& rhs) :
	m_device(rhs.m_device),
	m_dimensions(rhs.m_dimensions),
	m_filepath(rhs.m_filepath),
	m_indices(std::move(rhs.m_indices)),
	m_vertexBuffer(std::move(rhs.m_vertexBuffer)),
	m_indicesBuffer(std::move(rhs.m_indicesBuffer))
{
}

Model::~Model()
{
	CleanUp();
}

//-------------------------------------------------------------------------------------------------
void Model::Draw(const VkDescriptorSet& descriptorSet, VkCommandBuffer& buffer, const VkPipelineLayout& pipelineLayout) const
{
	VkBuffer vBuffers[] = { m_vertexBuffer.m_buffer };
	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers(buffer, 0, 1, vBuffers, offsets);
	vkCmdBindIndexBuffer(buffer, m_indicesBuffer.m_buffer, 0, VK_INDEX_TYPE_UINT32);
	vkCmdBindDescriptorSets(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSet, 0, nullptr);
	vkCmdDrawIndexed(buffer, (uint32_t)m_indices.size(), 1, 0, 0, 0);
}

//-------------------------------------------------------------------------------------------------
bool Model::LoadModel(const std::string& path)
{
	//Load in the file - Flips the UV's and triangulate the verts
	Assimp::Importer importer;
	//Other convenient post-processing : genNormals, SplitLargeMeshes, OptimizeMeshes
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		printf("FAILED TO LOAD MODEL : %s!", path.c_str());
		return false;
	}

	ProcessNode(*scene->mRootNode, *scene);

	return true;
}

//-------------------------------------------------------------------------------------------------
void Model::ProcessNode(const aiNode& node, const aiScene& scene)
{
	//Loop through all of this nodes meshes
	for (unsigned int curMesh = 0; curMesh < node.mNumMeshes; curMesh++)
	{
		aiMesh* mesh = scene.mMeshes[node.mMeshes[curMesh]];
		ProcessMesh(*mesh, scene);
	}

	//Loop through all of this nodes children
	for (unsigned int child = 0; child < node.mNumChildren; child++)
	{
		ProcessNode(*node.mChildren[child], scene);
	}
}

//-------------------------------------------------------------------------------------------------
void StaticModel::CreateVertexBuffer()
{
	VulkanUtils::Buffer	stagingBuffer(m_device);
	const VkDeviceSize	bufferSize = m_vertices.size() * sizeof(m_vertices[0]);

	//Create staging buffer
	VulkanCore::Singleton().CreateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, stagingBuffer.m_buffer, stagingBuffer.m_memory);

	//Map data to staging buffer
	void* data;
	vkMapMemory(m_device, stagingBuffer.m_memory, 0, bufferSize, 0, &data);
	memcpy(data, m_vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(m_device, stagingBuffer.m_memory);

	//Create vertex buffer
	VulkanCore::Singleton().CreateBuffer(bufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_vertexBuffer.m_buffer, m_vertexBuffer.m_memory);

	//Copy the staging buffer to the local vertex buffer
	VulkanCore::Singleton().CopyBuffer(stagingBuffer.m_buffer, m_vertexBuffer.m_buffer, bufferSize);
}

//-------------------------------------------------------------------------------------------------
void Model::CreateIndexBuffer()
{
	VkDevice& device = VulkanCore::Singleton().Device();

	VulkanUtils::Buffer	stagingBuffer(device);
	const VkDeviceSize	bufferSize = m_indices.size() * sizeof(m_indices[0]);

	//Create staging buffer
	VulkanCore::Singleton().CreateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, stagingBuffer.m_buffer, stagingBuffer.m_memory);

	//Map data to staging buffer
	void* data;
	vkMapMemory(device, stagingBuffer.m_memory, 0, bufferSize, 0, &data);
	memcpy(data, m_indices.data(), (size_t)bufferSize);
	vkUnmapMemory(device, stagingBuffer.m_memory);

	//Create index buffer
	VulkanCore::Singleton().CreateBuffer(bufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_indicesBuffer.m_buffer, m_indicesBuffer.m_memory);

	//Copy the staging buffer to the local index buffer
	VulkanCore::Singleton().CopyBuffer(stagingBuffer.m_buffer, m_indicesBuffer.m_buffer, bufferSize);
}

//-------------------------------------------------------------------------------------------------
void Model::CleanUp()
{
	m_indicesBuffer.CleanUp();
	m_vertexBuffer.CleanUp();
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
StaticModel::StaticModel(const std::string& filepath) :
	Model(filepath)
{
	LoadModel(filepath);

	CreateVertexBuffer();
	CreateIndexBuffer();
}

//-------------------------------------------------------------------------------------------------
StaticModel::StaticModel(std::vector<Vertex>&& verts, std::vector<uint32_t>&& indicies) :
	Model(std::move(indicies)),
	m_vertices(std::move(verts))
{
	CreateVertexBuffer();
	CreateIndexBuffer();
}

//-------------------------------------------------------------------------------------------------
void StaticModel::ProcessMesh(const aiMesh& mesh, const aiScene& scene)
{
	glm::vec3 smallest{ std::numeric_limits<float>::max() };
	glm::vec3 largest{ -std::numeric_limits<float>::max() };

	//Process all the vertices
	for (unsigned int v = 0; v < mesh.mNumVertices; v++)
	{
		Vertex vert{};

		vert.m_pos = glm::vec3(mesh.mVertices[v].x, mesh.mVertices[v].y, mesh.mVertices[v].z);
		vert.m_normal = glm::normalize(glm::vec3(mesh.mNormals[v].x, mesh.mNormals[v].y, mesh.mNormals[v].z));
		for (int i = 0; i < 3; ++i)
		{
			if (vert.m_pos[i] < smallest[i])
				smallest[i] = vert.m_pos[i];

			if (vert.m_pos[i] > largest[i])
				largest[i] = vert.m_pos[i];
		}

		vert.m_col = { 0.0f, 0.0f, 1.0f };

		if (mesh.mTextureCoords[0])
			vert.m_texCoords = glm::vec2(mesh.mTextureCoords[0][v].x, mesh.mTextureCoords[0][v].y);
		else
			vert.m_texCoords = glm::vec2(0.0f, 0.0f);

		m_vertices.push_back(std::move(vert));
	}

	//Process all the indices
	for (unsigned int i = 0; i < mesh.mNumFaces; i++)
	{
		aiFace face = mesh.mFaces[i];
		for (unsigned int ind = 0; ind < face.mNumIndices; ind++)
			m_indices.push_back(face.mIndices[ind]);
	}

	m_dimensions = largest - smallest;
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
PlaneGenerator::PlaneGenerator(const uint32_t width, const uint32_t height, const uint32_t numOfTris) :
	ShapeGenerator(),
	m_width(width),
	m_height(height),
	m_numOfTris(numOfTris)
{
}

glm::vec2 CalcBestQuadSetup(const uint32_t numOfTris)
{
	glm::vec2 smallestDiff{ 1.0f };
	
	if (numOfTris == 2 || numOfTris % 2 != 0)	//We have to have at atleast 2!
	{
		return smallestDiff;
	}

	const uint32_t numOfQuads = numOfTris / 2;	//This is divide by two as a 2 tris = 1 quad
	const uint32_t threshold = numOfQuads / 2;

	smallestDiff = { numOfQuads - threshold, 2.0f };

	for (uint32_t i = 3; i < threshold; ++i)
	{
		const uint32_t diff = numOfQuads / i;
		if (numOfQuads % i == 0 && diff < smallestDiff.x)
		{
			smallestDiff = { diff, i };
		}
	}

	return smallestDiff;
}

void PlaneGenerator::GenerateShape(std::vector<Vertex>& verts, std::vector<uint32_t>& indices) const
{
	//Build Vertices - In Sequence of Quads
	glm::vec2 quadsPerAxis = CalcBestQuadSetup(m_numOfTris);
	
	float currentX = 0.0f;
	float currentY = 0.0f;

	const float xStep = m_width / quadsPerAxis.x;
	const float yStep = m_height / quadsPerAxis.y;

	while (currentY <= m_height)
	{
		while (currentX <= m_width)
		{
			const float u = currentX == 0 ? 0.0f : currentX / m_width;
			const float v = currentY == 0 ? 1.0f : 1.0f - (currentY / m_height);
	
			Vertex currentVert{};
			currentVert.m_pos = { currentX, currentY, 0.0f };
			currentVert.m_texCoords = { u, v };
					   
			currentVert.m_col = { 1.0f, 1.0f, 1.0f };
			currentVert.m_normal = { 0.0f, 1.0f, 0.0f };
	
			verts.push_back(std::move(currentVert));
	
			currentX += xStep;
		}
	
		currentY += yStep;
		currentX = 0;
	}

	//Build indicies
	const uint32_t xOffset = (uint32_t)quadsPerAxis.x - 1;
	uint32_t bl = 0;	//Index for the bottom left of a quad

	//Build a quad using bottom left (bl) index
	for (uint32_t currentQuad = 1; currentQuad <= (m_numOfTris / 2); bl++, currentQuad++)
	{
		//We reach the extreme right side, the last vertex is not a bottom left of anything
		//Therefore we move it on BUT we didn't complete a quad, so don't progress then
		if ((bl + 1) % (uint32_t)(quadsPerAxis.x + 1) == 0)
			bl++;

		//Verts are in built in this sequence (index in brackets)
		//BL(0) -> BR(1) -> TL(2) -> TR(3)

		//Build the indices in this sequence
		//Tri 1 - BL -> TL -> TR
		//Tri 2 - BR -> BL -> TR
		indices.push_back(bl);
		indices.push_back(bl + 2 + xOffset);
		indices.push_back(bl + 3 + xOffset);
	
		indices.push_back(bl + 3 + xOffset);
		indices.push_back(bl + 1);
		indices.push_back(bl);
	}
}