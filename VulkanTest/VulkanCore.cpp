#include "VulkanCore.h"

#include <vulkan\vk_sdk_platform.h>

#ifdef __ANDROID__
#include <vulkan\vulkan_android.h>
#elif defined(_WIN32)
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3native.h>
#include <vulkan/vulkan_win32.h>
#elif defined(VK_USE_PLATFORM_IOS_MVK)
#include <vulkan\vulkan_ios.h>
#elif defined(VK_USE_PLATFORM_MACOS_MVK)
#include <vulkan\vulkan_macos.h>
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
#include <vulkan\vulkan_wayland.h>
#else
m_extensionNames.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#endif

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>
#include <chrono>

#include <glm\common.hpp>
#include <glm\gtx\transform.hpp>

#include "math.h"
#include "Vertex.h"
#include "SceneNode.h"
#include "Texture.h"
#include "RenderObject.h"
#include "CubeMap.h"
#include "DearImgui.h"
#include "ImGui/imgui.h"
#include "LightManager.h"
#include "Ray.h"
#include "Terrain.h"

VulkanCore* VulkanCore::m_singleton = nullptr;

namespace
{
	VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger)
	{
		auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
		if (func != nullptr)
		{
			return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
		}
		else
		{
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator)
	{
		auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
		if (func != nullptr)
		{
			func(instance, debugMessenger, pAllocator);
		}
	}

	QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice& device, VkSurfaceKHR surface)
	{
		QueueFamilyIndices indices;
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

		int i = 0;
		for (const auto& queueFamily : queueFamilies)
		{
			if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			{
				indices.gfxFamily = i;
			}

			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);

			if (presentSupport)
			{
				indices.presentFamily = i;
			}

			if (indices.isComplete())
				break;

			++i;
		}

		return indices;
	}

	uint32_t FindMemoryTypeFromReqs(VkPhysicalDevice& physicalDevice, uint32_t typeFilter, VkMemoryPropertyFlags properties)
	{
		VkPhysicalDeviceMemoryProperties memProps;
		vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProps);

		for (uint32_t i = 0; i < memProps.memoryTypeCount; ++i)
		{
			if (typeFilter & (1 << i) && (memProps.memoryTypes[i].propertyFlags & properties) == properties)
				return i;
		}

		printf("FATAL ERROR - FAILED TO FIND SUITABLE MEM TYPE! \n");
		return UINT32_MAX;
	}

	VkFormat FindSupportedFormat(VkPhysicalDevice& physicalDevice, const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
	{
		for (const auto& format : candidates)
		{
			VkFormatProperties props;
			vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);

			const bool matchedLinear = tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features;
			const bool matchedOptimal = tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features;

			if (matchedLinear | matchedOptimal)
			{
				return format;
			}
		}

		printf("FAILED TO FIND A SUITABLE FORMAT FOR DEVICE - PREPARE FOR SOME FUNKINESS! \n");
		return VK_FORMAT_R32G32B32A32_SFLOAT;
	}

}

VulkanCore::VulkanCore() :
	m_camera(false)
{
	m_window = nullptr;

	//----- VULKAN MEMBER VARIABLES -----
	m_instance		= nullptr;
	m_device		= nullptr;
	m_commandPool	= NULL;
	//m_commandBuffer = nullptr;
	m_surface		= NULL;

	//----- Device(s) -----
	m_deviceMemProps = {};
	m_familyQueueIndex  = std::numeric_limits<uint32_t>::max();

	//----- Queue(s) -----
	m_gfxQueue = nullptr;
	m_presentQueue = nullptr;

	//----- Debug/Validation Layers -----
	m_debugMessenger = NULL;

	//----- Swapchain -----
	m_swapchain = NULL;
	m_swapchainCurrentBuffer = std::numeric_limits<uint32_t>::max();
	m_swapchainImageCount	 = std::numeric_limits<uint32_t>::max();
	m_swapchainFormat = {};

	//----- Depth Buffer -----
	m_depthBufferImage = NULL;
	m_depthBufferView = NULL;
	m_depthMemoryReq = {};
	m_depthMemory = NULL;

	//----- Render Pass -----
	m_renderPass = NULL;

	//----- Shader -----
	m_staticShader = nullptr;
	m_animatedShader = nullptr;

	//----- Gfx Pipeline -----
	m_staticPipeline = VK_NULL_HANDLE;
	m_animatedPipeline = VK_NULL_HANDLE;

	//----- Descriptor/Pipeline Layout -----
	m_staticPipelineLayout = VK_NULL_HANDLE;
	m_animatedPipelineLayout = VK_NULL_HANDLE;

	m_swapchainExtents = { m_windowWidth, m_windowHeight };
}

VulkanCore::~VulkanCore()
{
	CleanUp();
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::Initialise()
{
	if (InitGLFW() == false)
	{
		printf("FATAL ERROR - FAILED TO INITIALISE GLFW WINDOW!");
	}

	if (InitInstance() == false)
	{
		printf("FATAL ERROR - FAILED TO INITIALISE VULKAN!");
		return false;
	}
	if (CreateSurface() == false)
	{
		return false;
	}

	SetupDebugMessenger();
	EnumerateDevices();
	
	if (CreateLogicalDevice() == false)
	{
		return false;
	}

	if (CreateSwapchain() == false)
	{
		return false;
	}
	if (CreateDepthResources() == false)
	{
		return false;
	}
	if (CreateRenderPass() == false)
	{
		return false;
	}

	m_camera.SetPosition({ -44.0f, -28.0f, 0.0f });
	//m_camera.AddYaw(209.0f);
	//m_camera.AddPitch(32.0f);

	InterpolatedCamera::InterpolateKeyFrame keyFrame1
	{
		{ -44.0f, -28.0f, 0.0f },
		{ 0.0f, -28.0f, -44.0f },
		{ 170.0f, 32.0f, 0.0f },
		{ 230.0f, 32.0f, 0.0f },
		1.0f
	};

	InterpolatedCamera::InterpolateKeyFrame keyFrame2
	{
		keyFrame1.endPosition,
		{ 45.0f, -28.0f, 0.0f },
		{ 230.0f, 32.0f, 0.0f },
		{ 170.0f, 32.0f, 0.0f },
		1.0f
	};

	InterpolatedCamera::InterpolateKeyFrame keyFrame3
	{
		keyFrame2.endPosition,
		{ 0.0f, -28.0f, 46.0f },
		{ 230.0f, 32.0f, 0.0f },
		{ 170.0f, 32.0f, 0.0f },
		2.0f
	};

	InterpolatedCamera::InterpolateKeyFrame keyFrame4
	{
		keyFrame3.endPosition,
		keyFrame1.startPosition,
		{ 230.0f, 32.0f, 0.0f },
		{ 170.0f, 32.0f, 0.0f },
		1.0f
	};

	m_camera.AddKeyFrame(std::move(keyFrame1));
	m_camera.AddKeyFrame(std::move(keyFrame2));
	m_camera.AddKeyFrame(std::move(keyFrame3));
	m_camera.AddKeyFrame(std::move(keyFrame4));
	m_camera.SetLooping(true);

	m_lightManager.LoadLights();

	m_staticShader = new VPLightShader("Shaders/CompiledShaders/lightingVert.spv", "Shaders/CompiledShaders/lightingFrag.spv", m_camera, m_lightManager);
	m_staticShader->Initialise();

	m_animatedShader = new AnimLightShader("Shaders/CompiledShaders/animVert.spv", "Shaders/CompiledShaders/animFrag.spv", m_camera, m_lightManager);
	m_animatedShader->Initialise();

	if (CreateStaticPipeline() == false)
	{
		return false;
	}
	if (CreateAnimatedPipeline() == false)
	{
		return false;
	}
	if (CreateFramebuffers() == false)
	{
		return false;
	}
	if (CreateCommandPools() == false)
	{
		return false;
	}

	m_modelManager.LoadModels();
	m_textureManager.LoadTextures();

	m_rootNode = new SceneNode("root");
	//m_rootNode->AddChild(std::make_unique<SceneNode>( "cube1", m_modelManager.GetModel("Assets/Models/cube.obj"), m_textureManager.GetTexture("Assets/Textures/cube_uv_solid.jpg"), *m_shader ));
	m_rootNode->AddChild(std::make_unique<SceneNode>( "cowboy", m_modelManager.GetModel("Assets/Models/cowboy.dae"), m_textureManager.GetTexture("Assets/Textures/character_texture.png"), *m_animatedShader, true ));
	m_rootNode->AddChild(std::make_unique<SceneNode>( "pilot", m_modelManager.GetModel("Assets/Models/Pilot/Pilot_LP_Animated.fbx"), m_textureManager.GetTexture("Assets/Models/Pilot/Material.002_Base_Color.png"), *m_animatedShader, true ));
	//m_rootNode->AddChild(std::make_unique<SceneNode>( "cube1", m_modelManager.GetModel("Assets/Models/Stormtrooper/Stormtrooper.fbx"), m_textureManager.GetTexture("Assets/Models/Stormtrooper/Stormtrooper_D.png"), *m_shader, true ));
	m_rootNode->ChildAtIndex(0)->SetPosition({ 8.0f, 0.0f, 0.0f });

	if (CreateCommandBuffers() == false)
	{
		return false;
	}
	if (CreateSemaphoresAndFences() == false)
	{
		return false;
	}

	if (m_imgui.Initialise() == false)
	{
		return false;
	}

	m_timer.Update();

	//Terrain
	static Terrain* terrain = new Terrain(32, 32);
	terrain->Initialise();

	m_rootNode->AddChild(std::make_unique<SceneNode>("terrain", terrain->GetModel(), m_textureManager.GetTexture("Assets/Textures/grass.png"), *m_staticShader));

	return true;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::RecreateSwapchain()
{
	int width = 0;
	int height = 0;
	while (width == 0 || height == 0)
	{
		glfwGetFramebufferSize(m_window, &width, &height);
		glfwWaitEvents();
	}

	vkDeviceWaitIdle(m_device);

	CleanUpSwapchain();

	CreateSwapchain();
	CreateRenderPass();
	CreateStaticPipeline();
	CreateAnimatedPipeline();
	CreateFramebuffers();
	CreateCommandBuffers();
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::Update()
{
	while (glfwWindowShouldClose(m_window) == false && glfwGetKey(m_window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		m_timer.Update();
		const auto& dt = m_timer.DeltaTime();

		glfwPollEvents();

		//Camera Movement
		m_camera.Update(dt);
		const double movement = m_camera.GetSpeed() * dt;
		if (glfwGetKey(m_window, GLFW_KEY_W) == GLFW_PRESS)
		{
			m_camera.ChangePosition((float)movement * m_camera.GetFront());
		}
		if (glfwGetKey(m_window, GLFW_KEY_S) == GLFW_PRESS)
		{
			m_camera.ChangePosition((float)-movement * m_camera.GetFront());
		}
		if (glfwGetKey(m_window, GLFW_KEY_A) == GLFW_PRESS)
		{
			m_camera.ChangePosition((float)-movement * glm::normalize(glm::cross(m_camera.GetFront(), m_camera.GetUp())));
		}
		if (glfwGetKey(m_window, GLFW_KEY_D) == GLFW_PRESS)
		{
			m_camera.ChangePosition((float)movement * glm::normalize(glm::cross(m_camera.GetFront(), m_camera.GetUp())));
		}
		if (glfwGetKey(m_window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			m_camera.ChangePosition({0.0f, -movement, 0.0f});
		}
		if (glfwGetKey(m_window, GLFW_KEY_E) == GLFW_PRESS)
		{
			m_camera.ChangePosition({ 0.0f, movement, 0.0f });
		}
		
		//Camera Rotation
		const float rotSpeed = 60.0f * static_cast<float>(dt);
		if (glfwGetKey(m_window, GLFW_KEY_LEFT) == GLFW_PRESS)
		{
			m_camera.AddYaw(rotSpeed);
		}
		if (glfwGetKey(m_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		{
			m_camera.AddYaw(-rotSpeed);
		}
		if (glfwGetKey(m_window, GLFW_KEY_UP) == GLFW_PRESS)
		{
			m_camera.AddPitch(-rotSpeed);
		}
		if (glfwGetKey(m_window, GLFW_KEY_DOWN) == GLFW_PRESS)
		{
			m_camera.AddPitch(rotSpeed);
		}

		//----------
		double xPos, yPos;
		static double prevXPos, prevYPos;
		static const double mouseSensitivity = 4.0;

		glfwGetCursorPos(m_window, &xPos, &yPos);
		double deltaX = prevXPos - xPos;
		double deltaY = (prevYPos - yPos) * -1.0f;
		
		if (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_RIGHT) && m_camera.HasFocusPoint() == false)
		{
			if (deltaX != 0)
			{
				m_camera.AddYaw(deltaX * dt * mouseSensitivity);
			}
			if (deltaY != 0)
			{
				m_camera.AddPitch(deltaY * dt * mouseSensitivity);
			}
		}

		//----- Screen Picking -----
		SceneNode* closestPickedNode = nullptr;
		if (glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_MIDDLE))
		{
			//NDC
			const float x = (2.0f * xPos) / m_windowWidth - 1.0f;
			const float y = 1.0f - (2.0f * yPos) / m_windowHeight;

			//Homogenous Clip Space
			const glm::vec4 rayClip{ x, y, -1.0f, 1.0f };

			//Eye (Camera) Space
			const glm::vec4 rayEyePRE = glm::inverse(m_camera.GetProjectionMatrix()) * rayClip;
			const glm::vec4 rayEye{ rayEyePRE.x, rayEyePRE.y, -1.0f, 0.0f };

			//World Space
			const glm::vec4 worldSpacePRE = glm::inverse(m_camera.GetViewMatrix()) * rayEye;
			glm::vec3 worldSpace{ worldSpacePRE.x, worldSpacePRE.y, worldSpacePRE.z };
			worldSpace = glm::normalize(worldSpace);

			Ray ray{ m_camera.GetPosition(), worldSpace };
			float smallestT = ray.m_t;
			
			auto HandleCollision = [&closestPickedNode, &smallestT](const Ray& ray, SceneNode* node)
			{
				if (ray.m_t < smallestT)
				{
					closestPickedNode = node;
					smallestT = ray.m_t;
				}
			};

			for (const auto& node : m_rootNode->Children())
			{
				if (node->RayIntersectsAABB(ray))
				{
					HandleCollision(ray, node.get());
				}

				for (const auto& child : node->Children())
				{
					if (node->RayIntersectsAABB(ray))
					{
						HandleCollision(ray, node.get());
					}
				}
			}
		}

		prevXPos = xPos; prevYPos = yPos;

		m_rootNode->Update(dt, m_currentFrame);

		DrawImGuiFrame(closestPickedNode);
		DrawFrame();
	}

	vkDeviceWaitIdle(m_device);
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::DrawFrame()
{
	//Wait for the GPU before submitting another command!
	//This takes an array of fences and waits for all or any of them to signal
	vkWaitForFences(m_device, 1, &m_inflightFences[m_currentFrame], VK_TRUE, UINT64_MAX);

	//Acquire image from the swapchain
	uint32_t imageIndex;
	VkResult result = vkAcquireNextImageKHR(m_device, m_swapchain, UINT64_MAX, m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex);

	if (result == VK_ERROR_OUT_OF_DATE_KHR)
	{
		RecreateSwapchain();
		return;
	}
	else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
	{
		printf("FAILED TO ACQUIRE SWAP CHAIN IMAGE! \n");
		return;
	}

	if (m_imagesInFlight[imageIndex] != VK_NULL_HANDLE)
	{
		vkWaitForFences(m_device, 1, &m_imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
	}
	m_imagesInFlight[imageIndex] = m_inflightFences[m_currentFrame];

	RecordCommandBuffer(imageIndex);
	m_staticShader->UpdateUniformBuffers(imageIndex);
	m_animatedShader->UpdateUniformBuffers(imageIndex);

	VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };	//Wait for the colour rendering to be finished

	std::vector<VkCommandBuffer> commandBuffers{ m_commandBuffers[imageIndex], m_imgui.GetCommandBuffer(imageIndex) };
	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = static_cast<uint32_t>(m_commandBuffers.size());
	submitInfo.pCommandBuffers = commandBuffers.data();

	VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };		//Signals when we can use the image
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	//Unlike semaphores, we need to manually reset the fence to unsignalled state
	vkResetFences(m_device, 1, &m_inflightFences[m_currentFrame]);

	const auto qResult = vkQueueSubmit(m_gfxQueue, 1, &submitInfo, m_inflightFences[m_currentFrame]);
	if (qResult != VK_SUCCESS)
	{
		printf("FAILED TO SUBMIT TO QUEUE! \n");
		__debugbreak();
		return;
	}

	VkSwapchainKHR swapchains[] = {m_swapchain};
	VkPresentInfoKHR presentInfo{};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapchains;
	presentInfo.pImageIndices = &imageIndex;
	//This can be used to store an array of results for each swapchain used to assure it rendered properly
	//Since we're only using one swapchain this is kinda pointless as you can use the result from the present call
	VkResult resArray{};
	presentInfo.pResults = &resArray; 

	result = vkQueuePresentKHR(m_gfxQueue, &presentInfo);
	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
	{
		RecreateSwapchain();
	}
	else if (result != VK_SUCCESS)
	{
		printf("FAILED TO PRESENT SWAP CHAIN IMAGE! ");
		return;
	}

	m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::DrawImGuiFrame(SceneNode* closestPickedNode)
{
	static SceneNode* chosenNode = nullptr;
	if (closestPickedNode != nullptr)
	{
		chosenNode = closestPickedNode;
	}

	m_imgui.StartNewImGuiFrame();

	//----- Main Window -----
	ImGui::Begin("Main Window");

	if (ImGui::BeginTabBar("##tabs", ImGuiTabBarFlags_FittingPolicyResizeDown))
	{
		if (ImGui::BeginTabItem("Camera"))
		{
			m_camera.AddToImGui();
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Scene Nodes"))
		{
			if (SceneNode* node = m_rootNode->AddHierarchyToImGui(); node != nullptr)
			{
				chosenNode = node;
			}

			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Lights"))
		{
			m_lightManager.AddToImGui();
			ImGui::EndTabItem();
		}

		ImGui::EndTabBar();
	}

	ImGui::End();

	//----- Chosen Node Window -----
	ImGui::Begin("Chosen Node");

	if (chosenNode != nullptr)
	{
		chosenNode->AddToImGui();
	}

	ImGui::End();

	//ImGui::ShowDemoWindow();
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::InitGLFW()
{
	if (glfwInit() == false)
	{
		return false;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	m_window = glfwCreateWindow(m_windowWidth, m_windowHeight, enableValidationLayers ? "WW2N - Using Validation Layers!" : "Wagwan2nite", nullptr, nullptr);
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	return m_window != nullptr;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::InitInstance()
{
	if (enableValidationLayers)
	{
		if (CheckValidationLayerSupport() == false)
		{
			printf("VALIDATION LAYERS NOT SUPPORTED! ");
		}
	}

	LoadRequiredExtensions();

	VkApplicationInfo appInfo;
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pNext = nullptr;
	appInfo.pApplicationName =  enableValidationLayers ? "WW2N - Using Validation Layers!" : "Wagwan2nite";
	appInfo.pEngineName = "Wagwan2nite Engine";
	appInfo.engineVersion = 1;
	appInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = (uint32_t)m_instanceExtensionNames.size();
	createInfo.ppEnabledExtensionNames = m_instanceExtensionNames.empty() ? nullptr : m_instanceExtensionNames.data();
	createInfo.flags = NULL;

	VkDebugUtilsMessengerCreateInfoEXT debugUtilInfo;
	if (enableValidationLayers)
	{
		createInfo.enabledLayerCount = (uint32_t)validationLayers.size();
		createInfo.ppEnabledLayerNames = validationLayers.empty() ? nullptr : validationLayers.data();

		
		PopulateDebugMessengerInfo(debugUtilInfo);
		createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)& debugUtilInfo;
	}
	else
	{
		createInfo.enabledLayerCount = 0;
		createInfo.ppEnabledLayerNames = nullptr;
	}

	VkResult result = vkCreateInstance(&createInfo, nullptr, &m_instance);
	if (result != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO CREATE VK INSTANCE ");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::EnumerateDevices()
{
	uint32_t deviceCount;
	vkEnumeratePhysicalDevices(m_instance, &deviceCount, nullptr);

	m_gpuList.resize(deviceCount);
	vkEnumeratePhysicalDevices(m_instance, &deviceCount, m_gpuList.data());

	vkGetPhysicalDeviceMemoryProperties(m_gpuList.front(), &m_deviceMemProps);
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateSurface()
{
#ifdef _WIN32
	VkWin32SurfaceCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	createInfo.pNext = nullptr;
	createInfo.hinstance = GetModuleHandle(NULL);
	createInfo.hwnd = glfwGetWin32Window(m_window);
	VkResult result = vkCreateWin32SurfaceKHR(m_instance, &createInfo, nullptr, &m_surface);
#endif

	if (result != VK_SUCCESS)
	{
		printf("FAILED TO CREATE SURFACE ");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateLogicalDevice()
{
	m_queueFamilyIndices = FindQueueFamilies(m_gpuList.front(), m_surface);

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = { m_queueFamilyIndices.gfxFamily.value(), m_queueFamilyIndices.presentFamily.value() };

	float queuePrio = 1.0f;
	for (uint32_t queueFamily : uniqueQueueFamilies)
	{
		VkDeviceQueueCreateInfo queueCreateInfo{};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePrio;
		queueCreateInfos.push_back(std::move(queueCreateInfo));
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.samplerAnisotropy = VK_TRUE;
	deviceFeatures.fillModeNonSolid = true;

	VkDeviceCreateInfo deviceInfo = {};
	deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

	deviceInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	deviceInfo.pQueueCreateInfos = queueCreateInfos.data();

	deviceInfo.pEnabledFeatures = &deviceFeatures;

	deviceInfo.enabledExtensionCount = (uint32_t)m_deviceExtensionNames.size();
	deviceInfo.ppEnabledExtensionNames = m_deviceExtensionNames.data();

	if (enableValidationLayers)
	{
		deviceInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
		deviceInfo.ppEnabledLayerNames = validationLayers.data();
	}
	else
	{
		deviceInfo.enabledLayerCount = 0;
		deviceInfo.ppEnabledLayerNames = nullptr;
	}
	
	if (vkCreateDevice(m_gpuList.front(), &deviceInfo, nullptr, &m_device) != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO CREATE LOGICAL DEVICE! ");
		return false;
	}

	vkGetDeviceQueue(m_device, m_queueFamilyIndices.gfxFamily.value(), 0, &m_gfxQueue);
	vkGetDeviceQueue(m_device, m_queueFamilyIndices.presentFamily.value(), 0, &m_presentQueue);

	return true;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::LoadRequiredExtensions()
{
	//Instance Extensions
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	m_instanceExtensionNames = { glfwExtensions, glfwExtensions + glfwExtensionCount };

	if (enableValidationLayers)
	{
		m_instanceExtensionNames.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	//Device Extensions
	m_deviceExtensionNames.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CheckValidationLayerSupport()
{
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availLayers.data());

	for (const auto& desiredLayer : validationLayers)
	{
		const auto itr = std::find_if(availLayers.begin(), availLayers.end(), [&desiredLayer](const VkLayerProperties& props) -> bool
		{
			return strcmp(props.layerName, desiredLayer) == 0;
		});

		if (itr == availLayers.end())
		{
			printf("FAILED TO LOAD VALIDATION LAYER! - Currently instance does not support %s \n", desiredLayer);
			return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateSwapchain()
{
	uint32_t formatCount;
	VkResult result = vkGetPhysicalDeviceSurfaceFormatsKHR(m_gpuList.front(), m_surface, &formatCount, NULL);
	VkSurfaceFormatKHR *surfFormats = (VkSurfaceFormatKHR *)malloc(formatCount * sizeof(VkSurfaceFormatKHR));
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(m_gpuList.front(), m_surface, &formatCount, surfFormats);
	if (formatCount == 1 && surfFormats[0].format == VK_FORMAT_UNDEFINED) 
	{
		m_swapchainFormat = VK_FORMAT_B8G8R8A8_UNORM;
	}
	else 
	{
		m_swapchainFormat = surfFormats[0].format;
	}
	free(surfFormats);

	VkSurfaceCapabilitiesKHR surfaceCapabilities;
	result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(m_gpuList.front(), m_surface, &surfaceCapabilities);

	uint32_t presentModeCount;
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(m_gpuList.front(), m_surface, &presentModeCount, nullptr);
	VkPresentModeKHR* presentModes = (VkPresentModeKHR*)malloc(presentModeCount * sizeof(VkPresentModeKHR));		//This is a mem leak
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(m_gpuList.front(), m_surface, &presentModeCount, presentModes);

	if (surfaceCapabilities.currentExtent.width == 0xFFFFFFF || surfaceCapabilities.currentExtent.height == 0xFFFFFFF)
	{
		m_swapchainExtents.width = m_windowWidth;
		m_swapchainExtents.height = m_windowHeight;

		if (m_swapchainExtents.height > surfaceCapabilities.maxImageExtent.height)
		{
			m_swapchainExtents.height = surfaceCapabilities.maxImageExtent.height;
		}
		else if (m_swapchainExtents.height < surfaceCapabilities.minImageExtent.height)
		{
			m_swapchainExtents.height = surfaceCapabilities.minImageExtent.height;
		}

		if (m_swapchainExtents.width > surfaceCapabilities.maxImageExtent.width)
		{
			m_swapchainExtents.width = surfaceCapabilities.maxImageExtent.width;
		}
		else if (m_swapchainExtents.width < surfaceCapabilities.minImageExtent.width)
		{
			m_swapchainExtents.width = surfaceCapabilities.minImageExtent.width;
		}
	}
	else
	{
		m_swapchainExtents = FindWindowExtents(surfaceCapabilities);
	}

	VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;

	uint32_t desiredImageCount = surfaceCapabilities.minImageCount;

	VkSurfaceTransformFlagBitsKHR preTransform;
	if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) 
	{
		preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	}
	else 
	{
		preTransform = surfaceCapabilities.currentTransform;
	}

	// Find a supported composite alpha mode - one of these is guaranteed to be set
	VkCompositeAlphaFlagBitsKHR compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = 
	{
		VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
		VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
		VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
		VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
	};

	for (uint32_t i = 0; i < sizeof(compositeAlphaFlags); i++) 
	{
		if (surfaceCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i])
		{
			compositeAlpha = compositeAlphaFlags[i];
			break;
		}
	}

	VkSwapchainCreateInfoKHR swapchainCreateInfo = {};
	swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	swapchainCreateInfo.pNext = nullptr;
	swapchainCreateInfo.surface = m_surface;
	swapchainCreateInfo.imageFormat = m_swapchainFormat;
	swapchainCreateInfo.minImageCount = desiredImageCount;
	swapchainCreateInfo.imageExtent.width = m_swapchainExtents.width;
	swapchainCreateInfo.imageExtent.height = m_swapchainExtents.height;
	swapchainCreateInfo.compositeAlpha = compositeAlpha;
	swapchainCreateInfo.imageArrayLayers = 1;
	swapchainCreateInfo.preTransform = preTransform;
	swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;
	swapchainCreateInfo.clipped = true;
	swapchainCreateInfo.imageColorSpace = VK_COLORSPACE_SRGB_NONLINEAR_KHR;
	swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	swapchainCreateInfo.queueFamilyIndexCount = 0;
	swapchainCreateInfo.presentMode = swapchainPresentMode;

	if (m_queueFamilyIndices.gfxFamily.value() != m_queueFamilyIndices.presentFamily.value())
	{
		swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;

		const uint32_t index_count = 2;
		uint32_t queueIndices[index_count] = { m_queueFamilyIndices.gfxFamily.value(), m_queueFamilyIndices.presentFamily.value() };
		swapchainCreateInfo.queueFamilyIndexCount = index_count;
		swapchainCreateInfo.pQueueFamilyIndices = queueIndices;
	}

	result = vkCreateSwapchainKHR(m_device, &swapchainCreateInfo, nullptr, &m_swapchain);
	if (result != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO CREATE SWAP CHAIN! ");
		return false;
	}

	result = vkGetSwapchainImagesKHR(m_device, m_swapchain, &m_swapchainImageCount, nullptr);
	if (result != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO GET SWAP CHAIN IMAGES! ");
		return false;
	}

	VkImage* swapchainImages = (VkImage*)malloc(m_swapchainImageCount * sizeof(VkImage));
	result = vkGetSwapchainImagesKHR(m_device, m_swapchain, &m_swapchainImageCount, swapchainImages);
	if (result != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO GET SWAP CHAIN IMAGES! ");
		return false;
	}

	m_bufferImages.resize(m_swapchainImageCount);
	m_bufferImageViews.resize(m_swapchainImageCount);
	for (unsigned int i = 0; i < m_swapchainImageCount; ++i)
	{
		m_bufferImages[i] = swapchainImages[i];

		VkImageViewCreateInfo imageCreateInfo = {};
		imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		imageCreateInfo.pNext = nullptr;
		imageCreateInfo.flags = 0;
		imageCreateInfo.image = m_bufferImages[i];
		imageCreateInfo.format = m_swapchainFormat;
		imageCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		imageCreateInfo.components.r = VK_COMPONENT_SWIZZLE_R;
		imageCreateInfo.components.g = VK_COMPONENT_SWIZZLE_G;
		imageCreateInfo.components.b = VK_COMPONENT_SWIZZLE_B;
		imageCreateInfo.components.a = VK_COMPONENT_SWIZZLE_A;
		imageCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		imageCreateInfo.subresourceRange.baseMipLevel = 0;
		imageCreateInfo.subresourceRange.levelCount = 1;
		imageCreateInfo.subresourceRange.baseArrayLayer = 0;
		imageCreateInfo.subresourceRange.layerCount = 1;

		result = vkCreateImageView(m_device, &imageCreateInfo, nullptr, &m_bufferImageViews[i]);
		if (result != VK_SUCCESS)
		{
			printf("FATAL ERROR - FAILED TO GET SWAP CHAIN IMAGE VIEW! ");
			return false;
		}
	}
	free(swapchainImages);

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateDepthResources()
{
	m_depthFormat = FindSupportedFormat(m_gpuList.front(), 
							{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D32_SFLOAT_S8_UINT },
							VK_IMAGE_TILING_OPTIMAL,
							VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);

	const bool has_failed = CreateImage(m_swapchainExtents.width, m_swapchainExtents.height, m_depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_depthBufferImage, m_depthMemory);
	CreateImageView(m_depthBufferView, m_depthBufferImage, m_depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);

	return has_failed;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateRenderPass()
{
	//Remember, attachments give additional context for the render pass
	//Basically explains extra things to do in the pass (such as layout transitions)
	VkAttachmentDescription attachments[2];
	//This is the swapchain rendering attachment
	attachments[0].format = m_swapchainFormat;
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;	//This should be present if we're not using DearImGui
	attachments[0].flags = 0;

	//This is the depth buffer attachment
	attachments[1].format = m_depthFormat;
	attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	attachments[1].flags = 0;

	VkAttachmentReference colourReference = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
	VkAttachmentReference depthReference = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;	//To explain this is a gfx subpass and not another version (these may be added later to Vulkan)
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colourReference;
	subpass.pDepthStencilAttachment = &depthReference;

	VkSubpassDependency dependency{};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo renderPassCreateInfo = {};
	renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassCreateInfo.pNext = nullptr;
	renderPassCreateInfo.pAttachments = attachments;
	renderPassCreateInfo.attachmentCount = 2;
	renderPassCreateInfo.pSubpasses = &subpass;
	renderPassCreateInfo.subpassCount = 1;
	renderPassCreateInfo.pDependencies = &dependency;
	renderPassCreateInfo.dependencyCount = 1;

	if (vkCreateRenderPass(m_device, &renderPassCreateInfo, nullptr, &m_renderPass) != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO CREATE RENDER PASS!! ");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateStaticPipeline()
{
	//Normally, OpenGL and DX have default states that you can leave them in to render stuff
	//However, Vulkan does not. You need to specify EVERYTHING!
	//Expect this to be a long and boring function with a shit-ton of boilerplate

	//Vertex Input State Creation - This would usually be used to give Vulkan the vertex info
	//Since the vertex info is hardcoded into the shader, we have nothing to pass it here
	VkPipelineVertexInputStateCreateInfo vertInputInfo{};
	vertInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertInputInfo.vertexBindingDescriptionCount = 1;
	vertInputInfo.pVertexBindingDescriptions = &Vertex::GetBindingDesc();
	vertInputInfo.vertexAttributeDescriptionCount = (uint32_t)Vertex::GetAttributeDescs().size();
	vertInputInfo.pVertexAttributeDescriptions = Vertex::GetAttributeDescs().data();

	//Input Assembly Creation - This tells Vulkan how to use the vertex information it's being given
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{};
	inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyInfo.primitiveRestartEnable = false;

	//Viewport Creation - Creates a viewport for Vulkan to render to and a scissor rectangle to cull (if needed)
	VkViewport viewport;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)m_swapchainExtents.width;
	viewport.height = (float)m_swapchainExtents.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor{};
	scissor.offset = { 0, 0 };
	scissor.extent = m_swapchainExtents;

	VkPipelineViewportStateCreateInfo viewportInfo{};
	viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportInfo.viewportCount = 1;
	viewportInfo.pViewports = &viewport;
	viewportInfo.scissorCount = 1;
	viewportInfo.pScissors = &scissor;

	//Rasterisation Setup
	VkPipelineRasterizationStateCreateInfo rasterizerInfo{};
	rasterizerInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizerInfo.polygonMode = VK_POLYGON_MODE_FILL;
	//rasterizerInfo.polygonMode = VK_POLYGON_MODE_LINE;
	rasterizerInfo.lineWidth = 1.0f;
	rasterizerInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizerInfo.cullMode = VK_CULL_MODE_NONE;
	rasterizerInfo.rasterizerDiscardEnable = VK_FALSE;
	rasterizerInfo.depthClampEnable = VK_FALSE;
	rasterizerInfo.depthBiasEnable = VK_FALSE;
	rasterizerInfo.depthBiasConstantFactor = 0.0f;
	rasterizerInfo.depthBiasClamp = 0.0f;
	rasterizerInfo.depthBiasSlopeFactor = 0.0f;

	//Multisampling
	VkPipelineMultisampleStateCreateInfo multisamplingInfo{};
	multisamplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisamplingInfo.sampleShadingEnable = VK_FALSE;
	multisamplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisamplingInfo.minSampleShading = 1.0f;
	multisamplingInfo.pSampleMask = nullptr;
	multisamplingInfo.alphaToCoverageEnable = VK_FALSE;
	multisamplingInfo.alphaToOneEnable = VK_FALSE;

	//Depth Stencil
	VkPipelineDepthStencilStateCreateInfo depthInfo = {};
	depthInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthInfo.depthTestEnable = VK_TRUE;
	depthInfo.depthWriteEnable = VK_TRUE;
	depthInfo.depthCompareOp = VK_COMPARE_OP_LESS;
	depthInfo.depthBoundsTestEnable = VK_FALSE;
	depthInfo.stencilTestEnable = VK_FALSE;

	//Colour Blending
	VkPipelineColorBlendAttachmentState colourBlendAttachment{};
	colourBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colourBlendAttachment.blendEnable = VK_FALSE;
	colourBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colourBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	colourBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colourBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colourBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colourBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colourBlendInfo{};
	colourBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colourBlendInfo.logicOpEnable = VK_FALSE; //Enable this for colour merging as discussed in word doc
	colourBlendInfo.logicOp = VK_LOGIC_OP_COPY;
	colourBlendInfo.attachmentCount = 1;
	colourBlendInfo.pAttachments = &colourBlendAttachment;

	//Push Constant(s)
	VkPushConstantRange pushConstantRange = {};
	pushConstantRange.size = sizeof(ObjPushConstant);
	pushConstantRange.offset = 0;
	pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	//Pipeline Layout
	VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &m_staticShader->m_descriptorLayout;
	pipelineLayoutInfo.pushConstantRangeCount = 1;
	pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;

	if (vkCreatePipelineLayout(m_device, &pipelineLayoutInfo, nullptr, &m_staticPipelineLayout) != VK_SUCCESS)
	{
		printf("FAILED TO CREATE PIPELINE LAYOUT! ");
		return false;
	}

	//Dynamic States - Allow these elements to change at runtime
	std::array<VkDynamicState, 2> states = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };

	VkPipelineDynamicStateCreateInfo dynInfo = {};
	dynInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynInfo.dynamicStateCount = 2;
	dynInfo.pDynamicStates = states.data();

	VkGraphicsPipelineCreateInfo gfxPipelineInfo{};
	gfxPipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	gfxPipelineInfo.stageCount = 2;
	gfxPipelineInfo.pStages = m_staticShader->m_shaderStageInfos;
	gfxPipelineInfo.pVertexInputState = &vertInputInfo;
	gfxPipelineInfo.pInputAssemblyState = &inputAssemblyInfo;
	gfxPipelineInfo.pViewportState = &viewportInfo;
	gfxPipelineInfo.pRasterizationState = &rasterizerInfo;
	gfxPipelineInfo.pMultisampleState = &multisamplingInfo;
	gfxPipelineInfo.pDepthStencilState = &depthInfo;
	gfxPipelineInfo.pColorBlendState = &colourBlendInfo;
	gfxPipelineInfo.pDynamicState = nullptr;
	gfxPipelineInfo.layout = m_staticPipelineLayout;
	gfxPipelineInfo.renderPass = m_renderPass;
	gfxPipelineInfo.subpass = 0;				//This is the index of the subpass and not a reference to it like in the other parameter values

	//gfxPipelineInfo.basePipelineHandle	It is possible for a pipeline to inherit from an already established one. You can use either the
	//gfxPipelineInfo.basePipelineIndex		handle or the index to define the parent pipeline

	if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &gfxPipelineInfo, nullptr, &m_staticPipeline) != VK_SUCCESS)
	{
		printf("FAILED TO CREATE THE STATIC GFX PIPELINE! ");
		return false;
	}

	m_staticShader->SetPipelineInfo(m_staticPipelineLayout, m_staticPipeline);

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateAnimatedPipeline()
{
	//Normally, OpenGL and DX have default states that you can leave them in to render stuff
	//However, Vulkan does not. You need to specify EVERYTHING!
	//Expect this to be a long and boring function with a shit-ton of boilerplate

	//Vertex Input State Creation - This would usually be used to give Vulkan the vertex info
	//Since the vertex info is hardcoded into the shader, we have nothing to pass it here
	VkPipelineVertexInputStateCreateInfo vertInputInfo{};
	vertInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertInputInfo.vertexBindingDescriptionCount = 1;
	vertInputInfo.pVertexBindingDescriptions = &AnimVertex::GetBindingDesc();
	vertInputInfo.vertexAttributeDescriptionCount = (uint32_t)AnimVertex::GetAttributeDescs().size();
	vertInputInfo.pVertexAttributeDescriptions = AnimVertex::GetAttributeDescs().data();

	//Input Assembly Creation - This tells Vulkan how to use the vertex information it's being given
	VkPipelineInputAssemblyStateCreateInfo inputAssemblyInfo{};
	inputAssemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssemblyInfo.primitiveRestartEnable = false;

	//Viewport Creation - Creates a viewport for Vulkan to render to and a scissor rectangle to cull (if needed)
	VkViewport viewport;
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = (float)m_swapchainExtents.width;
	viewport.height = (float)m_swapchainExtents.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor{};
	scissor.offset = { 0, 0 };
	scissor.extent = m_swapchainExtents;

	VkPipelineViewportStateCreateInfo viewportInfo{};
	viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportInfo.viewportCount = 1;
	viewportInfo.pViewports = &viewport;
	viewportInfo.scissorCount = 1;
	viewportInfo.pScissors = &scissor;

	//Rasterisation Setup
	VkPipelineRasterizationStateCreateInfo rasterizerInfo{};
	rasterizerInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizerInfo.polygonMode = VK_POLYGON_MODE_FILL;
	//rasterizerInfo.polygonMode = VK_POLYGON_MODE_LINE;
	rasterizerInfo.lineWidth = 1.0f;
	rasterizerInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterizerInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizerInfo.rasterizerDiscardEnable = VK_FALSE;
	rasterizerInfo.depthClampEnable = VK_FALSE;
	rasterizerInfo.depthBiasEnable = VK_FALSE;
	rasterizerInfo.depthBiasConstantFactor = 0.0f;
	rasterizerInfo.depthBiasClamp = 0.0f;
	rasterizerInfo.depthBiasSlopeFactor = 0.0f;

	//Multisampling
	VkPipelineMultisampleStateCreateInfo multisamplingInfo{};
	multisamplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisamplingInfo.sampleShadingEnable = VK_FALSE;
	multisamplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisamplingInfo.minSampleShading = 1.0f;
	multisamplingInfo.pSampleMask = nullptr;
	multisamplingInfo.alphaToCoverageEnable = VK_FALSE;
	multisamplingInfo.alphaToOneEnable = VK_FALSE;

	//Depth Stencil
	VkPipelineDepthStencilStateCreateInfo depthInfo = {};
	depthInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthInfo.depthTestEnable	= VK_TRUE;
	depthInfo.depthWriteEnable	= VK_TRUE;
	depthInfo.depthCompareOp = VK_COMPARE_OP_LESS;
	depthInfo.depthBoundsTestEnable = VK_FALSE;
	depthInfo.stencilTestEnable = VK_FALSE;

	//Colour Blending
	VkPipelineColorBlendAttachmentState colourBlendAttachment{};
	colourBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colourBlendAttachment.blendEnable = VK_FALSE;
	colourBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	colourBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
	colourBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colourBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colourBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colourBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colourBlendInfo{};
	colourBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colourBlendInfo.logicOpEnable = VK_FALSE; //Enable this for colour merging as discussed in word doc
	colourBlendInfo.logicOp = VK_LOGIC_OP_COPY;
	colourBlendInfo.attachmentCount = 1;
	colourBlendInfo.pAttachments = &colourBlendAttachment;

	//Push Constant(s)
	VkPushConstantRange pushConstantRange = {};
	pushConstantRange.size = sizeof(ObjPushConstant);
	pushConstantRange.offset = 0;
	pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	//Pipeline Layout
	VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &m_animatedShader->m_descriptorLayout;
	pipelineLayoutInfo.pushConstantRangeCount = 1;
	pipelineLayoutInfo.pPushConstantRanges = &pushConstantRange;

	if (vkCreatePipelineLayout(m_device, &pipelineLayoutInfo, nullptr, &m_animatedPipelineLayout) != VK_SUCCESS)
	{
		printf("FAILED TO CREATE PIPELINE LAYOUT! ");
		return false;
	}

	//Dynamic States - Allow these elements to change at runtime
	std::array<VkDynamicState, 2> states = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };

	VkPipelineDynamicStateCreateInfo dynInfo = {};
	dynInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynInfo.dynamicStateCount = 2;
	dynInfo.pDynamicStates = states.data();

	VkGraphicsPipelineCreateInfo gfxPipelineInfo{};
	gfxPipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	gfxPipelineInfo.stageCount = 2;
	gfxPipelineInfo.pStages = m_animatedShader->m_shaderStageInfos;
	gfxPipelineInfo.pVertexInputState = &vertInputInfo;
	gfxPipelineInfo.pInputAssemblyState = &inputAssemblyInfo;
	gfxPipelineInfo.pViewportState = &viewportInfo;
	gfxPipelineInfo.pRasterizationState = &rasterizerInfo;
	gfxPipelineInfo.pMultisampleState = &multisamplingInfo;
	gfxPipelineInfo.pDepthStencilState = &depthInfo;
	gfxPipelineInfo.pColorBlendState = &colourBlendInfo;
	gfxPipelineInfo.pDynamicState = nullptr;
	gfxPipelineInfo.layout = m_animatedPipelineLayout;
	gfxPipelineInfo.renderPass = m_renderPass;
	gfxPipelineInfo.subpass = 0;				//This is the index of the subpass and not a reference to it like in the other parameter values

	//gfxPipelineInfo.basePipelineHandle	It is possible for a pipeline to inherit from an already established one. You can use either the
	//gfxPipelineInfo.basePipelineIndex		handle or the index to define the parent pipeline

	if (vkCreateGraphicsPipelines(m_device, VK_NULL_HANDLE, 1, &gfxPipelineInfo, nullptr, &m_animatedPipeline) != VK_SUCCESS)
	{
		printf("FAILED TO CREATE THE ANIMATION GFX PIPELINE! ");
		return false;
	}

	m_animatedShader->SetPipelineInfo(m_animatedPipelineLayout, m_animatedPipeline);

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateFramebuffers()
{
	m_swapchainFramebuffers.resize(m_bufferImageViews.size());

	for (size_t i = 0; i < m_bufferImageViews.size(); ++i)
	{
		std::array<VkImageView, 2> attachments =
		{
			m_bufferImageViews[i],
			m_depthBufferView
		};

		VkFramebufferCreateInfo framebufferInfo{};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = m_renderPass;
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.attachmentCount = (uint32_t)attachments.size();
		framebufferInfo.width = m_swapchainExtents.width;
		framebufferInfo.height = m_swapchainExtents.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(m_device, &framebufferInfo, nullptr, &m_swapchainFramebuffers[i]) != VK_SUCCESS)
		{
			printf("FAILED TO CREATE FRAMEBUFFER! \n");
			return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateCommandPools()
{
	//Creation
	VkCommandPoolCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.queueFamilyIndex = m_queueFamilyIndices.gfxFamily.value();
	createInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	if (vkCreateCommandPool(m_device, &createInfo, nullptr, &m_commandPool) != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO CREATE COMMAND POOL! ");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateCommandBuffers()
{
	m_commandBuffers.resize(m_swapchainFramebuffers.size());

	VkCommandBufferAllocateInfo allocateInfo = {};
	allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocateInfo.pNext = nullptr;
	allocateInfo.commandPool = m_commandPool;
	allocateInfo.commandBufferCount = (uint32_t)m_commandBuffers.size();
	allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

	if (vkAllocateCommandBuffers(m_device, &allocateInfo, m_commandBuffers.data()) != VK_SUCCESS)
	{
		printf("FATAL ERROR - FAILED TO CREATE COMMAND BUFFERS! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::RecordCommandBuffer(const uint32_t currentImage)
{
	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	beginInfo.pInheritanceInfo = nullptr;

	VkCommandBuffer& currentBuffer = m_commandBuffers[currentImage];
	if (vkBeginCommandBuffer(currentBuffer, &beginInfo) != VK_SUCCESS)
	{
		printf("FAILED TO BEGIN COMMAND BUFFER! ");
		return false;
	}
	
	std::array<VkClearValue, 2> clearColours{};
	clearColours[0] = { 0.1f, 0.1f, 0.3f, 1.0f };
	clearColours[1] = { 1.0f, 0.0f };

	VkRenderPassBeginInfo renderBeginInfo = {};
	renderBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderBeginInfo.renderPass = m_renderPass;
	renderBeginInfo.framebuffer = m_swapchainFramebuffers[currentImage];
	renderBeginInfo.renderArea.offset = { 0, 0 };
	renderBeginInfo.renderArea.extent = m_swapchainExtents;
	renderBeginInfo.clearValueCount = 2;
	renderBeginInfo.pClearValues = clearColours.data();

	//Full Render
	vkCmdBeginRenderPass(currentBuffer, &renderBeginInfo, VK_SUBPASS_CONTENTS_INLINE);	
	m_rootNode->Draw(currentImage, currentBuffer, m_timer);
	vkCmdEndRenderPass(currentBuffer);

	if (vkEndCommandBuffer(currentBuffer) != VK_SUCCESS)
	{
		printf("FAILED TO RECORD TO COMMAND BUFFER - %d ! \n", currentImage);
		return false;
	}

	m_imgui.Draw(currentImage);

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateSemaphoresAndFences()
{
	//Render Syncs
	m_imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	m_renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);

	m_inflightFences.resize(MAX_FRAMES_IN_FLIGHT);
	m_imagesInFlight.resize(m_swapchainImageCount, VK_NULL_HANDLE);

	VkSemaphoreCreateInfo semaphoreInfo{};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo{};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		if (vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_imageAvailableSemaphores[i]) != VK_SUCCESS ||
			vkCreateSemaphore(m_device, &semaphoreInfo, nullptr, &m_renderFinishedSemaphores[i]) != VK_SUCCESS ||
			vkCreateFence(m_device, &fenceInfo, nullptr, &m_inflightFences[i]) != VK_SUCCESS)
		{
			printf("FAILED TO CREATE RENDERING SEMAPHORE or FENCE!! \n");
			return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::FindMemoryType(uint32_t typeBits, const VkFlags requirementMask, uint32_t* typeIndex)
{
	for (uint32_t i = 0; i < m_deviceMemProps.memoryTypeCount; i++) 
	{
		if ((typeBits & 1) == 1) 
		{
			// Type is available, does it match user properties?
			if ((m_deviceMemProps.memoryTypes[i].propertyFlags & requirementMask) == requirementMask) 
			{
				*typeIndex = i;
				return true;
			}
		}
		typeBits >>= 1;
	}
	// No memory types matched, return failure
	return false;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::SetupDebugMessenger()
{
	if (enableValidationLayers == false)		
		return;

	VkDebugUtilsMessengerCreateInfoEXT createInfo;
	PopulateDebugMessengerInfo(createInfo);

	VkResult result = CreateDebugUtilsMessengerEXT(m_instance, &createInfo, nullptr, &m_debugMessenger);
	if (result != VK_SUCCESS)
	{
		printf("FAILED TO CREATE DEBUG MESSENGER! ");
	}
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::PopulateDebugMessengerInfo(VkDebugUtilsMessengerCreateInfoEXT& info)
{
	info = {};
	info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_FLAG_BITS_MAX_ENUM_EXT;
	info.pfnUserCallback = debugMessageCallback;
}

//-------------------------------------------------------------------------------------------------
VkExtent2D VulkanCore::FindWindowExtents(const VkSurfaceCapabilitiesKHR& cap)
{
	if (cap.currentExtent.width != UINT32_MAX)
	{
		return cap.currentExtent;
	}
	else
	{
		int width, height;
		glfwGetFramebufferSize(m_window, &width, &height);

		return { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };
	}
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags props, VkBuffer& buffer, VkDeviceMemory& memory)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.usage = usage;
	bufferInfo.size = size;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	if (vkCreateBuffer(m_singleton->m_device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS)
	{
		printf("WARNING - FAILED TO CREATE VERTEX BUFFER \n");
		return false;
	}

	VkMemoryRequirements memReqs = {};
	vkGetBufferMemoryRequirements(m_singleton->m_device, buffer, &memReqs);

	VkMemoryAllocateInfo allocateInfo = {};
	allocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocateInfo.allocationSize = memReqs.size;
	allocateInfo.memoryTypeIndex = FindMemoryTypeFromReqs(m_singleton->m_gpuList.front(), memReqs.memoryTypeBits, props);

	if (vkAllocateMemory(m_singleton->m_device, &allocateInfo, nullptr, &memory) != VK_SUCCESS)
	{
		printf("WARNING - FAILED TO ALLOCATE MEMORY FOR BUFFER \n");
		return false;
	}

	vkBindBufferMemory(m_singleton->m_device, buffer, memory, 0);
	return true;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::CopyBuffer(VkBuffer src, VkBuffer dst, VkDeviceSize size)
{
	VkCommandBuffer commandBuffer = m_singleton->BeginSingleTimeCommands();

	VkBufferCopy copyRegion = {};
	copyRegion.size = size;
	vkCmdCopyBuffer(commandBuffer, src, dst, 1, &copyRegion);

	m_singleton->EndSingleTimeCommands(commandBuffer, m_singleton->m_gfxQueue);
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateImage(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usageFlags, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory, const uint32_t mipLevels, const uint32_t arrayLayers)
{
	VkFormatProperties formatProps;
	vkGetPhysicalDeviceFormatProperties(VulkanCore::Singleton().m_gpuList.front(), format, &formatProps);

	if ((formatProps.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT) == 0)
	{
		printf("Provided format is not supported for a sampled image \n");
		return false;
	}

	if ((formatProps.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT) == 0)
	{
		printf("Provided format is not supported for linear image filtering \n");
		return false;
	}

	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = mipLevels;
	imageInfo.arrayLayers = arrayLayers;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = usageFlags;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;

	if (vkCreateImage(m_singleton->m_device, &imageInfo, nullptr, &image) != VK_SUCCESS)
	{
		return false;
	}

	//-------------
	VkMemoryRequirements memReqs;
	vkGetImageMemoryRequirements(m_singleton->m_device, image, &memReqs);

	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memReqs.size;
	allocInfo.memoryTypeIndex = FindMemoryTypeFromReqs(m_singleton->m_gpuList.front(), memReqs.memoryTypeBits, properties);

	if (vkAllocateMemory(m_singleton->m_device, &allocInfo, nullptr, &imageMemory) != VK_SUCCESS)
	{
		return false;
	}

	vkBindImageMemory(m_singleton->m_device, image, imageMemory, 0);

	return true;
}

//-------------------------------------------------------------------------------------------------
bool VulkanCore::CreateImageView(VkImageView& imageView, VkImage& image, VkFormat format, VkImageAspectFlags aspectFlags)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.format = format;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
	viewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
	viewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
	viewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.layerCount = 1;

	if (vkCreateImageView(VulkanCore::Singleton().Device(), &viewInfo, nullptr, &imageView) != VK_SUCCESS)
	{
		printf("ERROR - Failed to create image view! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
VkCommandBuffer VulkanCore::BeginSingleTimeCommands() const
{
	VkCommandBuffer commandBuffer;

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = m_commandPool;
	allocInfo.commandBufferCount = 1;

	vkAllocateCommandBuffers(m_device, &allocInfo, &commandBuffer);

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::EndSingleTimeCommands(VkCommandBuffer& commandBuffer, VkQueue& queue)
{
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(queue);

	vkFreeCommandBuffers(m_device, m_commandPool, 1, &commandBuffer);
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::TransitionImageLayout(VkImage& image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout)
{
	VkCommandBuffer commandBuffer = m_singleton->BeginSingleTimeCommands();

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER; //VK_IMAGE_LAYOUT_UNDEFINED if don't care about original layout
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; //Used when transferring ownership of queues
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED; //Used when transferring ownership of queues
	barrier.image = image;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.subresourceRange.levelCount = 1;
	barrier.srcAccessMask = 0; //TODO
	barrier.dstAccessMask = 0; //TODO

		//----- Find Pipeline Stage Flags! -----
	VkPipelineStageFlags srcStage, dstStage;
	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		srcStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		dstStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		srcStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		dstStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else
	{
		printf("ERROR - Image transition not supported! \n");
		return;
	}

	vkCmdPipelineBarrier
	(
		commandBuffer,
		srcStage, dstStage,	//TODO
		0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	m_singleton->EndSingleTimeCommands(commandBuffer, m_singleton->m_gfxQueue);
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::CopyBufferToImage(VkBuffer& imageBuffer, VkImage& image, uint32_t w, uint32_t h, const std::optional<VkImageSubresourceLayers> subresource)
{
	VkCommandBuffer commandBuffer = m_singleton->BeginSingleTimeCommands();

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;
	region.imageExtent = { w, h, 1 };
	region.imageOffset = { 0, 0, 0 };

	if (subresource.has_value())
	{
		region.imageSubresource = subresource.value();
	}
	else
	{
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.baseArrayLayer = 0;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.layerCount = 1;
	}

	vkCmdCopyBufferToImage(commandBuffer, imageBuffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);

	m_singleton->EndSingleTimeCommands(commandBuffer, m_singleton->m_gfxQueue);
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::CleanUp()
{
	//for (auto& object : m_renderObject)
	//{
	//	delete object; object = nullptr;
	//}

	delete m_rootNode; m_rootNode = nullptr;
	delete m_animatedShader; m_animatedShader = nullptr;
	delete m_staticShader; m_staticShader = nullptr;

	m_modelManager.CleanUp();
	m_textureManager.CleanUp();

	//Semaphores
	for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		vkDestroySemaphore(m_device, m_renderFinishedSemaphores[i], nullptr);
		vkDestroySemaphore(m_device, m_imageAvailableSemaphores[i], nullptr);
		vkDestroyFence(m_device, m_inflightFences[i], nullptr);
	}

	//Swapchain and relevant objects
	CleanUpSwapchain();

	//Descriptor Set Layout(s)
	//vkDestroyDescriptorSetLayout(m_device, m_descriptorLayout, nullptr);

	//Command Pools/Buffers
	vkDestroyCommandPool(m_device, m_commandPool, nullptr);

	//Device
	vkDestroyDevice(m_device, nullptr);

	//Validation Layers
	if (enableValidationLayers)
	{
		DestroyDebugUtilsMessengerEXT(m_instance, m_debugMessenger, nullptr);
	}

	//Surface
	vkDestroySurfaceKHR(m_instance, m_surface, nullptr);

	//Instance
	vkDestroyInstance(m_instance, nullptr);

	//GLFW
	glfwDestroyWindow(m_window);
	glfwTerminate(); m_window = nullptr;

	m_cleanedUp = true;
}

//-------------------------------------------------------------------------------------------------
void VulkanCore::CleanUpSwapchain()
{
	//DepthStencil
	vkDestroyImageView(m_device, m_depthBufferView, nullptr);
	vkDestroyImage(m_device, m_depthBufferImage, nullptr);
	vkFreeMemory(m_device, m_depthMemory, nullptr);

	//Framebuffers
	for (auto& framebuffer : m_swapchainFramebuffers)
	{
		vkDestroyFramebuffer(m_device, framebuffer, nullptr);
	}

	//Command Buffers
	//This could be avoided by deleting the entire pool and starting from scratch
	//That is wasteful and this way means we can reuse the existing pool and clear the old stuff out of it
	vkFreeCommandBuffers(m_device, m_commandPool, (uint32_t)m_commandBuffers.size(), m_commandBuffers.data());

	//Gfx Pipeline
	vkDestroyPipeline(m_device, m_animatedPipeline, nullptr);
	vkDestroyPipeline(m_device, m_staticPipeline, nullptr);

	//Pipeline Layouts
	vkDestroyPipelineLayout(m_device, m_animatedPipelineLayout, nullptr);
	vkDestroyPipelineLayout(m_device, m_staticPipelineLayout, nullptr);

	//Render Pass
	vkDestroyRenderPass(m_device, m_renderPass, nullptr);
	//vkDestroySemaphore(m_device, m_renderPassSemaphore, nullptr);

	//Swapchain
	for (auto& imageView : m_bufferImageViews)
	{
		vkDestroyImageView(m_device, imageView, nullptr);
	}
	vkDestroySwapchainKHR(m_device, m_swapchain, nullptr);
}