#include "SceneNode.h"

#include <glm/common.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/mat4x4.hpp>

#include <functional>

#include "Model.h"
#include "RenderObject.h"
#include "CubeMap.h"
#include "Timer.h"
#include "Ray.h"
#include "Shader.h"

#include "ImGui/imgui_helper_functions.h"
#include "ImGui/imgui.h"

namespace
{
	void AddChosenNodeToImGui(SceneNode& n)
	{
		ImGui::OpenPopup("Edit Node");

		if (ImGui::BeginPopup("Edit Node"))
		{
			ImGui::Text(n.Id().c_str());
			n.AddToImGui();
			ImGui::EndPopup();
		}

	}
}

SceneNode::SceneNode(std::string&& id) :
	m_id(std::move(id)),
	m_isVisible(true),
	m_renderObject(nullptr),
	m_position({ 0.0f }),
	m_rotation({ 0.0f }),
	m_scale({ 0.0f }),
	m_colour({ 0.0f, 0.0f, 0.0f }),
	m_modelMatrix(glm::mat4(1.0f))
{
}

SceneNode::SceneNode(std::string&& id, const Model& model, const Texture& texture, Shader& shader, const bool isAnimated) :
	m_id(std::move(id)),
	m_renderObject(isAnimated ? new AnimRenderObject{ model, texture, shader } : new RenderObject{ model, texture, shader }),
	m_position({ 0.0f }),
	m_rotation({ 0.0f }),
	m_scale({ 1.0f }),
	m_modelMatrix(glm::mat4(1.0f)),
	m_colour({ 0.0f, 0.0f, 1.0f }),
	m_isVisible(true)
{
}

SceneNode::SceneNode(std::string&& id, const Model& model, std::vector<const Texture*> textures, Shader& shader) :
	m_id(std::move(id)),
	m_renderObject(new RenderObject{ model, textures, shader }),
	m_position({ 0.0f }),
	m_rotation({ 0.0f }),
	m_scale({ 1.0f }),
	m_modelMatrix(glm::mat4(1.0f)),
	m_colour({ 0.0f, 0.0f, 1.0f }),
	m_isVisible(true)
{
}

SceneNode::SceneNode(std::string&& id, const Model& model, const CubeTexture& texture, Shader& shader) :
	m_id(std::move(id)),
	m_renderObject(new CubeMap{ model, texture, shader }),
	m_position({ 0.0f }),
	m_rotation({ 0.0f }),
	m_scale({ 1.0f }),
	m_modelMatrix(glm::mat4(1.0f)),
	m_colour({ 0.0f, 0.0f, 1.0f }),
	m_isVisible(true)
{
}

SceneNode::~SceneNode()
{
	delete m_renderObject; m_renderObject = nullptr;
}

//-------------------------------------------------------------------------------------------------
void SceneNode::Update(const double dt, const uint32_t currentImage)
{
	if (m_renderObject)
	{
		m_renderObject->Update(dt, currentImage);
	}

	for (auto& child : m_children)
	{
		child->Update(dt, currentImage);
	}
}

//-------------------------------------------------------------------------------------------------
void SceneNode::Draw(const uint32_t currentImage, VkCommandBuffer& buffer, const Timer& timer) const
{
	if (m_isVisible)
	{
		if (m_renderObject)
		{
			const Shader& shader = m_renderObject->GetShader();
			if (shader.GetPipeline() != VK_NULL_HANDLE && shader.GetPipelineLayout() != VK_NULL_HANDLE)
			{
				vkCmdBindPipeline(buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader.GetPipeline());
				vkCmdPushConstants(buffer, shader.GetPipelineLayout(), VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(ObjPushConstant), (void*)&ObjPushConstant(m_modelMatrix, m_colour, timer.DeltaTime()));
				m_renderObject->Draw(currentImage, buffer, shader.GetPipelineLayout());
			}
			else
			{
				printf("Shader is missing pipeline info, this shader will NOT render anything! \n");
			}
		}

		for (const auto& child : m_children)
		{
			child->Draw(currentImage, buffer, timer);
		}
	}
}

//-------------------------------------------------------------------------------------------------
void SceneNode::AddToImGui()
{
	auto CreateEntry = [](const char* txt, glm::vec3& data) -> bool
	{
		ImGui::Text(txt);
		ImGui::NextColumn();
		const bool dirty = ImGuiHelper::CreateSameLineBoxForVector(txt, data);
		ImGui::NextColumn();
		ImGui::Separator();

		return dirty;
	};

	ImGui::Text(m_id.c_str());

	ImGui::Columns(2);
	ImGui::SetColumnWidth(0, 96);
	ImGui::SetColumnWidth(1, 384);
	ImGui::Separator();

	ImGui::Text("Is Visible");
	ImGui::NextColumn();
	ImGui::Checkbox("##Is Visible", &m_isVisible);
	ImGui::NextColumn();
	ImGui::Separator();

	bool matrixDirty = false;
	matrixDirty |= CreateEntry("Position",	m_position);
	matrixDirty |= CreateEntry("Rotation",	m_rotation);
	matrixDirty |= CreateEntry("Scale",		m_scale);
	ImGui::Columns(1);

	if (matrixDirty)
	{
		CalculateModelMatrix(glm::mat4(1.0f));
		RefreshChildrenMatrices();
	}

	if (m_renderObject)
	{
		m_renderObject->AddToImGui();
	}
}

//-------------------------------------------------------------------------------------------------
SceneNode* SceneNode::AddHierarchyToImGui()
{
	//Cache whether the tree node is expanded
	const bool isNodeExpanded = ImGui::TreeNode(m_id.c_str());
	SceneNode* chosenNode = nullptr;

	//If this node is right clicked, open up editing popup
	//This needs to be done inbetween the tree node being added and it being popped
	//This is why we cache the result above
	if (ImGui::IsItemClicked(1))
	{
		chosenNode = this;
	}

	if (isNodeExpanded)
	{
		for (auto& child : m_children)
		{
			if (SceneNode* chosenChild = child->AddHierarchyToImGui(); chosenNode == nullptr && chosenChild != nullptr)
			{
				chosenNode = chosenChild;
			}
		}

		ImGui::TreePop();
	}

	return chosenNode;
}

//-------------------------------------------------------------------------------------------------
void SceneNode::CalculateModelMatrix(const glm::mat4& parentMatrix)
{
	//Could be done in one line but I like this for clarity and it is also easier to debug
	m_modelMatrix = parentMatrix;

	m_modelMatrix = glm::scale(m_modelMatrix, m_scale);
	
	glm::vec3 axes;
	for (uint32_t i = 0; i < 3; ++i)
	{
		axes[i] = 1.0f;
		m_modelMatrix = glm::rotate(m_modelMatrix, glm::radians(m_rotation[i]), axes);
		axes[i] = 0.0f;
	}

	m_modelMatrix = glm::translate(m_modelMatrix, m_position);

	RefreshChildrenMatrices();
}

//-------------------------------------------------------------------------------------------------
void SceneNode::AddChild(std::unique_ptr<SceneNode>&& child)
{
	child->CalculateModelMatrix(m_modelMatrix);
	m_children.push_back(std::move(child));
}

//-------------------------------------------------------------------------------------------------
void SceneNode::ChangePosition(const glm::vec3& delta)
{
	m_position += delta;
	m_modelMatrix = glm::translate(m_modelMatrix, delta);

	RefreshChildrenMatrices();
}

//-------------------------------------------------------------------------------------------------
void SceneNode::SetPosition(const glm::vec3& newPos)
{
	m_position = newPos;
	m_modelMatrix = glm::translate(m_modelMatrix, m_position);	//#FIXME: This doesn't work

	RefreshChildrenMatrices();
}

//-------------------------------------------------------------------------------------------------
void SceneNode::ChangeScale(const glm::vec3& delta)
{
	m_scale += delta;
	m_modelMatrix = glm::scale(m_scale);

	RefreshChildrenMatrices();
}

//-------------------------------------------------------------------------------------------------
void SceneNode::SetScale(const glm::vec3& delta)
{
	m_scale = delta;
	m_modelMatrix = glm::scale(m_scale);

	RefreshChildrenMatrices();
}

//-------------------------------------------------------------------------------------------------
void SceneNode::ChangeRotation(const glm::vec3& delta)
{
	m_rotation += delta;

	glm::vec3 axes;
	for (uint32_t i = 0; i < 3; ++i)
	{
		axes[i] = 1.0f;
		m_modelMatrix = glm::rotate(m_modelMatrix, glm::radians(delta[i]), axes);
		axes[i] = 0.0f;
	}

	RefreshChildrenMatrices();
}

//-------------------------------------------------------------------------------------------------
const SceneNode* SceneNode::ChildAtIndex(const uint32_t index) const
{
	if (index >= 0 && index < m_children.size())
	{
		return m_children[index].get();
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------------
SceneNode* SceneNode::ChildAtIndex(const uint32_t index)
{
	if (index >= 0 && index < m_children.size())
	{
		return m_children[index].get();
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------------
const glm::mat4& SceneNode::ModelMatrix() const
{
	return m_modelMatrix;
}

//-------------------------------------------------------------------------------------------------
const glm::vec3 SceneNode::Dimensions() const
{
	if (m_renderObject)
	{
		return m_renderObject->GetModel().Dimensions() * m_scale;
	}
	else
	{
		return glm::vec3{ 0.0f };
	}
}

//-------------------------------------------------------------------------------------------------
void SceneNode::SetVisiblity(const bool isVisible)
{
	m_isVisible = isVisible;
}

//-------------------------------------------------------------------------------------------------
void SceneNode::ToggleVisibility()
{
	m_isVisible = !m_isVisible;
}

//-------------------------------------------------------------------------------------------------
void SceneNode::RefreshChildrenMatrices()
{
	for (auto& child : m_children)
	{
		child->CalculateModelMatrix(m_modelMatrix);
	}
}

//-------------------------------------------------------------------------------------------------
void SceneNode::UpdateDescriptorSets(const Texture& texture)
{
	if (m_renderObject)
	{
		m_renderObject->UpdateDescriptorSets(texture);
	}
}

//-------------------------------------------------------------------------------------------------
void SceneNode::ChangeTexture(const Texture& texture)
{
	if (m_renderObject)
	{
		m_renderObject->ChangeTexture(texture);
	}
}

//-------------------------------------------------------------------------------------------------
void SceneNode::SetColliding(const bool isColliding)
{
	m_colour = isColliding ? glm::vec3{ 1.0f, 0.0f, 0.0f } : glm::vec3{ 0.0f, 0.0f, 1.0f };
}

//-------------------------------------------------------------------------------------------------
bool SceneNode::RayIntersectsAABB(Ray& ray) const
{
	float tMin = 0.001f;
	float tMax = std::numeric_limits<float>::max();

	const glm::vec3 min = m_position - (Dimensions() * 0.5f);
	const glm::vec3 max = m_position + (Dimensions() * 0.5f);

	for (int i = 0; i < 3; ++i)
	{
		//Are they parallel?
		if (std::abs(ray.m_dir[i]) < FLT_EPSILON)
		{
			if (ray.m_pos[i] < min[i] || ray.m_pos[i] > max[i])
			{
				return false;
			}
		}
		//Nope, they're not! They must collide with this plane at some point...
		else
		{
			const float ood = 1.0f / ray.m_dir[i];
			float t1 = (min[i] - ray.m_pos[i]) * ood;
			float t2 = (max[i] - ray.m_pos[i]) * ood;

			if (t1 > t2)
			{
				std::swap(t1, t2);
			}

			//The pattern should always be min, min, min, max, max max
			//If it doesn't, then it can't hit
			tMin = std::max(t1, tMin);
			tMax = std::min(t2, tMax);

			if (tMin > tMax)
			{
				return false;
			}
		}
	}

	ray.m_t = tMin;
	return true;
}