#pragma once

#include "Model.h"
#include "Animation.h"

#include <unordered_map>
#include <vector>
#include <glm/gtx/quaternion.hpp>


struct aiBone;
struct aiNode;
struct Bone
{
	std::string m_name;
	glm::mat4	m_transformation;
	glm::mat4	m_offset;
	const Bone*	m_parentBone;

	//Compiler won't generate a decent ctor apparently.... for some reason
	explicit Bone(const std::string& name, const glm::mat4& transform, const glm::mat4& offset) :
		m_name(name), m_transformation(transform), m_offset(offset), m_parentBone(nullptr) 
	{}

	explicit Bone(const std::string& name, const glm::mat4& transform, const glm::mat4& offset, const Bone* parentBone) :
		m_name(name), m_transformation(transform), m_offset(offset), m_parentBone(parentBone)
	{}
};

class AnimatedModel : public Model
{
protected:
	using BoneList = std::vector<const aiBone*>;

	void LoadSkeleton		(const aiNode& sceneRootNode, const BoneList& bones);
	void LoadBoneWeights	(const BoneList& bones);

	Animation					m_animation;
	mutable std::vector<Bone>	m_skeleton;
	glm::mat4					m_invGlobalTransform;

	std::vector<AnimVertex> m_vertices;

	virtual void ProcessMesh		(const aiMesh& mesh, const aiScene& scene) override;
	virtual void CreateVertexBuffer () override;

public:
	static constexpr int MAX_BONES = 50;

	 AnimatedModel(const std::string& filepath);
	~AnimatedModel();

	const Animation&				GetAnimation				() const { return m_animation; }
	const std::vector<Bone>&		GetSkeleton					() const { return m_skeleton; }
		  std::vector<glm::mat4>	GetSkeletonTransformsAtTime	(const double time) const;
};