#include <stb_image.h>

#include "Texture.h"
#include "VulkanCore.h"
#include "VulkanUtils.h"

Texture::Texture() :
	m_textureImage(VK_NULL_HANDLE),
	m_textureImageMemory(VK_NULL_HANDLE),
	m_textureImageView(VK_NULL_HANDLE),
	m_textureSampler(VK_NULL_HANDLE)
{
}

Texture::Texture(const std::string& texturePath) :
	m_textureImage(VK_NULL_HANDLE),
	m_textureImageMemory(VK_NULL_HANDLE),
	m_textureImageView(VK_NULL_HANDLE),
	m_textureSampler(VK_NULL_HANDLE)
{
	CreateTextureImage(texturePath);
	CreateTextureImageView();
	CreateTextureSampler(VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT);
}

Texture::Texture(VkImage image, VkDeviceMemory imageMemory, VkImageView imageView, VkSampler sampler) :
	m_textureImage(image),
	m_textureImageMemory(imageMemory),
	m_textureImageView(imageView),
	m_textureSampler(sampler)
{
}

Texture::Texture(Texture&& rhs)
{
	m_textureImage			= rhs.m_textureImage;
	m_textureImageMemory	= rhs.m_textureImageMemory;
	m_textureImageView		= rhs.m_textureImageView;
	m_textureSampler		= rhs.m_textureSampler;

	rhs.m_textureImage			= NULL;
	rhs.m_textureImageMemory	= NULL;
	rhs.m_textureImageView		= NULL;
	rhs.m_textureSampler		= NULL;
}

Texture::~Texture()
{
}

Texture Texture::operator= (Texture&& rhs)
{
	Texture t;
	t.m_textureImage = rhs.m_textureImage;
	t.m_textureImageMemory = rhs.m_textureImageMemory;
	t.m_textureImageView = rhs.m_textureImageView;
	t.m_textureSampler = rhs.m_textureSampler;

	rhs.m_textureImage = NULL;
	rhs.m_textureImageMemory = NULL;
	rhs.m_textureImageView = NULL;
	rhs.m_textureSampler = NULL;

	return t;
}

//-------------------------------------------------------------------------------------------------
bool Texture::CreateTextureImage(const std::string& texturePath)
{
	VkDevice& device = VulkanCore::Singleton().Device();

	//Load the image from disk
	int texW, texH, texChannels;
	stbi_uc* pixels = stbi_load(texturePath.c_str(), &texW, &texH, &texChannels, STBI_rgb_alpha);
	const uint32_t pixelSize = 4;
	const VkDeviceSize imageSize = texW * texH * pixelSize;

	if (pixels == nullptr)
	{
		printf("ERROR - Failed to load image %s! \n", texturePath.c_str());
		return false;
	}

	//Create staging buffer
	VulkanUtils::Buffer stagingBuffer(device);
	VulkanCore::Singleton().CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, stagingBuffer.m_buffer, stagingBuffer.m_memory);

	//Map data to image buffer
	void* data;
	vkMapMemory(device, stagingBuffer.m_memory, 0, imageSize, 0, &data);
	memcpy(data, pixels, (size_t)imageSize);
	vkUnmapMemory(device, stagingBuffer.m_memory);

	stbi_image_free(pixels);

	//Create Vulkan Image
	if (VulkanCore::Singleton().CreateImage(texW, texH, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_textureImage, m_textureImageMemory) == false)
	{
		printf("ERROR - Failed to create image from texture %s! \n", texturePath.c_str());
		return false;
	}

	//Transition to correct layout then copy from buffer
	VulkanCore::Singleton().TransitionImageLayout(m_textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	VulkanCore::Singleton().CopyBufferToImage(stagingBuffer.m_buffer, m_textureImage, texW, texH);
	VulkanCore::Singleton().TransitionImageLayout(m_textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

	return true;
}

//-------------------------------------------------------------------------------------------------
bool Texture::CreateTextureImageView()
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.format = VK_FORMAT_R8G8B8A8_SRGB;
	viewInfo.image = m_textureImage;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.layerCount = 1;

	VkDevice& device = VulkanCore::Singleton().Device();

	if (vkCreateImageView(VulkanCore::Singleton().Device(), &viewInfo, nullptr, &m_textureImageView) != VK_SUCCESS)
	{
		printf("ERROR - Failed to create image view for texture! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool Texture::CreateTextureSampler(const VkSamplerAddressMode addressMode)
{
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.addressModeU = addressMode;
	samplerInfo.addressModeV = addressMode;
	samplerInfo.addressModeW = addressMode;
	samplerInfo.anisotropyEnable = true;
	samplerInfo.maxAnisotropy = 16;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.compareEnable = false;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.unnormalizedCoordinates = false;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = 0.0f;

	if (vkCreateSampler(VulkanCore::Singleton().Device(), &samplerInfo, nullptr, &m_textureSampler) != VK_SUCCESS)
	{
		printf("ERROR - Failed to create sampler for texture \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
void Texture::CleanUp()
{
	VkDevice& device = VulkanCore::Singleton().Device();

	vkDestroySampler(device, m_textureSampler, nullptr);
	vkDestroyImageView(device, m_textureImageView, nullptr);
	vkFreeMemory(device, m_textureImageMemory, nullptr);
	vkDestroyImage(device, m_textureImage, nullptr);
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
CubeTexture::CubeTexture(const std::vector<std::string>& filepaths)
{
	LoadCubeTextures(filepaths);
	CreateTextureImageView();
	CreateTextureSampler(VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE);
}

CubeTexture::CubeTexture(CubeTexture&& rhs)
{
	m_textureImage = rhs.m_textureImage;
	m_textureImageMemory = rhs.m_textureImageMemory;
	m_textureImageView = rhs.m_textureImageView;
	m_textureSampler = rhs.m_textureSampler;

	rhs.m_textureImage = NULL;
	rhs.m_textureImageMemory = NULL;
	rhs.m_textureImageView = NULL;
	rhs.m_textureSampler = NULL;
}

CubeTexture::~CubeTexture()
{
}

//-------------------------------------------------------------------------------------------------
bool CubeTexture::CreateTextureImageView()
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
	viewInfo.image = m_textureImage;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
	viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.layerCount = 6;

	VkDevice& device = VulkanCore::Singleton().Device();

	if (vkCreateImageView(VulkanCore::Singleton().Device(), &viewInfo, nullptr, &m_textureImageView) != VK_SUCCESS)
	{
		printf("ERROR - Failed to create image view for texture! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool CubeTexture::LoadCubeTextures(const std::vector<std::string>& filepaths)
{
	VkDevice& device = VulkanCore::Singleton().Device();
	const uint32_t pixelSize = 4;

	for (size_t i = 0; i < filepaths.size(); ++i)
	{
		const std::string& path = filepaths[i];

		//Load the image from disk
		int texW, texH, texChannels;
		stbi_uc* pixels = stbi_load(path.c_str(), &texW, &texH, &texChannels, STBI_rgb_alpha);

		const VkDeviceSize imageSize = texW * texH * pixelSize;

		if (pixels == nullptr)
		{
			printf("ERROR - Failed to load image %s! \n", path.c_str());
			return false;
		}

		//------
		VkImageSubresourceLayers subLayer = {};
		subLayer.layerCount = 1;
		subLayer.baseArrayLayer = static_cast<uint32_t>(i);
		subLayer.mipLevel = 0;
		subLayer.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

		//-----
		VulkanUtils::Buffer stagingBuffer(device);
		VulkanCore::Singleton().CreateBuffer(imageSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, stagingBuffer.m_buffer, stagingBuffer.m_memory);

		void* data;
		vkMapMemory(device, stagingBuffer.m_memory, 0, imageSize, VK_NULL_HANDLE, &data);
		memcpy(data, pixels, (size_t)imageSize);
		vkUnmapMemory(device, stagingBuffer.m_memory);

		if (m_textureImage == VK_NULL_HANDLE)
		{
			if (VulkanCore::Singleton().CreateImage(texW, texH, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_textureImage, m_textureImageMemory, 1, 6) == false)
			{
				printf("ERROR - Failed to create image from texture %s! \n", path.c_str());
				return false;
			}
		}

		VulkanCore::Singleton().TransitionImageLayout(m_textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
		VulkanCore::Singleton().CopyBufferToImage(stagingBuffer.m_buffer, m_textureImage, texW, texH, subLayer);
		VulkanCore::Singleton().TransitionImageLayout(m_textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

		stbi_image_free(pixels);
	}

	return true;
}
