#pragma once

#include "Camera.h"

#include <vector>

class InterpolatedCamera : public Camera
{
public:
	struct InterpolateKeyFrame
	{
		glm::vec3	startPosition;
		glm::vec3	endPosition;
		glm::vec3	startRotation;
		glm::vec3	endRotation;
		float		length;
	};

protected:
	void GoToNextFrame();

	std::vector<InterpolateKeyFrame>	m_keyFrames;
	int									m_currentFrameIdx;
	float								m_currentLerpTime;
	bool								m_loopWhenFinished;

public:
	 InterpolatedCamera(const bool loopWhenFinished);
	~InterpolatedCamera();

	void Update(const double dt);

	//----- Utility Functions -----
	void AddKeyFrame(InterpolateKeyFrame&& frame);
	void StartInterpolation();
	void StopInterpolation ();

	void AddToImGui() override;

	//----- Getters -----
	bool IsInterpolating() const { return m_currentFrameIdx != -1; }

	//----- Setters -----
	void SetLooping(const bool loopWhenFinished) { m_loopWhenFinished = loopWhenFinished; }

	virtual void AddYaw		(const float delta) override;
	virtual void AddPitch	(const float delta) override;
	virtual void AddRoll	(const float delta) override;

	virtual void SetYaw		(const float newYaw) override;
	virtual void SetPitch	(const float newPitch) override;
	virtual void SetRoll	(const float newRoll) override;

	virtual void ChangePosition	(const glm::vec3& delta) override;
	virtual void SetPosition	(const glm::vec3& new_pos) override;

};