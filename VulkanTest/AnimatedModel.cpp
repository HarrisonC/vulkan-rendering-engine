#include "AnimatedModel.h"
#include "VulkanCore.h"

#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <numeric>
#include <deque>

#include <glm/gtc/matrix_transform.hpp>

namespace
{
	aiMesh* FindMesh(const aiScene& scene, const aiNode& node)
	{
		for (unsigned int curMesh = 0; curMesh < node.mNumMeshes; curMesh++)
		{
			return scene.mMeshes[node.mMeshes[curMesh]];
		}

		for (unsigned int child = 0; child < node.mNumChildren; child++)
		{
			if (aiMesh* mesh = FindMesh(scene, *node.mChildren[child]))
			{
				return mesh;
			}
		}

		return nullptr;
	};

	glm::mat4 aiMatrix4ToGlm(const aiMatrix4x4& from)
	{
		glm::mat4 tmp;
		tmp[0][0] = from.a1;
		tmp[1][0] = from.b1;
		tmp[2][0] = from.c1;
		tmp[3][0] = from.d1;
		
		tmp[0][1] = from.a2;
		tmp[1][1] = from.b2;
		tmp[2][1] = from.c2;
		tmp[3][1] = from.d2;
		
		tmp[0][2] = from.a3;
		tmp[1][2] = from.b3;
		tmp[2][2] = from.c3;
		tmp[3][2] = from.d3;
				
		tmp[0][3] = from.a4;
		tmp[1][3] = from.b4;
		tmp[2][3] = from.c4;
		tmp[3][3] = from.d4;
		return tmp;
	}
}

AnimatedModel::AnimatedModel(const std::string& filepath) :
	Model(filepath)
{
	//Load in the file - Flips the UV's and triangulate the verts
	Assimp::Importer importer;
	importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	//Other convenient post-processing : genNormals, SplitLargeMeshes, OptimizeMeshes
	const aiScene* scene = importer.ReadFile(filepath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace | aiProcess_OptimizeGraph);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		printf("FAILED TO LOAD MODEL : %s!", filepath.c_str());
		return;
	}

	//Load the verts
	ProcessNode(*scene->mRootNode, *scene);

	//Now load the bones
	const auto& rootNode = scene->mRootNode;
	BoneList bones;
	for (unsigned int m = 0; m < scene->mNumMeshes; ++m)
	{
		const aiMesh* currentMesh = scene->mMeshes[m];
		for (unsigned int b = 0; b < currentMesh->mNumBones; ++b)
		{
			bones.push_back(currentMesh->mBones[b]);
		}
	}

	//Interpret that data
	LoadSkeleton(*rootNode, bones);
	LoadBoneWeights(bones);
	m_animation.LoadAnimation(*scene);

	CreateVertexBuffer();
	CreateIndexBuffer();
}

AnimatedModel::~AnimatedModel()
{
}

//-------------------------------------------------------------------------------------------------
void AnimatedModel::LoadSkeleton(const aiNode& sceneRootNode, const BoneList& bones)
{
	//Find the root
	//We do this by looping through each bone and find if any have parent that doesn't exist
	//in our bone list already. When we do, that one must be the root
	const aiNode* rootNode = nullptr;
	for (const auto& bone : bones)
	{
		const aiNode* boneNode = sceneRootNode.FindNode(bone->mName);
		if (boneNode)
		{
			//If we find a bone that doesn't have a parent (by name) that is within our bone list, that must be the root
			if (std::find_if(bones.begin(), bones.end(), [&boneNode](const auto& bone) { return boneNode->mParent->mName == bone->mName; }) == bones.end())
			{
				rootNode = boneNode;
				break;
			}
		}
	}

	//Load the hierarchy from the root node
	if (rootNode)
	{
		auto FindIndexForNode = [&bones](const aiNode* node) -> int
		{
			const auto itr = std::find_if(bones.begin(), bones.end(), [&node](const aiBone* bone) { return bone->mName == node->mName; });
			if (itr != bones.end())
			{
				return std::distance(bones.begin(), itr);
			}

			printf("Failed to find bone for node, this should not happen! \n");
			return -1;
		};

		m_invGlobalTransform = glm::inverse(glm::transpose(aiMatrix4ToGlm(rootNode->mTransformation)));

		std::vector<int> parentIndices;
		parentIndices.reserve(bones.size());
		for (const auto& aBone : bones)
		{
			const aiNode* node = rootNode->mParent->FindNode(aBone->mName);
			int parentIdx = node == rootNode ? -1 : FindIndexForNode(node->mParent);
			parentIndices.push_back(parentIdx);

			m_skeleton.push_back(Bone(aBone->mName.C_Str(), glm::transpose(aiMatrix4ToGlm(node->mTransformation)), glm::transpose(aiMatrix4ToGlm(aBone->mOffsetMatrix))));
		}

		for (unsigned int p = 0; p < parentIndices.size(); ++p)
		{
			//This is dangerous... catching a ptr to an item in a vector. 
			//Bones should NEVER be changed after this so should be safe for now
			if (parentIndices[p] >= 0)
				m_skeleton[p].m_parentBone = &m_skeleton[parentIndices[p]];
		}
	}
	else
	{
		printf("Failed to find root bone for skeleton!\n");
	}
}

//-------------------------------------------------------------------------------------------------
void AnimatedModel::LoadBoneWeights(const BoneList& bones)
{
	auto FindMinElementIndex = [](const glm::vec4& v) 
	{  
		int lowest = 0;
		for (int i = 1; i < 4; ++i)
		{
			if (v[i] < v[lowest])
			{
				lowest = i;
			}
		}

		return lowest;
	};

	//Load all bone weights for verts
	//Some of these have more than 3-4 weights which may make them a pain
	for(unsigned int b = 0; b < bones.size(); ++b)
	{
		const auto& bone = bones[b];

		if (bone->mNumWeights > 4)
		{
			printf("Bone has more than 4 weights, only highest 4 will be kept \n");
		}

		for (unsigned int w = 0; w < bone->mNumWeights; ++w)
		{
			const aiVertexWeight& currentWeight = bone->mWeights[w];
			glm::vec4& vertexWeights = m_vertices[currentWeight.mVertexId].m_weights;

			int minIndex = FindMinElementIndex(vertexWeights);
			if (vertexWeights[minIndex] < currentWeight.mWeight)
			{
				vertexWeights[minIndex] = currentWeight.mWeight;
				m_vertices[currentWeight.mVertexId].m_boneIDs[minIndex] = b;
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void AnimatedModel::CreateVertexBuffer()
{
	VulkanUtils::Buffer	stagingBuffer(m_device);
	const VkDeviceSize	bufferSize = m_vertices.size() * sizeof(m_vertices[0]);

	//Create staging buffer
	VulkanCore::Singleton().CreateBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_COHERENT_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, stagingBuffer.m_buffer, stagingBuffer.m_memory);

	//Map data to staging buffer
	void* data;
	vkMapMemory(m_device, stagingBuffer.m_memory, 0, bufferSize, 0, &data);
	memcpy(data, m_vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(m_device, stagingBuffer.m_memory);

	//Create vertex buffer
	VulkanCore::Singleton().CreateBuffer(bufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, m_vertexBuffer.m_buffer, m_vertexBuffer.m_memory);

	//Copy the staging buffer to the local vertex buffer
	VulkanCore::Singleton().CopyBuffer(stagingBuffer.m_buffer, m_vertexBuffer.m_buffer, bufferSize);
}

//-------------------------------------------------------------------------------------------------
void AnimatedModel::ProcessMesh(const aiMesh& mesh, const aiScene& scene)
{
	glm::vec3 smallest{ std::numeric_limits<float>::max() };
	glm::vec3 largest{ -std::numeric_limits<float>::max() };

	//Process all the vertices
	for (unsigned int v = 0; v < mesh.mNumVertices; v++)
	{
		AnimVertex vert{};

		if (mesh.mNormals)
		{
			vert.m_normal = glm::normalize(glm::vec3(mesh.mNormals[v].x, mesh.mNormals[v].y, mesh.mNormals[v].z));
		}

		vert.m_pos = glm::vec3(mesh.mVertices[v].x, mesh.mVertices[v].y, mesh.mVertices[v].z);
		for (int i = 0; i < 3; ++i)
		{
			if (vert.m_pos[i] < smallest[i])
				smallest[i] = vert.m_pos[i];

			if (vert.m_pos[i] > largest[i])
				largest[i] = vert.m_pos[i];
		}

		vert.m_col = { 0.0f, 0.0f, 1.0f };

		if (mesh.mTextureCoords[0])
			vert.m_texCoords = glm::vec2(mesh.mTextureCoords[0][v].x, mesh.mTextureCoords[0][v].y);
		else
			vert.m_texCoords = glm::vec2(0.0f, 0.0f);

		m_vertices.push_back(std::move(vert));
	}

	//Process all the indices
	for (unsigned int i = 0; i < mesh.mNumFaces; i++)
	{
		aiFace face = mesh.mFaces[i];
		for (unsigned int ind = 0; ind < face.mNumIndices; ind++)
			m_indices.push_back(face.mIndices[ind]);
	}

	m_dimensions = largest - smallest;
}

//-------------------------------------------------------------------------------------------------
std::vector<glm::mat4> AnimatedModel::GetSkeletonTransformsAtTime(const double time) const
{
	std::vector<glm::mat4> finalTransforms;
	finalTransforms.reserve(MAX_BONES);

	std::unordered_map<std::string, glm::vec3> newPositions = m_animation.GetPositionPoseForTime(time);
	std::unordered_map<std::string, glm::quat> newRotations = m_animation.GetRotationPoseForTime(time);

	for (auto& bone : m_skeleton)
	{
		glm::mat4 T, R, S;
		bool isDirty = false;

		if (const auto& itr = newPositions.find(bone.m_name); itr != newPositions.end())
		{
			T = glm::translate(T, itr->second);
			isDirty = true;
		}
		if (const auto& itr = newRotations.find(bone.m_name); itr != newRotations.end())
		{
			R = glm::toMat4(itr->second);
			isDirty = true;
		}

		if(isDirty)
			bone.m_transformation = T * R * S;
	}

	//#TODO: This can be optimised very easily, we're calculating the same shit over and over
	auto FindParentTransform = [&](const Bone& bone) -> glm::mat4
	{
		std::deque<glm::mat4> parentTransforms;
		const Bone* currentBone = bone.m_parentBone;
		while (currentBone != nullptr)
		{
			parentTransforms.push_front(currentBone->m_transformation);
			currentBone = currentBone->m_parentBone;
		}

		glm::mat4 concatTransform;
		for (const auto& transform : parentTransforms)
		{
			concatTransform *= transform;
		}

		return concatTransform;
	};

	for (auto& bone : m_skeleton)
	{
		const glm::mat4 concatM = FindParentTransform(bone) * bone.m_transformation;
		finalTransforms.push_back(m_invGlobalTransform * concatM * bone.m_offset);
	}

	finalTransforms.resize(MAX_BONES);
	return finalTransforms;
}