#include "TextureManager.h"
#include "Texture.h"
#include "ImGui/imgui.h"

TextureManager::TextureManager()
{
}

TextureManager::~TextureManager()
{
}

//-------------------------------------------------------------------------------------------------
void TextureManager::LoadTextures()
{
	std::vector<std::string> cubePaths
	{ 
		"Assets/Skyboxes/Sweden/posx.jpg",
		"Assets/Skyboxes/Sweden/negx.jpg",
		"Assets/Skyboxes/Sweden/posy.jpg",
		"Assets/Skyboxes/Sweden/negy.jpg",
		"Assets/Skyboxes/Sweden/posz.jpg",
		"Assets/Skyboxes/Sweden/negz.jpg"
	};

	m_textures.reserve(16);
	m_textures.emplace("Assets/Textures/container2.png",		std::make_unique<Texture>( "Assets/Textures/container2.png" ));
	m_textures.emplace("Assets/Textures/cube_uv.png",			std::make_unique<Texture>( "Assets/Textures/cube_uv.png"));
	m_textures.emplace("Assets/Textures/cube_uv_solid.jpg",		std::make_unique<Texture>( "Assets/Textures/cube_uv_solid.jpg"));
	m_textures.emplace("Assets/Textures/sphere_uv.png",			std::make_unique<Texture>( "Assets/Textures/sphere_uv.png" ));
	m_textures.emplace("Assets/Textures/spaceship_uv.png",		std::make_unique<Texture>( "Assets/Textures/spaceship_uv.png" ));
	m_textures.emplace("Assets/Textures/character_texture.png", std::make_unique<Texture>("Assets/Textures/character_texture.png"));
	m_textures.emplace("Assets/Textures/grass.png",				std::make_unique<Texture>("Assets/Textures/grass.png"));

	m_textures.emplace("Assets/Models/Pilot/Material.002_Base_Color.png",	std::make_unique<Texture>("Assets/Models/Pilot/Material.002_Base_Color.png"));
	m_textures.emplace("Assets/Models/Stormtrooper/Stormtrooper_D.png",		std::make_unique<Texture>("Assets/Models/Stormtrooper/Stormtrooper_D.png"));
	m_textures.emplace("Assets/Models/Doom/doomx_Albedo.jpg",				std::make_unique<Texture>("Assets/Models/Doom/doomx_Albedo.jpg"));
	//m_textures.emplace("Assets/Textures/chalet.jpg",						std::make_unique<Texture>( "Assets/Textures/chalet.jpg" ));

	m_textures.emplace("cubemapTexture",									std::make_unique<CubeTexture>(cubePaths));
}

//-------------------------------------------------------------------------------------------------
const Texture* TextureManager::AddToImGui() const
{
	if (ImGui::BeginCombo("##Textures", nullptr))
	{
		for (const auto& texture : m_textures)
		{
			if (ImGui::Selectable(texture.first.c_str()))
			{
				ImGui::EndCombo();
				return texture.second.get();
			}
		}

		ImGui::EndCombo();
	}

	return nullptr;
}

//-------------------------------------------------------------------------------------------------
const Texture& TextureManager::GetTexture(const std::string& texturePath) const
{
	return *m_textures.at(texturePath);
}

//-------------------------------------------------------------------------------------------------
void TextureManager::CleanUp()
{
	for (auto& texture : m_textures)
	{
		texture.second->CleanUp();
	}
}