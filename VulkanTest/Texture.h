#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <vulkan/vulkan.h>
#include <string>
#include <vector>

struct Texture
{
protected:
	Texture();

	virtual bool CreateTextureImage		(const std::string& texturePath);
	virtual bool CreateTextureImageView	();
	virtual bool CreateTextureSampler	(const VkSamplerAddressMode addressMode);

public:
	void			CleanUp		();

	VkImage			m_textureImage;
	VkDeviceMemory	m_textureImageMemory;
	VkImageView		m_textureImageView;
	VkSampler		m_textureSampler;

	 Texture(const std::string& texturePath);
	 Texture(VkImage image, VkDeviceMemory imageMemory, VkImageView imageView, VkSampler sampler);
	 Texture(Texture&& rhs);
	~Texture();

	 Texture operator= (Texture&& rhs);

	 //Texture			(const Texture& rhs) = delete;
	 //Texture operator=	(const Texture& rhs) = delete;
};

struct CubeTexture : public Texture
{
protected:
	bool CreateTextureImageView	() override;
	bool LoadCubeTextures		(const std::vector<std::string>& filepaths);

public:
	CubeTexture(const std::vector<std::string>& filepaths);
	CubeTexture(CubeTexture&& rhs);
	~CubeTexture();

	CubeTexture(const CubeTexture& rhs) = delete;
};

#endif 

