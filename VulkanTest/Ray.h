#ifndef _RAY_H_
#define _RAY_H_

#include <glm/vec3.hpp>

struct Ray
{
	glm::vec3	m_pos;
	glm::vec3	m_dir;
	float		m_t;

	explicit Ray(const glm::vec3& pos, const glm::vec3& dir) :
		m_pos(pos), m_dir(dir), m_t(std::numeric_limits<float>::max())
	{
	}
};

#endif
