#include "Terrain.h"

#include "PerlinNoise.hpp"
#include "Vertex.h"
#include "Model.h"
#include <fstream>

namespace
{
	void CreatePPMForHeightMap(const std::vector<double>& data, const unsigned int width, const unsigned int height,  const std::string& filepath)
	{
		std::fstream file;
		file.open(filepath, std::ios::out);

		if (file.is_open())
		{
			file << "P3\n" << width << ' ' << height << "\n255\n";

			for (const auto& val : data)
			{
				const auto col = static_cast<int>(val * 255.99);
				file << col << ' ' << ' ' << col << ' ' << col << '\n';
			}

			file.close();
		}
	}
}

Terrain::Terrain(const unsigned int width, const unsigned int height) :
	m_width(width), 
	m_height(height),
	m_model()
{
}

Terrain::~Terrain()
{
}

//-------------------------------------------------------------------------------------------------
void Terrain::Initialise()
{
	siv::PerlinNoise perlin(12345);
	const double frequency = 8.0;
	const int32_t octaves = 8;

	const double fx = m_width / frequency;
	const double fy = m_height / frequency;

	std::vector<double> elevations;
	elevations.reserve(m_width * m_height);

	for (unsigned int y = 0; y < m_height; y++) 
	{
		for (unsigned int x = 0; x < m_width; x++)
		{
			elevations.push_back(perlin.normalizedOctaveNoise2D_0_1(x / fx, y / fy, octaves));
		}
	}

	CreatePPMForHeightMap(elevations, m_width, m_height, "Assets/HeightMaps/test1.ppm");
	
	//Build Vertices
	const double height = 10.0;

	std::vector<Vertex> verts;
	for (uint32_t x = 0; x < m_width; ++x)
	{
		for (uint32_t z = 0; z < m_height; ++z)
		{
			Vertex v;
			v.m_pos.x = (float)x;
			v.m_pos.y = static_cast<float>(height * elevations[x + z]);
			v.m_pos.z = (float)z;

			v.m_col = glm::vec3{ 1.0f };

			v.m_texCoords.x = static_cast<float>(x) / static_cast<float>(m_width - 1);
			v.m_texCoords.y = static_cast<float>(z) / static_cast<float>(m_height - 1);

			v.m_normal = glm::vec3{ 0.0f, -1.0f, 0.0 };

			verts.push_back(std::move(v));
		}
	}

	//Build Indices
	std::vector<uint32_t> indicies;

	uint32_t bottomLeft = 1;
	uint32_t currentRow = 0;
	while (bottomLeft < m_height)
	{
		for (; (bottomLeft + 4) < verts.size(); bottomLeft += 4)
		{
			indicies.push_back(bottomLeft);			//BL
			indicies.push_back(bottomLeft - 1);		//TL
			indicies.push_back(bottomLeft + 3);		//TR

			indicies.push_back(bottomLeft + 4);		//BR
			indicies.push_back(bottomLeft);			//BL
			indicies.push_back(bottomLeft + 3);		//TR
		}

		currentRow++;
		bottomLeft = currentRow + 1;
	}

	m_model = std::make_unique<StaticModel>(std::move(verts), std::move(indicies));
}