#ifndef _TIMER_H_
#define _TIMER_H_

#include <chrono>

class Timer
{
private:
	std::chrono::high_resolution_clock::time_point m_startTime;
	std::chrono::high_resolution_clock::time_point m_prevFrameTime;
	double m_deltaTime;

public:
	 Timer();
	~Timer();

	void	Update		();
	double	DeltaTime	() const;
};

#endif
