#include "Shader.h"

#include "VulkanCore.h"
#include <fstream>
#include <array>
#include "Camera.h"
#include "AnimatedModel.h"

Shader::Shader(const std::string& vertPath, const std::string& fragPath) :
	m_vertPath(vertPath),
	m_fragPath(fragPath),
	m_descriptorPool(NULL),
	m_descriptorLayout(NULL),
	m_vertShaderModule(NULL),
	m_fragShaderModule(NULL),
	m_pipeline(VK_NULL_HANDLE),
	m_pipelineLayout(VK_NULL_HANDLE)
{
	m_shaderStageInfos[0] = {};
	m_shaderStageInfos[1] = {};
}

Shader::~Shader()
{
	//CleanUp();
}

//-------------------------------------------------------------------------------------------------
void Shader::CreateShaders()
{
	std::vector<char> vertexShaderCode;
	std::vector<char> fragShaderCode;

	LoadShader(vertexShaderCode, m_vertPath);
	LoadShader(fragShaderCode, m_fragPath);

	m_vertShaderModule = CreateVkShaderModule(vertexShaderCode);
	m_fragShaderModule = CreateVkShaderModule(fragShaderCode);

	m_shaderStageInfos[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	m_shaderStageInfos[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
	m_shaderStageInfos[0].module = m_vertShaderModule;
	m_shaderStageInfos[0].pName = "main";
					 
	m_shaderStageInfos[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	m_shaderStageInfos[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	m_shaderStageInfos[1].module = m_fragShaderModule;
	m_shaderStageInfos[1].pName = "main";
}

//-------------------------------------------------------------------------------------------------
bool Shader::LoadShader(std::vector<char>& shaderCode, const std::string& filepath)
{
	std::ifstream file(filepath.c_str(), std::ios::ate | std::ios::binary);

	if (file.is_open() == false)
	{
		printf("FAILED TO OPEN SHADER FILE : %s \n", filepath.c_str());
		return false;
	}

	size_t fileSize = (size_t)file.tellg();
	shaderCode.resize(fileSize);

	file.seekg(0);
	file.read(shaderCode.data(), fileSize);

	file.close();

	return true;
}

//-------------------------------------------------------------------------------------------------
VkShaderModule Shader::CreateVkShaderModule(const std::vector<char>& shaderCode)
{
	VkShaderModuleCreateInfo createInfo{};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.pNext = nullptr;
	createInfo.codeSize = shaderCode.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(shaderCode.data());

	VkShaderModule newShader;
	if (vkCreateShaderModule(VulkanCore::Singleton().Device(), &createInfo, nullptr, &newShader) != VK_SUCCESS)
	{
		printf("FAILED TO COMPILE SHADER - WILL PROBABLY CRASH IF USED! ");
	}

	return newShader;
}

//-------------------------------------------------------------------------------------------------
void Shader::SetPipelineInfo(const VkPipelineLayout& layout, const VkPipeline& pipeline)
{
	m_pipelineLayout = layout;
	m_pipeline = pipeline;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
ImGuiShader::ImGuiShader(const std::string& vertPath, const std::string& fragPath) :
	Shader(vertPath, fragPath)
{
}

ImGuiShader::~ImGuiShader()
{
	CleanUp();
}

//-------------------------------------------------------------------------------------------------
void ImGuiShader::Initialise()
{
	CreateShaders();
	CreateDescriptorPools();
	CreateDescriptorSetLayout();
	CreateUniformBuffers();
}

//-------------------------------------------------------------------------------------------------
void ImGuiShader::CreateDescriptorPools()
{
	std::vector<VkDescriptorPoolSize> poolSizes = 
	{
		{ VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
	};

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = (uint32_t)poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 1;

	if (vkCreateDescriptorPool(VulkanCore::Singleton().Device(), &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor pool! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void ImGuiShader::CleanUpForSwapchainRecreation()
{
	vkDestroyDescriptorPool(VulkanCore::Singleton().Device(), m_descriptorPool, nullptr);
}

//-------------------------------------------------------------------------------------------------
void ImGuiShader::CleanUp()
{
	CleanUpForSwapchainRecreation();

	vkDestroyShaderModule(VulkanCore::Singleton().Device(), m_fragShaderModule, nullptr);
	vkDestroyShaderModule(VulkanCore::Singleton().Device(), m_vertShaderModule, nullptr);
	vkDestroyDescriptorSetLayout(VulkanCore::Singleton().Device(), m_descriptorLayout, nullptr);
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
VPShader::VPShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera) :
	Shader(vertPath, fragPath),
	m_camera(camera)
{
}

VPShader::~VPShader()
{
	CleanUp();
}

//-------------------------------------------------------------------------------------------------
void VPShader::Initialise()
{
	CreateShaders();
	CreateDescriptorPools();
	CreateDescriptorSetLayout();
	CreateUniformBuffers();
}

//-------------------------------------------------------------------------------------------------
void VPShader::CreateUniformBuffers()
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();
	const VkDeviceSize vpBufferSize = sizeof(VP);

	m_vpUniformBuffers.resize(swapchainImageCount);
	m_vpUniformMemories.resize(swapchainImageCount);

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VulkanCore::Singleton().CreateBuffer(vpBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_vpUniformBuffers[i], m_vpUniformMemories[i]);
	}
}

//-------------------------------------------------------------------------------------------------
void VPShader::UpdateUniformBuffers(const uint32_t currentImage)
{
	const glm::vec2 extents = VulkanCore::Singleton().WindowExtents();
	const VkDevice& device = VulkanCore::Singleton().Device();
	const uint32_t vpSize = sizeof(VP);

	VP vp;
	vp.view = m_camera.GetViewMatrix(); //glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	vp.proj = m_camera.GetProjectionMatrix();
	vp.proj[1][1] *= -1;

	void* data;
	vkMapMemory(device, m_vpUniformMemories[currentImage], 0, vpSize, 0, &data);
	memcpy(data, &vp, vpSize);
	vkUnmapMemory(device, m_vpUniformMemories[currentImage]);
}

//-------------------------------------------------------------------------------------------------
void VPShader::CreateDescriptorPools()
{
	//FIX ME LATER!
	std::array<VkDescriptorPoolSize, 3> poolSizes = {};
	//VP
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = 200;
	//Material
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[1].descriptorCount = 200;
	//Texture
	poolSizes[2].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[2].descriptorCount = 200;

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = (uint32_t)poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 200;

	if (vkCreateDescriptorPool(VulkanCore::Singleton().Device(), &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor pool! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void VPShader::CreateDescriptorSetLayout()
{
	VkDescriptorSetLayoutBinding vpLayoutBinding = {};
	vpLayoutBinding.binding = 0;
	vpLayoutBinding.descriptorCount = 1;
	vpLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	vpLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding materialLayoutBinding = {};
	materialLayoutBinding.binding = 1;
	materialLayoutBinding.descriptorCount = 1;
	materialLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	materialLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
	samplerLayoutBinding.binding = 2;
	samplerLayoutBinding.descriptorCount = 1;
	samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerLayoutBinding.pImmutableSamplers = nullptr;
	samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	std::array<VkDescriptorSetLayoutBinding, 3> layoutBindings = { vpLayoutBinding, materialLayoutBinding, samplerLayoutBinding };
	VkDescriptorSetLayoutCreateInfo descriptorSetInfo = {};
	descriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetInfo.bindingCount = (uint32_t)layoutBindings.size();
	descriptorSetInfo.pBindings = layoutBindings.data();

	if (vkCreateDescriptorSetLayout(VulkanCore::Singleton().Device(), &descriptorSetInfo, nullptr, &m_descriptorLayout) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor set layout, uniform variables in shader will NOT work! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void VPShader::CleanUpForSwapchainRecreation()
{
	for (int32_t i = 0; i < VulkanCore::Singleton().SwampchainImageCount(); ++i)
	{
		vkFreeMemory(VulkanCore::Singleton().Device(), m_vpUniformMemories[i], nullptr);
		vkDestroyBuffer(VulkanCore::Singleton().Device(), m_vpUniformBuffers[i], nullptr);
	}
	vkDestroyDescriptorPool(VulkanCore::Singleton().Device(), m_descriptorPool, nullptr);
}

//-------------------------------------------------------------------------------------------------
void VPShader::CleanUp()
{
	CleanUpForSwapchainRecreation();

	vkDestroyShaderModule(VulkanCore::Singleton().Device(), m_fragShaderModule, nullptr);
	vkDestroyShaderModule(VulkanCore::Singleton().Device(), m_vertShaderModule, nullptr);
	vkDestroyDescriptorSetLayout(VulkanCore::Singleton().Device(), m_descriptorLayout, nullptr);
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
VPLightShader::VPLightShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera, const LightManager& lightManager) :
	VPShader(vertPath, fragPath, camera),
	m_lightManager(lightManager)
{
}

VPLightShader::~VPLightShader()
{
	CleanUp();
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::Initialise()
{
	CreateShaders();
	CreateDescriptorPools();
	CreateDescriptorSetLayout();
	CreateUniformBuffers();
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::UpdateUniformBuffers(const uint32_t currentImage)
{
	const glm::vec2 extents = VulkanCore::Singleton().WindowExtents();
	const VkDevice& device = VulkanCore::Singleton().Device();
	const uint32_t vpSize = sizeof(VP);

	VP vp;
	vp.view = m_camera.GetViewMatrix(); //glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	vp.proj = m_camera.GetProjectionMatrix();
	vp.proj[1][1] *= -1;

	void* data;
	vkMapMemory(device, m_vpUniformMemories[currentImage], 0, vpSize, 0, &data);
	memcpy(data, &vp, vpSize);
	vkUnmapMemory(device, m_vpUniformMemories[currentImage]);

	void* directionalLightData;
	vkMapMemory(device, m_directionalLightUniformMemories[currentImage], 0, sizeof(DirectionalLight) * LightManager::MAX_DIRECTIONAL_LIGHTS, 0, &directionalLightData);
	memcpy(directionalLightData, &m_lightManager.GetAllDirectionalLights(), sizeof(DirectionalLight) * LightManager::MAX_DIRECTIONAL_LIGHTS);
	vkUnmapMemory(device, m_directionalLightUniformMemories[currentImage]);

	void* pointLightData;
	vkMapMemory(device, m_pointLightUniformMemories[currentImage], 0, sizeof(PointLight) * LightManager::MAX_POINT_LIGHTS, 0, &pointLightData);
	memcpy(pointLightData, &m_lightManager.GetAllPointLights(), sizeof(PointLight) * LightManager::MAX_POINT_LIGHTS);
	vkUnmapMemory(device, m_pointLightUniformMemories[currentImage]);
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::CreateUniformBuffers()
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();
	const VkDeviceSize vpBufferSize = sizeof(VP);
	const VkDeviceSize directionLightBufferSize = sizeof(DirectionalLight) * LightManager::MAX_DIRECTIONAL_LIGHTS;
	const VkDeviceSize pointLightBufferSize = sizeof(PointLight) * LightManager::MAX_POINT_LIGHTS;

	m_vpUniformBuffers.resize(swapchainImageCount);
	m_vpUniformMemories.resize(swapchainImageCount);

	m_directionalLightUniformBuffers.resize(swapchainImageCount);
	m_directionalLightUniformMemories.resize(swapchainImageCount);

	m_pointLightUniformBuffers.resize(swapchainImageCount);
	m_pointLightUniformMemories.resize(swapchainImageCount);

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VulkanCore::Singleton().CreateBuffer(vpBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_vpUniformBuffers[i], m_vpUniformMemories[i]);
		VulkanCore::Singleton().CreateBuffer(directionLightBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_directionalLightUniformBuffers[i], m_directionalLightUniformMemories[i]);
		VulkanCore::Singleton().CreateBuffer(pointLightBufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_pointLightUniformBuffers[i], m_pointLightUniformMemories[i]);
	}
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::CreateDescriptorPools()
{
	//FIX ME LATER!
	std::array<VkDescriptorPoolSize, 5> poolSizes = {};
	//VP
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = 1;
	//Directional Lights
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[1].descriptorCount = 1;
	//Point Lights
	poolSizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[2].descriptorCount = 1;
	//Material
	poolSizes[3].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[3].descriptorCount = 1;
	//Texture
	poolSizes[4].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[4].descriptorCount = 1;

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = (uint32_t)poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 200;

	if (vkCreateDescriptorPool(VulkanCore::Singleton().Device(), &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor pool! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::CreateDescriptorSetLayout()
{
	VkDescriptorSetLayoutBinding vpLayoutBinding = {};
	vpLayoutBinding.binding = 0;
	vpLayoutBinding.descriptorCount = 1;
	vpLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	vpLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding directionalLightLayoutBinding = {};
	directionalLightLayoutBinding.binding = 1;
	directionalLightLayoutBinding.descriptorCount = 1;
	directionalLightLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	directionalLightLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding pointLightLayoutBinding = {};
	pointLightLayoutBinding.binding = 2;
	pointLightLayoutBinding.descriptorCount = 1;
	pointLightLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	pointLightLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding materialLayoutBinding = {};
	materialLayoutBinding.binding = 3;
	materialLayoutBinding.descriptorCount = 1;
	materialLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	materialLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
	samplerLayoutBinding.binding = 4;
	samplerLayoutBinding.descriptorCount = 1;
	samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerLayoutBinding.pImmutableSamplers = nullptr;
	samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	std::array<VkDescriptorSetLayoutBinding, 5> layoutBindings = { vpLayoutBinding, directionalLightLayoutBinding, pointLightLayoutBinding, materialLayoutBinding, samplerLayoutBinding };
	VkDescriptorSetLayoutCreateInfo descriptorSetInfo = {};
	descriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetInfo.bindingCount = (uint32_t)layoutBindings.size();
	descriptorSetInfo.pBindings = layoutBindings.data();

	if (vkCreateDescriptorSetLayout(VulkanCore::Singleton().Device(), &descriptorSetInfo, nullptr, &m_descriptorLayout) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor set layout, uniform variables in shader will NOT work! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::CleanUpForSwapchainRecreation()
{
	for (int32_t i = 0; i < VulkanCore::Singleton().SwampchainImageCount(); ++i)
	{
		vkFreeMemory(VulkanCore::Singleton().Device(), m_pointLightUniformMemories[i], nullptr);
		vkDestroyBuffer(VulkanCore::Singleton().Device(), m_pointLightUniformBuffers[i], nullptr);

		vkFreeMemory(VulkanCore::Singleton().Device(), m_directionalLightUniformMemories[i], nullptr);
		vkDestroyBuffer(VulkanCore::Singleton().Device(), m_directionalLightUniformBuffers[i], nullptr);
	}

	m_pointLightUniformMemories.clear();
	m_pointLightUniformBuffers.clear();
	m_directionalLightUniformMemories.clear();
	m_directionalLightUniformBuffers.clear();
}

//-------------------------------------------------------------------------------------------------
void VPLightShader::CleanUp()
{
	CleanUpForSwapchainRecreation();
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
AnimLightShader::AnimLightShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera, const LightManager& lightManager) :
	VPLightShader(vertPath, fragPath, camera, lightManager)
{
}

AnimLightShader::~AnimLightShader()
{
}

//-------------------------------------------------------------------------------------------------
void AnimLightShader::CreateDescriptorPools()
{
	//FIX ME LATER!
	std::array<VkDescriptorPoolSize, 6> poolSizes = {};
	//VP
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = 1;
	//Directional Lights
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[1].descriptorCount = 1;
	//Point Lights
	poolSizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[2].descriptorCount = 1;
	//Material
	poolSizes[3].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[3].descriptorCount = 1;
	//Texture
	poolSizes[4].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[4].descriptorCount = 1;
	//Bone Transforms
	poolSizes[5].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[5].descriptorCount = 1;

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = (uint32_t)poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 200;

	if (vkCreateDescriptorPool(VulkanCore::Singleton().Device(), &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor pool! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void AnimLightShader::CreateDescriptorSetLayout()
{
	VkDescriptorSetLayoutBinding vpLayoutBinding = {};
	vpLayoutBinding.binding = 0;
	vpLayoutBinding.descriptorCount = 1;
	vpLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	vpLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding directionalLightLayoutBinding = {};
	directionalLightLayoutBinding.binding = 1;
	directionalLightLayoutBinding.descriptorCount = 1;
	directionalLightLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	directionalLightLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding pointLightLayoutBinding = {};
	pointLightLayoutBinding.binding = 2;
	pointLightLayoutBinding.descriptorCount = 1;
	pointLightLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	pointLightLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding materialLayoutBinding = {};
	materialLayoutBinding.binding = 3;
	materialLayoutBinding.descriptorCount = 1;
	materialLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	materialLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding samplerLayoutBinding = {};
	samplerLayoutBinding.binding = 4;
	samplerLayoutBinding.descriptorCount = 1;
	samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	samplerLayoutBinding.pImmutableSamplers = nullptr;
	samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding boneTransformsLayoutBinding = {};
	boneTransformsLayoutBinding.binding = 5;
	boneTransformsLayoutBinding.descriptorCount = 1;
	boneTransformsLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	boneTransformsLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	std::array<VkDescriptorSetLayoutBinding, 6> layoutBindings = { vpLayoutBinding, directionalLightLayoutBinding, pointLightLayoutBinding, materialLayoutBinding, samplerLayoutBinding, boneTransformsLayoutBinding };
	VkDescriptorSetLayoutCreateInfo descriptorSetInfo = {};
	descriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetInfo.bindingCount = (uint32_t)layoutBindings.size();
	descriptorSetInfo.pBindings = layoutBindings.data();

	if (vkCreateDescriptorSetLayout(VulkanCore::Singleton().Device(), &descriptorSetInfo, nullptr, &m_descriptorLayout) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor set layout, uniform variables in shader will NOT work! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
/*WaterShader::WaterShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera, const Light& light) :
	VPLightShader(vertPath, fragPath, camera, light)
{
	CreateDescriptorPools();
	CreateDescriptorSetLayout();
}

WaterShader::~WaterShader()
{
}

//-------------------------------------------------------------------------------------------------
void WaterShader::CreateDescriptorPools()
{
	//FIX ME LATER!
	std::array<VkDescriptorPoolSize, 4> poolSizes = {};
	//VP
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = 200;
	//Light
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[1].descriptorCount = 200;
	//Material
	poolSizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[2].descriptorCount = 200;
	//Reflection Texture
	poolSizes[3].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[3].descriptorCount = 200;
	//Refraction Texture
	//poolSizes[4].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	//poolSizes[4].descriptorCount = 200;

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = (uint32_t)poolSizes.size();
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 200;

	if (vkCreateDescriptorPool(VulkanCore::Singleton().Device(), &poolInfo, nullptr, &m_descriptorPool) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor pool! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void WaterShader::CreateDescriptorSetLayout()
{
	VkDescriptorSetLayoutBinding vpLayoutBinding = {};
	vpLayoutBinding.binding = 0;
	vpLayoutBinding.descriptorCount = 1;
	vpLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	vpLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	VkDescriptorSetLayoutBinding lightLayoutBinding = {};
	lightLayoutBinding.binding = 1;
	lightLayoutBinding.descriptorCount = 1;
	lightLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	lightLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding materialLayoutBinding = {};
	materialLayoutBinding.binding = 2;
	materialLayoutBinding.descriptorCount = 1;
	materialLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	materialLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	VkDescriptorSetLayoutBinding reflectionLayoutBinding = {};
	reflectionLayoutBinding.binding = 3;
	reflectionLayoutBinding.descriptorCount = 1;
	reflectionLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	reflectionLayoutBinding.pImmutableSamplers = nullptr;
	reflectionLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	//VkDescriptorSetLayoutBinding refractionLayoutBinding = {};
	//refractionLayoutBinding.binding = 4;
	//refractionLayoutBinding.descriptorCount = 1;
	//refractionLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	//refractionLayoutBinding.pImmutableSamplers = nullptr;
	//refractionLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

	//std::array<VkDescriptorSetLayoutBinding, 5> layoutBindings = { vpLayoutBinding, lightLayoutBinding, materialLayoutBinding, reflectionLayoutBinding, refractionLayoutBinding };
	std::array<VkDescriptorSetLayoutBinding, 4> layoutBindings = { vpLayoutBinding, lightLayoutBinding, materialLayoutBinding, reflectionLayoutBinding };
	VkDescriptorSetLayoutCreateInfo descriptorSetInfo = {};
	descriptorSetInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	descriptorSetInfo.bindingCount = (uint32_t)layoutBindings.size();
	descriptorSetInfo.pBindings = layoutBindings.data();

	if (vkCreateDescriptorSetLayout(VulkanCore::Singleton().Device(), &descriptorSetInfo, nullptr, &m_descriptorLayout) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to create descriptor set layout, uniform variables in shader will NOT work! \n");
		return;
	}
}*/