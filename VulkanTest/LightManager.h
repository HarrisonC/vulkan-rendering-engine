#ifndef _LIGHT_MANAGER_H_
#define _LIGHT_MANAGER_H_

#include "Light.h"
#include <array>

class LightManager
{
public:
	static constexpr unsigned int MAX_DIRECTIONAL_LIGHTS = 4;	//Has to match shader code value!
	static constexpr unsigned int MAX_POINT_LIGHTS = 12;		//Has to match shader code value!
private:
	using DIRECTIONAL_LIGHT_LIST = std::array<DirectionalLight, MAX_DIRECTIONAL_LIGHTS>;
	using POINT_LIGHT_LIST = std::array<PointLight, MAX_POINT_LIGHTS>;

	DIRECTIONAL_LIGHT_LIST	m_directionalLights;
	POINT_LIGHT_LIST		m_pointLights;

public:
	 LightManager();
	~LightManager();

	//Should this be public though?
	void LoadLights();
	void AddToImGui();

	//Getters/Setters etc
	const DirectionalLight* GetDirectionalLight	(const unsigned int idx) const;
	const PointLight*		GetPointLight		(const unsigned int idx) const;

	const DIRECTIONAL_LIGHT_LIST&	GetAllDirectionalLights	() const { return m_directionalLights; }
	const POINT_LIGHT_LIST&			GetAllPointLights		() const { return m_pointLights; }
};

#endif
