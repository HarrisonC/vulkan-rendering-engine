#ifndef _SHADER_H_
#define _SHADER_H_

#include <vulkan/vulkan.h>
#include <vector>
#include <string>
#include <glm/mat4x4.hpp>

class LightManager;
class  Camera;

class Shader
{
	friend class VulkanCore;
	friend class CubeMap;

protected:
	//----- Init Functions -----
	virtual void Initialise					() = 0;
	virtual void CreateShaders				();
	virtual void CreateDescriptorPools		() = 0;
	virtual void CreateDescriptorSetLayout	() = 0;
	virtual void CreateUniformBuffers		() = 0;

	//----- Util Functions -----
	bool				LoadShader			 (std::vector<char>& shaderCode, const std::string& filepath);
	VkShaderModule		CreateVkShaderModule (const std::vector<char>& shaderCode);

	std::string						m_vertPath;
	std::string						m_fragPath;
	VkPipelineShaderStageCreateInfo m_shaderStageInfos[2];
	VkShaderModule					m_vertShaderModule;
	VkShaderModule					m_fragShaderModule;

	VkDescriptorPool				m_descriptorPool;
	VkDescriptorSetLayout			m_descriptorLayout;

	VkPipeline						m_pipeline;
	VkPipelineLayout				m_pipelineLayout;

public:
			 Shader(const std::string& vertPath, const std::string& fragPath);
	virtual ~Shader();

	//----- Maintenance Functions -----
	virtual void UpdateUniformBuffers(const uint32_t currentImage) = 0;
			void SetPipelineInfo(const VkPipelineLayout& layout, const VkPipeline& pipeline);

	//These should not be pures, wtf am I doing...
	//Cbf to fix this now but these should just clean up what Shader holds (modules and pools)
	virtual void CleanUp						() = 0;
	virtual void CleanUpForSwapchainRecreation	() = 0;

	//----- Getter Functions -----
	const VkDescriptorPool&			GetDescriptorPool		() const { return m_descriptorPool; }
	const VkDescriptorSetLayout&	GetDescriptorSetLayout	() const { return m_descriptorLayout; }
	const VkPipelineLayout			GetPipelineLayout		() const { return m_pipelineLayout; }
	const VkPipeline				GetPipeline				() const { return m_pipeline; }
};

class ImGuiShader : public Shader
{
protected:
	virtual void CreateDescriptorPools		() override;
	virtual void CreateDescriptorSetLayout	() override {}
	virtual void CreateUniformBuffers		() override {}

public:
	ImGuiShader(const std::string& vertPath, const std::string& fragPath);
	virtual ~ImGuiShader();

	virtual void UpdateUniformBuffers(const uint32_t currentImage) override {}

	virtual void Initialise						() override;
	virtual void CleanUp						() override;
	virtual void CleanUpForSwapchainRecreation	() override;
};

class VPShader : public Shader
{
	friend class CubeMap;

public:
	struct VP
	{
		glm::mat4 view;
		glm::mat4 proj;
	};

protected:

	virtual void CreateDescriptorPools		() override;
	virtual void CreateDescriptorSetLayout	() override;
	virtual void CreateUniformBuffers		() override;

	std::vector<VkBuffer>			m_vpUniformBuffers;
	std::vector<VkDeviceMemory>		m_vpUniformMemories;

	const Camera&					m_camera;

public:
			 VPShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera);
	virtual ~VPShader();

	virtual void UpdateUniformBuffers(const uint32_t currentImage) override;

	virtual void Initialise						() override;
	virtual void CleanUp						() override;
	virtual void CleanUpForSwapchainRecreation	() override;

	//----- Getters -----
	const std::vector<VkBuffer>&		GetVpUniformBuffers	() const { return m_vpUniformBuffers; }
};

class VPLightShader : public VPShader
{
protected:
	virtual void CreateDescriptorPools		() override;
	virtual void CreateDescriptorSetLayout	() override;
	virtual void CreateUniformBuffers		() override;

	std::vector<VkBuffer>			m_directionalLightUniformBuffers;
	std::vector<VkDeviceMemory>		m_directionalLightUniformMemories;

	std::vector<VkBuffer>			m_pointLightUniformBuffers;
	std::vector<VkDeviceMemory>		m_pointLightUniformMemories;

	const LightManager&				m_lightManager;

public:
			 VPLightShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera, const LightManager& light);
	virtual ~VPLightShader();

	virtual void UpdateUniformBuffers(const uint32_t currentImage) override;

	virtual void Initialise						() override;
	virtual void CleanUp						() override;
	virtual void CleanUpForSwapchainRecreation	() override;

	//----- Getters -----
	const std::vector<VkBuffer>&		GetDirectionalLightBuffers	() const { return m_directionalLightUniformBuffers; } 
	const std::vector<VkBuffer>&		GetPointLightBuffers		() const { return m_pointLightUniformBuffers; }
};

class AnimLightShader : public VPLightShader
{
protected:
	virtual void CreateDescriptorPools		() override;
	virtual void CreateDescriptorSetLayout	() override;

public:
	AnimLightShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera, const LightManager& light);
	virtual ~AnimLightShader();
};

/*class WaterShader : public VPLightShader
{
	friend class RenderObject;

protected:
	virtual void CreateDescriptorPools		() override;
	virtual void CreateDescriptorSetLayout	() override;

public:
				 WaterShader(const std::string& vertPath, const std::string& fragPath, const Camera& camera, const Light& light);
	virtual		 ~WaterShader();

	//virtual void UpdateUniformBuffers(const uint32_t currentImage) override;
};*/

#endif
