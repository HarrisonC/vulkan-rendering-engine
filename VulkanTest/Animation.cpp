#include "Animation.h"

#include <assimp/scene.h>

//-------------------------------------------------------------------------------------------------
void Animation::LoadAnimation(const aiScene& scene)
{
	if (scene.HasAnimations())
	{
		//for (unsigned int a = 0; a < scene.mNumAnimations; ++a)
		{
			const aiAnimation* anim = scene.mAnimations[0];
	
			m_duration = anim->mDuration;
			m_ticksPerSec = anim->mTicksPerSecond;

			//If the ticks per second is over one, assume we're using frames and not MS for units
			if (m_ticksPerSec > 1.0)
			{
				m_isUsingFrames = true;
				m_duration = ConvertFrameToMS(m_duration);
			}

			for (unsigned int c = 0; c < anim->mNumChannels; ++c)
			{
				const aiNodeAnim* animNode = anim->mChannels[c];
				LoadPositionKeyFrames(*animNode);
				LoadRotationKeyFrames(*animNode);
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
void Animation::LoadPositionKeyFrames(const aiNodeAnim& animNode)
{
	const char* name = animNode.mNodeName.C_Str();
	for (unsigned int k = 0; k < animNode.mNumPositionKeys; ++k)
	{
		double frameTime = animNode.mPositionKeys[k].mTime;
		if (m_isUsingFrames)
		{
			frameTime = ConvertFrameToMS(frameTime);
		}

		const aiVector3D& aiPos = animNode.mPositionKeys[k].mValue;
		glm::vec3 pos{ aiPos.x, aiPos.y, aiPos.z };

		m_positionKeyFrames[frameTime].insert({ name, pos });
	}
}

//-------------------------------------------------------------------------------------------------
void Animation::LoadRotationKeyFrames(const aiNodeAnim& animNode)
{
	const char*name = animNode.mNodeName.C_Str();
	for (unsigned int k = 0; k < animNode.mNumRotationKeys; ++k)
	{
		double frameTime = animNode.mRotationKeys[k].mTime;
		if (m_isUsingFrames)
		{
			frameTime = ConvertFrameToMS(frameTime);
		}

		const aiQuaternion& aiRot = animNode.mRotationKeys[k].mValue;
		glm::quat rot{ aiRot.w, aiRot.x, aiRot.y, aiRot.z };

		m_rotationKeyFrames[frameTime].insert({ name, rot });
	}
}

//-------------------------------------------------------------------------------------------------
std::unordered_map<std::string, glm::vec3> Animation::GetPositionPoseForTime(const double time) const
{
	std::unordered_map<std::string, glm::vec3> transforms;

	for (auto itr = m_positionKeyFrames.begin(); itr != m_positionKeyFrames.end(); itr++)
	{
		if (itr->first > time)
		{
			const auto& nextFrame = *itr;
			const auto& currentFrame = (*--itr);
			const double factor = (time - currentFrame.first) / (nextFrame.first - currentFrame.first);

			for (const auto& [name, pos] : currentFrame.second)
			{
				if (const auto& nextFramePose = nextFrame.second.find(name); nextFramePose != nextFrame.second.end())
					transforms[name] = glm::mix(pos, nextFramePose->second, factor);
				else
					transforms[name] = pos;
			}

			return transforms;
		}
	}

	return transforms;
}

//-------------------------------------------------------------------------------------------------
std::unordered_map<std::string, glm::quat> Animation::GetRotationPoseForTime(const double time) const
{
	std::unordered_map<std::string, glm::quat> transforms;

	for (auto itr = m_rotationKeyFrames.begin(); itr != m_rotationKeyFrames.end(); itr++)
	{
		if (itr->first > time)
		{
			const auto& nextFrame = *itr;
			const auto& currentFrame = (*--itr);
			//This HAS to be a float for the slerp function, no idea why as it says it can be a double too :|
			const float factor = static_cast<float>((time - currentFrame.first) / (nextFrame.first - currentFrame.first));

			for (const auto& [name, rot] : currentFrame.second)
			{
				if (const auto& nextFramePose = nextFrame.second.find(name); nextFramePose != nextFrame.second.end())
					transforms[name] = glm::slerp(rot, nextFramePose->second, factor);
				else
					transforms[name] = rot;
			}

			return transforms;
		}
	}

	return transforms;
}

//-------------------------------------------------------------------------------------------------
std::unordered_map<std::string, glm::vec3> Animation::GetPositionPoseForFrame(const unsigned int frameIdx) const
{
	auto& itr = m_positionKeyFrames.begin();
	std::advance(itr, frameIdx);

	return GetPositionPoseForTime(itr->first);
}

//-------------------------------------------------------------------------------------------------
std::unordered_map<std::string, glm::quat> Animation::GetRotationPoseForFrame(const unsigned int frameIdx) const
{
	auto& itr = m_rotationKeyFrames.begin();
	std::advance(itr, frameIdx);

	return GetRotationPoseForTime(itr->first);
}

//-------------------------------------------------------------------------------------------------
uint32_t Animation::GetNumOfFrames() const
{
	return m_positionKeyFrames.size() > m_rotationKeyFrames.size() ? m_positionKeyFrames.size() : m_rotationKeyFrames.size();
}

double Animation::ConvertFrameToMS(const double frame) const
{
	if (m_isUsingFrames)
	{
		return frame / m_ticksPerSec;
	}
	else
	{
		if (frame > 1 && frame < m_positionKeyFrames.size())
		{
			auto itr = m_positionKeyFrames.begin();
			std::advance(itr, frame - 1);
			return itr->first;
		}
	}

	return 0.0;
}

//-------------------------------------------------------------------------------------------------
double Animation::GetFrameForTime(const double time) const
{
	const auto& posItr = m_positionKeyFrames.lower_bound(time);
	if (posItr != m_positionKeyFrames.end())
	{
		return std::distance(m_positionKeyFrames.begin(), posItr);
	}

	const auto& rotItr = m_rotationKeyFrames.lower_bound(time);
	if (rotItr != m_rotationKeyFrames.end())
	{
		return std::distance(m_rotationKeyFrames.begin(), rotItr);
	}

	return 0.0;
}