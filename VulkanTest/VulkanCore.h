#ifndef _VULKAN_CORE_H_
#define _VULKAN_CORE_H_

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vector>
#include <string>
#include <optional>

#include <glm/mat4x4.hpp>

#include "Shader.h"
#include "Timer.h"
#include "TextureManager.h"
#include "ModelManager.h"
#include "LightManager.h"
#include "InterpolatedCamera.h"
#include "DearImGui.h"

#ifdef NDEBUG
	const bool enableValidationLayers = false;
#else
	const bool enableValidationLayers = true;

	static const std::vector<const char*> validationLayers =
	{
		"VK_LAYER_LUNARG_standard_validation"
	};
#endif

static VKAPI_ATTR VkBool32 VKAPI_CALL debugMessageCallback
(
	VkDebugUtilsMessageSeverityFlagBitsEXT		severity,
	VkDebugUtilsMessageTypeFlagsEXT				type,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData
)
{
	printf("validation layer : %s  \n", pCallbackData->pMessage);

	return VK_FALSE;
}

struct QueueFamilyIndices
{
	std::optional<uint32_t> gfxFamily;
	std::optional<uint32_t> presentFamily;

	bool isComplete() { return gfxFamily.has_value() && presentFamily.has_value(); }
};

class SceneNode;

class VulkanCore
{
	//Ptoential hack here but exposing every single part of the core is tedious/tiresome. This actually feels more elegant
	//If more items need this info, it'd be better to expose with getters for encapsulation
	friend class DearImGui;

private:
	//----- GLFW INIT FUNCTIONS -----
	bool InitGLFW				();

	//----- VULKAN INIT FUNCTIONS -----
	bool InitInstance					();
	bool CreateSurface					();
	void EnumerateDevices				();
	bool CreateLogicalDevice			();
	bool CheckValidationLayerSupport	();
	bool CreateSwapchain				();
	bool CreateDepthResources			();
	bool CreateRenderPass				();
	bool CreateStaticPipeline			();
	bool CreateAnimatedPipeline			();
	bool CreateFramebuffers				();
	bool CreateCommandPools				();
	bool CreateCommandBuffers			();
	bool RecordCommandBuffer			(const uint32_t currentImage);
	bool CreateSemaphoresAndFences		();
	void RecreateSwapchain				();

	void DrawImGuiFrame					(SceneNode* closestPickedNode);
	void DrawFrame						();

	//----- UTILITY FUNCTIONS -----
	bool				FindMemoryType					(uint32_t typeBits, const VkFlags requirementMask, uint32_t* typeIndex);
	void				LoadRequiredExtensions			();
	void				SetupDebugMessenger				();
	void				PopulateDebugMessengerInfo		(VkDebugUtilsMessengerCreateInfoEXT& info);
	VkExtent2D			FindWindowExtents				(const VkSurfaceCapabilitiesKHR& capa);
	VkCommandBuffer		BeginSingleTimeCommands			() const;
	void				EndSingleTimeCommands			(VkCommandBuffer& commandBuffer, VkQueue& queue);

	void CleanUpSwapchain	();

	//----- CONSTANTS -----
	const uint32_t MAX_FRAMES_IN_FLIGHT = 2;

	//----- GLFW Member Variables -----
	GLFWwindow*	m_window;
	uint32_t	m_windowWidth	= 1920;
	uint32_t	m_windowHeight	= 1080;

	//----- VULKAN MEMBER VARIABLES -----
	VkInstance		m_instance;
	VkDevice		m_device;
	VkSurfaceKHR	m_surface;
	bool			m_cleanedUp = false;

	//----- Device(s) -----
	std::vector<VkPhysicalDevice>	 m_gpuList;
	VkPhysicalDeviceMemoryProperties m_deviceMemProps;

	uint32_t							 m_familyQueueIndex;
	std::vector<VkQueueFamilyProperties> m_queueProps;

	//----- Queue(s) -----
	VkQueue m_gfxQueue;
	VkQueue m_presentQueue;
	QueueFamilyIndices m_queueFamilyIndices;

	//----- Extensions -----
	std::vector<const char*> m_instanceExtensionNames;
	std::vector<const char*> m_deviceExtensionNames;

	//----- Debug/Validation Layers -----
	VkDebugUtilsMessengerEXT m_debugMessenger;

	//----- Swapchain -----
	VkSwapchainKHR				m_swapchain;
	uint32_t					m_swapchainCurrentBuffer;
	uint32_t					m_swapchainImageCount;
	VkFormat					m_swapchainFormat;
	VkExtent2D					m_swapchainExtents;
	std::vector<VkImage>		m_bufferImages;
	std::vector<VkImageView>	m_bufferImageViews;

	//----- Depth Buffer -----
	VkImage					m_depthBufferImage;
	VkImageView				m_depthBufferView;
	VkMemoryRequirements	m_depthMemoryReq;
	VkDeviceMemory			m_depthMemory;
	VkFormat				m_depthFormat;

	//----- Descriptor/Pipeline Layout -----
	VkPipelineLayout m_staticPipelineLayout;
	VkPipelineLayout m_animatedPipelineLayout;

	//----- Render Pass -----
	VkRenderPass			 m_renderPass;
	std::vector<VkSemaphore> m_imageAvailableSemaphores;
	std::vector<VkSemaphore> m_renderFinishedSemaphores;
	std::vector<VkFence>	 m_inflightFences;
	std::vector<VkFence>	 m_imagesInFlight;

	size_t m_currentFrame = 0;

	//----- Pipeline ------
	VkPipeline m_staticPipeline;
	VkPipeline m_animatedPipeline;

	//----- Framebuffer -----
	std::vector<VkFramebuffer>	m_swapchainFramebuffers;

	//----- Command Pools/Buffers -----
	VkCommandPool				 m_commandPool;
	std::vector<VkCommandBuffer> m_commandBuffers;

	//----- Shader -----
	Shader* m_staticShader;
	Shader* m_animatedShader;

	//----- Textures -----
	TextureManager m_textureManager;

	//----- Models -----
	ModelManager m_modelManager;

	//----- Lights -----
	LightManager m_lightManager;

	//----- Camera -----
	//Camera m_camera;
	InterpolatedCamera m_camera;

	//----- Timer -----
	Timer m_timer;

	//----- Scene Node -----
	SceneNode* m_rootNode = nullptr;

	//----- ImGui -----
	DearImGui m_imgui;

	//----- SINGLETON -----
	static VulkanCore* m_singleton;
	VulkanCore();

public:
	VulkanCore(const VulkanCore&)			 = delete;
	VulkanCore(VulkanCore&&)				 = delete;
	VulkanCore operator= (const VulkanCore&) = delete;
	VulkanCore operator= (VulkanCore&&)		 = delete;

	~VulkanCore();

	bool Initialise	();
	void Update		();
	void CleanUp	();

	static VulkanCore& Singleton()
	{
		if (m_singleton == nullptr)
		{
			m_singleton = new VulkanCore();
		}

		return *m_singleton;
	}

	static void DeleteSingleton()
	{
		if (m_singleton)
		{
			delete m_singleton;
			m_singleton = nullptr;
		}
	}

	//Singleton Interface
	//Utils Functions
	static bool	CreateBuffer			(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags props, VkBuffer& buffer, VkDeviceMemory& memory);
	static void	CopyBuffer				(VkBuffer src, VkBuffer dst, VkDeviceSize size);
	static void	CopyBufferToImage		(VkBuffer& imageBuffer, VkImage& image, uint32_t w, uint32_t h, const std::optional<VkImageSubresourceLayers> subresource = std::nullopt);
	static bool	CreateImage				(uint32_t width, uint32_t height, VkFormat format, VkImageTiling tiling, VkImageUsageFlags usageFlags, VkMemoryPropertyFlags properties, VkImage& image, VkDeviceMemory& imageMemory, const uint32_t mipLevels = 1, const uint32_t arrayLayers = 1);
	static bool	CreateImageView			(VkImageView& imageView, VkImage& image, VkFormat format, VkImageAspectFlags aspectFlags);
	static void	TransitionImageLayout	(VkImage& image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout);

	//Getters
	VkDevice&	Device					() { return m_singleton->m_device; }
	int32_t		SwampchainImageCount	() { return m_singleton->m_swapchainImageCount; }
	glm::vec2	WindowExtents			() { return { m_singleton->m_windowWidth, m_singleton->m_windowHeight }; }
	const TextureManager& TextureManager() const { return m_singleton->m_textureManager; }
};

#endif
