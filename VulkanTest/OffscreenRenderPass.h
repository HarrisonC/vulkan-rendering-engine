#ifndef _FRAME_BUFFER_H_
#define _FRAME_BUFFER_H_

#include <vulkan/vulkan.h>
#include <vector>

struct Texture;

class OffscreenRenderPass
{
public:
	struct FrameBufferImageInfo
	{
		const VkFormat format;
		const VkImageTiling tiling;
		const VkImageUsageFlagBits usageFlags;
		const VkMemoryPropertyFlagBits memFlags;
		const VkImageAspectFlagBits aspectFlags;
	};

	struct FrameBufferImageViewInfo
	{
		const VkImage image;
		const VkDeviceMemory imageMemory;
		const VkExtent2D extents;
		const VkFormat format;
		const VkImageAspectFlagBits aspectFlags;
	};

protected:
	struct FrameBufferImage
	{
		VkImage			m_image			= nullptr;
		VkDeviceMemory	m_imageMemory	= nullptr;
		VkImageView		m_imageView		= nullptr;
	};

	bool CreateImagesFromInfo(const std::vector<OffscreenRenderPass::FrameBufferImageInfo>& infos);
	bool CreateImageSampler();
	bool CreateRenderPass();
	bool CreateFrameBuffer();

	VkExtent2D m_extents{ 0, 0 };

	std::vector<FrameBufferImage> m_images;

	VkFramebuffer			m_frameBuffer	 = nullptr;
	VkRenderPass			m_renderPass	 = nullptr;
	VkSampler				m_sampler		 = nullptr;
	VkDescriptorImageInfo	m_descriptorInfo = {};
	Texture*				m_texture;

	VkFormat m_colourFormat = VkFormat::VK_FORMAT_UNDEFINED;
	VkFormat m_depthFormat = VkFormat::VK_FORMAT_UNDEFINED;

	bool m_cleanedUp = false;

public:

	 OffscreenRenderPass() = default;
	 OffscreenRenderPass(const VkExtent2D& extents, const std::vector<OffscreenRenderPass::FrameBufferImageInfo>& imageInfos);
	
	 OffscreenRenderPass			(OffscreenRenderPass&& rhs);
	 OffscreenRenderPass& operator= (OffscreenRenderPass&& rhs);

	 OffscreenRenderPass			(const OffscreenRenderPass&) = delete;
	 OffscreenRenderPass operator=	(const OffscreenRenderPass&) = delete;

	~OffscreenRenderPass();
	void CleanUp();

	//----- Getters -----
	VkRenderPassBeginInfo	GetRenderPassInfo	() const;
	const VkFramebuffer&	GetFramebuffer		() const	{ return m_frameBuffer; }
	const VkImage&			GetColourImage		()const		{ return m_images[0].m_image; }
	const VkImage&			GetDepthImage		()const		{ return m_images[1].m_image; }

	const Texture&			GetTexture			() const	{ return *m_texture; }

};

#endif
