#ifndef _MODEL_MANAGER_H_
#define _MODEL_MANAGER_H_ 

#include <unordered_map>
#include <memory>

class Model;

class ModelManager
{
private:
	std::unordered_map<std::string, std::unique_ptr<Model>> m_models;

public:
	 ModelManager();
	~ModelManager();

	void CleanUp();
	void LoadModels();

	const Model& GetModel(const std::string& modelPath) const;
};

#endif