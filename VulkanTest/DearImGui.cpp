#include "DearImgui.h"

#include "VulkanCore.h"
#include "ImGui/imgui_impl_glfw.h"
#include "ImGui/imgui_impl_vulkan.h"

DearImGui::DearImGui() :
	m_core(nullptr),
	m_shader(nullptr),
	m_componentsToDraw(),
	m_commandPool(VK_NULL_HANDLE),
	m_commandBuffers(),
	m_frameBuffers(),
	m_renderPass(VK_NULL_HANDLE)
{
}

//-------------------------------------------------------------------------------------------------
bool DearImGui::Initialise()
{
	m_core = &VulkanCore::Singleton();
	m_shader = new ImGuiShader{ "ImGui/imgui_shaders/glsl_shader.vert", "ImGui/imgui_shaders/glsl_shader.frag" };
	m_shader->Initialise();

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGui::StyleColorsDark();
	ImGuiIO& io = ImGui::GetIO();

	if (CreateCommandPools()	== false)	return false;
	if (CreateCommandBuffers()	== false)	return false;
	if (CreateRenderPass()		== false)	return false;
	if (CreateFrameBuffers()	== false)	return false;

	ImGui_ImplGlfw_InitForVulkan(m_core->m_window, true);
	ImGui_ImplVulkan_InitInfo initInfo = {};
	initInfo.Instance = m_core->m_instance;
	initInfo.PhysicalDevice = m_core->m_gpuList.front();
	initInfo.Device = m_core->m_device;
	initInfo.ImageCount = m_core->m_swapchainImageCount;
	initInfo.MinImageCount = m_core->m_swapchainImageCount;
	initInfo.Queue = m_core->m_gfxQueue;
	initInfo.QueueFamily = m_core->m_queueFamilyIndices.gfxFamily.value();
	initInfo.DescriptorPool = m_shader->GetDescriptorPool();
	initInfo.PipelineCache = VK_NULL_HANDLE;
	initInfo.CheckVkResultFn = [](const auto p) { if (p != VK_SUCCESS) printf("Well shit!\n"); };;
	ImGui_ImplVulkan_Init(&initInfo, m_renderPass);

	LoadFonts();

	return true;
}

//-------------------------------------------------------------------------------------------------
void DearImGui::CleanUp()
{
	CleanUpSwapchain();

	ImGui_ImplVulkan_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

//-------------------------------------------------------------------------------------------------
void DearImGui::CleanUpSwapchain()
{
	for (auto& framebuffer : m_frameBuffers)
	{
		vkDestroyFramebuffer(m_core->m_device, framebuffer, nullptr);
	}

	vkDestroyRenderPass(m_core->m_device, m_renderPass, nullptr);
	vkFreeCommandBuffers(m_core->m_device, m_commandPool, (uint32_t)m_commandBuffers.size(), m_commandBuffers.data());
	vkDestroyCommandPool(m_core->m_device, m_commandPool, nullptr);

	delete m_shader; m_shader = nullptr;
}

//-------------------------------------------------------------------------------------------------
void DearImGui::Draw(const uint32_t currentImage)
{
	ImGui::Render();

	if (vkResetCommandPool(m_core->m_device, m_commandPool, VK_NULL_HANDLE) != VK_SUCCESS)
	{
		printf("Failed to reset command pool for ImGui render \n");
		return;
	}

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	VkCommandBuffer& currentBuffer = m_commandBuffers[currentImage];
	if (vkBeginCommandBuffer(currentBuffer, &beginInfo) != VK_SUCCESS)
	{
		printf("Failed to begin command buffer for ImGui \n");
		return;
	}

	VkClearValue clearColour = { 0.1f, 0.1f, 0.3f, 1.0f };

	VkRenderPassBeginInfo renderBeginInfo = {};
	renderBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderBeginInfo.renderPass = m_renderPass;
	renderBeginInfo.renderArea.extent = m_core->m_swapchainExtents;
	renderBeginInfo.clearValueCount = 1;
	renderBeginInfo.pClearValues = &clearColour;
	renderBeginInfo.framebuffer = m_frameBuffers[currentImage];

	vkCmdBeginRenderPass(currentBuffer, &renderBeginInfo, VK_SUBPASS_CONTENTS_INLINE);
	ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), currentBuffer);
	vkCmdEndRenderPass(currentBuffer);

	if (vkEndCommandBuffer(currentBuffer) != VK_SUCCESS)
	{
		printf("Failed to submit command buffer for ImGui! \n");
		return;
	}
}

//-------------------------------------------------------------------------------------------------
void DearImGui::StartNewImGuiFrame()
{
	ImGui_ImplVulkan_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
}

//-------------------------------------------------------------------------------------------------
bool DearImGui::CreateCommandPools()
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = m_core->m_queueFamilyIndices.gfxFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

	if (vkCreateCommandPool(m_core->m_device, &poolInfo, nullptr, &m_commandPool) != VK_SUCCESS)
	{
		printf("Failed to creat ImGui Command Pool! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool DearImGui::CreateCommandBuffers()
{
	const auto numOfBuffers = m_core->m_swapchainFramebuffers.size();
	m_commandBuffers.resize(numOfBuffers);

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = m_commandPool;
	allocInfo.commandBufferCount = static_cast<uint32_t>(numOfBuffers);
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;

	if (vkAllocateCommandBuffers(m_core->m_device, &allocInfo, m_commandBuffers.data()) != VK_SUCCESS)
	{
		printf("Failed to create ImGui Command Buffers! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool DearImGui::CreateFrameBuffers()
{
	const auto numOfBuffers = m_core->m_bufferImageViews.size();
	m_frameBuffers.resize(numOfBuffers);

	VkImageView attachment[1];

	VkFramebufferCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	createInfo.width = m_core->m_swapchainExtents.width;
	createInfo.height = m_core->m_swapchainExtents.height;
	createInfo.layers = 1;
	createInfo.pAttachments = attachment;
	createInfo.attachmentCount = 1;
	createInfo.renderPass = m_renderPass;

	for (uint32_t i = 0; i < numOfBuffers; ++i)
	{
		attachment[0] = m_core->m_bufferImageViews[i];
		if (vkCreateFramebuffer(m_core->m_device, &createInfo, nullptr, &m_frameBuffers[i]) != VK_SUCCESS)
		{
			printf("Failed to create frame buffer for ImGui, nothing will be drawn! \n");
			return false;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool DearImGui::CreateRenderPass()
{
	VkAttachmentDescription attachmentDesc = {};
	attachmentDesc.format = m_core->m_swapchainFormat;
	attachmentDesc.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;	//Don't overwrite framebuffer, load and render above instead
	attachmentDesc.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachmentDesc.initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
	attachmentDesc.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
	//The dont cares, we don't want multisampling nor do we want a stencil buffer
	attachmentDesc.samples = VK_SAMPLE_COUNT_1_BIT;
	attachmentDesc.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachmentDesc.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;

	VkAttachmentReference attachmentRef = {};
	attachmentRef.attachment = 0;
	attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpassDesc = {};
	subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDesc.colorAttachmentCount = 1;
	subpassDesc.pColorAttachments = &attachmentRef;

	VkSubpassDependency subpassDep = {};
	subpassDep.srcSubpass = VK_SUBPASS_EXTERNAL;	//Dependency on subpass outside of this renderpass
	subpassDep.dstSubpass = 0;						//dstSubpass is this one, our one and only one
	subpassDep.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;	//Wait for the previous geometry to be rendered
	subpassDep.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;	//We're drawing to the color attachment too
	subpassDep.srcAccessMask = 0;
	subpassDep.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpassDesc;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &subpassDep;
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &attachmentDesc;
	
	if (vkCreateRenderPass(m_core->m_device, &renderPassInfo, nullptr, &m_renderPass) != VK_SUCCESS)
	{
		printf("Failed to initialise ImGui render pass, it ain't going to appear! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
void DearImGui::LoadFonts()
{
	VkCommandBuffer commandBuffer = m_core->BeginSingleTimeCommands();
	ImGui_ImplVulkan_CreateFontsTexture(commandBuffer);
	m_core->EndSingleTimeCommands(commandBuffer, m_core->m_gfxQueue);

	ImGui_ImplVulkan_DestroyFontUploadObjects();
}