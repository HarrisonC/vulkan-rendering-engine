#ifndef	_VERTEX_H_
#define _VERTEX_H_

#include <glm/glm.hpp>
#include <array>
#include <vulkan/vulkan.h>

struct Vertex
{
	glm::vec3 m_pos;
	glm::vec3 m_normal;
	glm::vec3 m_col;
	glm::vec2 m_texCoords;

	static VkVertexInputBindingDescription GetBindingDesc()
	{
		VkVertexInputBindingDescription bindingDesc = {};
		bindingDesc.binding = 0;
		bindingDesc.stride = sizeof(Vertex);
		bindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDesc;
	}

	static std::array<VkVertexInputAttributeDescription, 4> GetAttributeDescs()
	{
		std::array<VkVertexInputAttributeDescription, 4> attribDescs = {};

		attribDescs[0].binding = 0;
		attribDescs[0].location = 0;
		attribDescs[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribDescs[0].offset = offsetof(Vertex, m_pos);

		attribDescs[1].binding = 0;
		attribDescs[1].location = 1;
		attribDescs[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribDescs[1].offset = offsetof(Vertex, m_normal);

		attribDescs[2].binding = 0;
		attribDescs[2].location = 2;
		attribDescs[2].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribDescs[2].offset = offsetof(Vertex, m_col);
		
		attribDescs[3].binding = 0;
		attribDescs[3].location = 3;
		attribDescs[3].format = VK_FORMAT_R32G32_SFLOAT;
		attribDescs[3].offset = offsetof(Vertex, m_texCoords);

		return attribDescs;
	}

};

struct AnimVertex
{
	glm::vec3  m_pos;
	glm::vec3  m_normal;
	glm::vec3  m_col;
	glm::vec2  m_texCoords;
	glm::ivec4 m_boneIDs;
	glm::vec4  m_weights;

	static VkVertexInputBindingDescription GetBindingDesc()
	{
		VkVertexInputBindingDescription bindingDesc = {};
		bindingDesc.binding = 0;
		bindingDesc.stride = sizeof(AnimVertex);
		bindingDesc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDesc;
	}

	static std::array<VkVertexInputAttributeDescription, 6> GetAttributeDescs()
	{
		std::array<VkVertexInputAttributeDescription, 6> attribDescs = {};

		attribDescs[0].binding = 0;
		attribDescs[0].location = 0;
		attribDescs[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribDescs[0].offset = offsetof(AnimVertex, m_pos);

		attribDescs[1].binding = 0;
		attribDescs[1].location = 1;
		attribDescs[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribDescs[1].offset = offsetof(AnimVertex, m_normal);

		attribDescs[2].binding = 0;
		attribDescs[2].location = 2;
		attribDescs[2].format = VK_FORMAT_R32G32B32_SFLOAT;
		attribDescs[2].offset = offsetof(AnimVertex, m_col);

		attribDescs[3].binding = 0;
		attribDescs[3].location = 3;
		attribDescs[3].format = VK_FORMAT_R32G32_SFLOAT;
		attribDescs[3].offset = offsetof(AnimVertex, m_texCoords);

		attribDescs[4].binding = 0;
		attribDescs[4].location = 4;
		attribDescs[4].format = VK_FORMAT_R32G32B32A32_SINT;
		attribDescs[4].offset = offsetof(AnimVertex, m_boneIDs);

		attribDescs[5].binding = 0;
		attribDescs[5].location = 5;
		attribDescs[5].format = VK_FORMAT_R32G32B32A32_SFLOAT;
		attribDescs[5].offset = offsetof(AnimVertex, m_weights);

		return attribDescs;
	}
};

#endif
