#ifndef _CAMERA_H_ 
#define _CAMERA_H_ 

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Camera
{
protected:
	void UpdateFront();

	glm::vec3	m_position;
	glm::vec3	m_up;
	glm::vec3	m_originalUp;
	glm::vec3	m_front;

	glm::mat4	m_projection;

	glm::vec3	m_rotation;
	float		m_pitchBoundary;

	double		m_speed;
	double		m_FOV;

	glm::vec3	m_focusPoint;
	bool		m_hasFocusPoint;

public:
	 Camera();
	~Camera();

	//----- Utility Functions -----
	[[maybe_unused]] bool LookAtPoint	(const glm::vec3& point);
	virtual			 void AddToImGui	();

	//----- Setters -----
	virtual void AddYaw		(const float delta);
	virtual void AddPitch	(const float delta);
	virtual void AddRoll	(const float delta);

	virtual void SetYaw		(const float newYaw);
	virtual void SetPitch	(const float newPitch);
	virtual void SetRoll	(const float newRoll);

	virtual void ChangePosition	(const glm::vec3& delta);
	virtual void SetPosition	(const glm::vec3& new_pos);

	void SetFocus		(const glm::vec3& newFocus);
	void RemoveFocus	();

	//----- Getters -----
	glm::mat4			GetViewMatrix		() const;
	const glm::vec3&	GetPosition			() const;
	const glm::vec3&	GetUp				() const;
	const glm::vec3&	GetFront			() const;
	const glm::vec3&	GetFocusPoint		() const;
	const glm::mat4&	GetProjectionMatrix	() const;
	double				GetSpeed			() const;
	bool				HasFocusPoint		() const;
};

#endif
