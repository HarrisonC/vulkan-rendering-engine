#include "CubeMap.h"
#include "Texture.h"
#include "Shader.h"
#include "VulkanCore.h"

#include <array>

CubeMap::CubeMap(const Model& model, const CubeTexture& texture, Shader& shader) :
	RenderObject(model, texture, shader)
{
	m_material = Material{ glm::vec4{ 1.0f }, glm::vec4{ 0.0f, 0.8f, 0.8f, 1.0f }, glm::vec4{ 1.0f }, 0.1f };

	CreateUniformBuffers();
	CreateDescriptorSets(static_cast<const Texture&>(texture));
}

CubeMap::~CubeMap()
{
}

//-------------------------------------------------------------------------------------------------
bool CubeMap::CreateDescriptorSets(const Texture& texture)
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();

	std::vector<VkDescriptorSetLayout> layouts(swapchainImageCount, m_shader.m_descriptorLayout);
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.pSetLayouts = layouts.data();
	allocInfo.descriptorPool = m_shader.m_descriptorPool;
	allocInfo.descriptorSetCount = swapchainImageCount;

	m_descriptorSets.resize(swapchainImageCount);
	if (vkAllocateDescriptorSets(VulkanCore::Singleton().Device(), &allocInfo, m_descriptorSets.data()) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to allocate descriptor sets! \n");
		return false;
	}

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VPShader& shader = static_cast<VPShader&>(m_shader);

		VkMemoryRequirements memReqs = {};
		vkGetBufferMemoryRequirements(VulkanCore::Singleton().Device(), shader.m_vpUniformBuffers[i], &memReqs);

		VkDescriptorBufferInfo vpBufferInfo = {};
		vpBufferInfo.buffer = shader.m_vpUniformBuffers[i];
		vpBufferInfo.offset = 0;
		vpBufferInfo.range = sizeof(VPShader::VP);

		VkDescriptorBufferInfo materialBufferInfo = {};
		materialBufferInfo.buffer = m_materialUniformBuffers[i];
		materialBufferInfo.offset = 0;
		materialBufferInfo.range = sizeof(Material);

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = texture.m_textureImageView;
		imageInfo.sampler = texture.m_textureSampler;

		std::array<VkWriteDescriptorSet, 3> sets = {};
		sets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[0].dstSet = m_descriptorSets[i];
		sets[0].dstBinding = 0;
		sets[0].dstArrayElement = 0;
		sets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[0].descriptorCount = 1;
		sets[0].pBufferInfo = &vpBufferInfo;
		sets[0].pImageInfo = nullptr;
		sets[0].pTexelBufferView = nullptr;

		sets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[1].dstSet = m_descriptorSets[i];
		sets[1].dstBinding = 1;
		sets[1].dstArrayElement = 0;
		sets[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[1].descriptorCount = 1;
		sets[1].pBufferInfo = &materialBufferInfo;
		sets[1].pImageInfo = nullptr;
		sets[1].pTexelBufferView = nullptr;

		sets[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[2].dstSet = m_descriptorSets[i];
		sets[2].dstBinding = 2;
		sets[2].dstArrayElement = 0;
		sets[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		sets[2].descriptorCount = 1;
		sets[2].pBufferInfo = nullptr;
		sets[2].pImageInfo = &imageInfo;
		sets[2].pTexelBufferView = nullptr;

		vkUpdateDescriptorSets(VulkanCore::Singleton().Device(), (uint32_t)sets.size(), sets.data(), 0, nullptr);
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
void CubeMap::CleanUp()
{
}