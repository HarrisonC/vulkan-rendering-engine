#ifndef	_TEXTURE_MANAGER_H_
#define	_TEXTURE_MANAGER_H_

#include <unordered_map>
#include <string>
#include <memory>

struct Texture;

class TextureManager
{
private:
	std::unordered_map<std::string, std::unique_ptr<Texture>> m_textures;

public:
	 TextureManager	();
	~TextureManager	();
	void CleanUp	();

	void				LoadTextures();
	const Texture&		GetTexture(const std::string& texturePath) const;

	const Texture* 		AddToImGui() const;
};

#endif
