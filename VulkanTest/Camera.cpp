#include "Camera.h"
#include <glm/trigonometric.hpp>
#include <glm/gtx/transform.hpp>

#include "ImGui/imgui_helper_functions.h"
#include "ImGui/imgui.h"

namespace
{
	float WrapNumberToCircle(const float n)
	{
		//]TODO: Very naive solution, think of something more robust
		//i.e. something that can deal with 1000+
		if (n > 360.0f)		{ return n - 360.0f; }
		else if (n < 0.0f)  { return n + 360.0f; }

		return n;
	}
}

Camera::Camera() :
	m_position({ 0.0f }),
	m_up({ 0.0f, -1.0f, 0.0f }),
	m_originalUp({ 0.0f, -1.0f, 0.0f }),
	m_front({ 0.0f }),
	m_rotation(0.0),
	m_pitchBoundary(60.0f),
	m_speed(30.0),
	m_FOV(45.0),
	m_hasFocusPoint(false),
	m_focusPoint(glm::vec3(-1.0f))
{
	//m_projection = glm::ortho(0.0f, extents.x, extents.y, 0.0f, -1.0f, 1.0f);
	m_projection = glm::perspective(glm::radians(45.f), 800.0f / 600.0f, 0.1f, 10000.0f);	//#TODO: FIX_THIS
	UpdateFront();
}

Camera::~Camera()
{
}

//-------------------------------------------------------------------------------------------------
void Camera::AddToImGui()
{
	auto CreateEntry = [](const char* txt, glm::vec3& data) -> bool
	{
		ImGui::Text(txt);
		ImGui::NextColumn();
		const bool hasChanged = ImGuiHelper::CreateSameLineBoxForVector(txt, data);
		ImGui::NextColumn();
		ImGui::Separator();

		return hasChanged;
	};

	ImGui::Columns(2);
	ImGui::SetColumnWidth(0, 150);
	ImGui::SetColumnWidth(1, 384);
	ImGui::Separator();

	CreateEntry("Position",		m_position);
	CreateEntry("Current Up",	m_up);
	if (CreateEntry("Rotation", m_rotation))
	{
		UpdateFront();
	}

	ImGui::Text("Enable Focus Point");
	ImGui::NextColumn();
	ImGui::Checkbox("##enablefp", &m_hasFocusPoint);
	ImGui::NextColumn();
	ImGui::Separator();

	if (CreateEntry("Focus Point", m_focusPoint))
	{
		LookAtPoint(m_focusPoint);
	}

	ImGui::Text("Speed");
	ImGui::NextColumn();
	ImGuiHelper::CreateDoubleInputBoxWithArrows(m_speed, "", 5.0);
	ImGui::NextColumn();
	ImGui::Separator();

	ImGui::Columns(1);
}

//-------------------------------------------------------------------------------------------------
void Camera::UpdateFront()
{
	m_front.x = static_cast<float>(cos(glm::radians(m_rotation.y)) * cos(glm::radians(m_rotation.x)));
	m_front.y = static_cast<float>(sin(glm::radians(m_rotation.y)));
	m_front.z = static_cast<float>(cos(glm::radians(m_rotation.y)) * sin(glm::radians(m_rotation.x)));
	m_front	= glm::normalize(m_front);
}

//-------------------------------------------------------------------------------------------------
bool Camera::LookAtPoint(const glm::vec3& point)
{
	const glm::vec3 camToP = glm::normalize(point - m_position);

	SetYaw(glm::degrees(atan2(camToP.z, camToP.x)));
	SetPitch(glm::degrees(asin(camToP.y)));

	//We already have the front so no point going through the trauma of recalculating it
	m_front = camToP;

	return true;	//#TODO: Make it so it returns false if looking at point breaks restrictions
}

//-------------------------------------------------------------------------------------------------
const glm::vec3& Camera::GetPosition() const
{
	return m_position;
}

//-------------------------------------------------------------------------------------------------
const glm::vec3& Camera::GetFront() const
{
	return m_front;
}

//-------------------------------------------------------------------------------------------------
const glm::vec3& Camera::GetUp() const
{
	return m_up;
}

//-------------------------------------------------------------------------------------------------
glm::mat4 Camera::GetViewMatrix() const
{
	return glm::lookAt(m_position, m_position + m_front, m_up);
}

//-------------------------------------------------------------------------------------------------
const glm::mat4& Camera::GetProjectionMatrix() const
{
	return m_projection;
}

//-------------------------------------------------------------------------------------------------
void Camera::ChangePosition(const glm::vec3& delta)
{
	m_position += delta;

	if (m_hasFocusPoint)
	{
		LookAtPoint(m_focusPoint);
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::SetPosition(const glm::vec3& new_pos)
{
	m_position = new_pos;

	if (m_hasFocusPoint)
	{
		LookAtPoint(m_focusPoint);
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::AddYaw(const float delta)
{
	if (m_hasFocusPoint == false)
	{
		m_rotation.x = WrapNumberToCircle(m_rotation.x + delta);
		UpdateFront();
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::AddPitch(const float delta)
{
	if (m_hasFocusPoint == false)
	{
		m_rotation.y += delta;

		if (m_rotation.y > m_pitchBoundary)
		{
			m_rotation.y = m_pitchBoundary;
		}
		else if (m_rotation.y < -m_pitchBoundary)
		{
			m_rotation.y = -m_pitchBoundary;
		}

		UpdateFront();
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::AddRoll(const float delta)
{
	if (m_hasFocusPoint == false)
	{
		m_rotation.z = WrapNumberToCircle(m_rotation.z + delta);
		UpdateFront();
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::SetYaw(const float newYaw)
{
	if (m_hasFocusPoint)
	{
		m_rotation.x = WrapNumberToCircle(newYaw);
		UpdateFront();
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::SetPitch(const float newPitch)
{
	if (m_hasFocusPoint)
	{
		m_rotation.y = WrapNumberToCircle(newPitch);
		UpdateFront();
	}
}

//-------------------------------------------------------------------------------------------------
void Camera::SetRoll(const float newRoll)
{
	if (m_hasFocusPoint)
	{
		m_rotation.z = WrapNumberToCircle(newRoll);
		UpdateFront();
	}
}

//-------------------------------------------------------------------------------------------------
double Camera::GetSpeed() const 
{
	return m_speed;
}

//-------------------------------------------------------------------------------------------------
void Camera::SetFocus(const glm::vec3& newFocus)
{
	m_hasFocusPoint = true;
	m_focusPoint = newFocus;

	LookAtPoint(m_focusPoint);
}

//-------------------------------------------------------------------------------------------------
void Camera::RemoveFocus()
{
	m_hasFocusPoint = false;
	m_focusPoint = glm::vec3(-1.0f);
}

//-------------------------------------------------------------------------------------------------
const glm::vec3& Camera::GetFocusPoint() const
{
	return m_focusPoint;
}

//-------------------------------------------------------------------------------------------------
bool Camera::HasFocusPoint() const
{
	return m_hasFocusPoint;
}