#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include <memory>

class  Model;

class Terrain
{
private:
	unsigned int m_width = 256u;
	unsigned int m_height = 256u;

	std::unique_ptr<Model>			m_model;

public:
	 Terrain(const unsigned int width, const unsigned int height);
	~Terrain();

	void Initialise();

	const Model& GetModel() const { return *m_model; }
};

#endif