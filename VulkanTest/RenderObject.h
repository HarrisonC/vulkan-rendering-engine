#pragma once

#include <vulkan/vulkan.h>
#include <vector>
#include <list>
#include <string>
#include <assimp/scene.h>
#include <optional>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include "VulkanUtils.h"

class  Model;
class  Shader;
struct Texture;
struct CubeTexture;
class TextureManager;

struct ObjPushConstant
{
	glm::mat4 modelMatrix;
	glm::vec3 colour;
	double dt;

	ObjPushConstant(const glm::mat4& mat, const glm::vec3& col, const double deltatime) :
		modelMatrix(mat), colour(col), dt(deltatime)
	{
	}
};

struct Material
{
	//These are vec4's instead of 3's for padding!
	glm::vec4	ambient;
	glm::vec4	diffuse;
	glm::vec4	specular;
	float		shininess;
};

class RenderObject
{
protected:
	virtual void Initialise				(const Texture& texture);
	virtual bool CreateDescriptorSets	(const Texture& texture);
	virtual bool CreateDescriptorSets	(std::vector<const Texture*> textures);
	virtual void CreateUniformBuffers	();
	virtual void UpdateUniformBuffers	(const uint32_t currentImage);
	virtual void CleanUp				();

	const Model&					m_model;

	Shader&							m_shader;
	std::vector<VkDescriptorSet>	m_descriptorSets;

//private:

	std::optional<Material>			m_material;

	std::vector<VkBuffer>			m_materialUniformBuffers;
	std::vector<VkDeviceMemory>		m_materialUniformMemories;

public:
	 RenderObject(const Model& model, const Texture& texture, Shader& shader, const bool skipInit = false);
	 RenderObject(const Model& model, std::vector<const Texture*> textures, Shader& shader);
	 RenderObject(const Model& model, const CubeTexture& texture, Shader& shader);
	virtual ~RenderObject();

	//----- Ctors and Assignments -----
	RenderObject(const RenderObject&) = delete;
	RenderObject(RenderObject&&) = delete;
	RenderObject operator= (const RenderObject&) = delete;
	RenderObject operator= (RenderObject&&) = delete;

	//----- Essential Functions -----
	virtual void Update(const double dt, const uint32_t currentImage) {}
	virtual void UpdateDescriptorSets(const Texture& texture);

	void Draw(const uint32_t currentImage, VkCommandBuffer& buffer, const VkPipelineLayout& pipelineLayout) const;
	void ChangeTexture(const Texture& texture);

	//----- Getters -----
	const Model&	GetModel	() const { return m_model; }
	const Shader&	GetShader	() const { return m_shader; }

	//----- Debug -----
	virtual void AddToImGui();
};

class AnimRenderObject : public RenderObject
{
protected:
	std::vector<VkBuffer>			m_boneTransformsUniformBuffers;
	std::vector<VkDeviceMemory>		m_boneTransformsUniformMemories;

	bool	m_isAnimPaused = false;
	double	m_animTime = 0.0;

public:
	AnimRenderObject(const Model& model, const Texture& texture, Shader& shader);

	virtual void Initialise				(const Texture& texture) override;
	virtual void CreateUniformBuffers	() override;
	virtual void UpdateUniformBuffers	(const uint32_t currentImage) override;
	virtual void UpdateDescriptorSets	(const Texture& texture) override;

	virtual void Update(const double dt, const uint32_t currentImage) override;

	virtual void AddToImGui() override;

};