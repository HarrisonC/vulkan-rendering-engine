#pragma once

#include "RenderObject.h"

class CubeMap : public RenderObject
{
protected:
	bool CreateDescriptorSets	(const Texture& texture)		override;
	void CleanUp				()								override;

public:
	 CubeMap(const Model& model, const CubeTexture& texture, Shader& shader);
	~CubeMap();
};