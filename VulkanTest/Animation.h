#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include <map>
#include <unordered_map>
#include <string>
#include <glm/gtc/quaternion.hpp>

struct aiScene;
struct aiNodeAnim;

class Animation
{
public:
	//All these maps make this very unfriendly for the cache
	//We should fix this later but for now, let's get things working (even though this hurts!)
	using PositionKeyFramePose = std::map<std::string, glm::vec3>;
	using RotationKeyFramePose = std::map<std::string, glm::quat>;

private:
	void LoadPositionKeyFrames(const aiNodeAnim& animNode);
	void LoadRotationKeyFrames(const aiNodeAnim& animNode);

	double									m_duration;
	double									m_ticksPerSec;
	bool									m_isUsingFrames = false;
	bool									m_isLooping = true;
	std::map<double, PositionKeyFramePose>	m_positionKeyFrames;
	std::map<double, RotationKeyFramePose>	m_rotationKeyFrames;

public:
	void	 LoadAnimation	(const aiScene& scene);
	double	 GetDuration	() const { return m_duration; }
	uint32_t GetNumOfFrames	() const;

	std::unordered_map<std::string, glm::vec3> GetPositionPoseForTime(const double time) const;
	std::unordered_map<std::string, glm::quat> GetRotationPoseForTime(const double time) const;

	std::unordered_map<std::string, glm::vec3> GetPositionPoseForFrame(const unsigned int frameIdx) const;
	std::unordered_map<std::string, glm::quat> GetRotationPoseForFrame(const unsigned int frameIdx) const;

	double ConvertFrameToMS	(const double frame) const;
	double GetFrameForTime	(const double time) const;
};

#endif
