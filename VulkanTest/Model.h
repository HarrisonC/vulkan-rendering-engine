#ifndef _MODEL_H_
#define _MODEL_H_

#include <vector>
#include <glm/vec3.hpp>
#include <string>
#include <memory>

#include "VulkanUtils.h"
#include "Vertex.h"

struct aiMesh;
struct aiNode;
struct aiScene;

class ShapeGenerator
{
public:
	virtual void GenerateShape(std::vector<Vertex>& verts, std::vector<uint32_t>& indices) const = 0;
};

class PlaneGenerator : public ShapeGenerator
{
private:
	uint32_t m_width;
	uint32_t m_height;
	uint32_t m_numOfTris;

public:
	PlaneGenerator(const uint32_t width, const uint32_t height, const uint32_t numOfTris = 2);

	void GenerateShape(std::vector<Vertex>& verts, std::vector<uint32_t>& indices) const override;
};


class Model
{
protected:
	std::vector<uint32_t>	m_indices;

	VulkanUtils::Buffer		m_vertexBuffer;
	VulkanUtils::Buffer		m_indicesBuffer;

	VkDevice&				m_device;
	glm::vec3				m_dimensions;

	std::string				m_filepath;

	virtual bool LoadModel			(const std::string& path);
	virtual void ProcessNode		(const aiNode& node, const aiScene& scene);
	virtual void ProcessMesh		(const aiMesh& mesh, const aiScene& scene) = 0;

	virtual void CreateVertexBuffer	() = 0;
	virtual void CreateIndexBuffer	();

public:
	Model(const std::string& filepath);
	Model(std::vector<uint32_t>&& indicies);
	 //Model(const ShapeGenerator& generator);
	virtual ~Model();

	//----- Ctors & Assignments -----
	Model(Model&& rhs);
	Model(const Model&) = delete;
	Model operator= (const Model&) = delete;
	Model operator=(Model&&) = delete;

	virtual void CleanUp();

	void Draw(const VkDescriptorSet& descriptorSet, VkCommandBuffer& buffer, const VkPipelineLayout& pipelineLayout) const;

	const glm::vec3& Dimensions() const { return m_dimensions; }
};

class StaticModel : public Model
{
protected:
	std::vector<Vertex>		m_vertices;

	virtual void ProcessMesh		(const aiMesh& mesh, const aiScene& scene) override;
	virtual void CreateVertexBuffer	() override;

public:
	StaticModel(const std::string& filepath);
	StaticModel(std::vector<Vertex>&& verts, std::vector<uint32_t>&& indicies);
	~StaticModel() {}
	//StaticModel(const ShapeGenerator& generator);

	//----- Ctors & Assignments -----
	StaticModel(const StaticModel&)				= delete;
	StaticModel operator= (const StaticModel&)	= delete;
	StaticModel operator=(StaticModel&&)		= delete;
};

#endif