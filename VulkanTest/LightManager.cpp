#include "LightManager.h"

#include <algorithm>

namespace
{
	//I don't really think this function is best suited as a template just because arrays
	//can cause serious bloat with templates. It works for now though and keeps code tidy (before compilation anyway!)
	template <typename Container, typename T = typename Container::value_type>
	const T* FindLightInArray(const Container& c, const unsigned int idx)
	{
		if (idx < 0 || idx > c.size() - 1)
		{
			printf("Idx is out of range, what are you doing!? \n");
			return nullptr;
		}

		return &c[idx];
	}

	template<typename Container>
	void AddLightsToImgui(const std::string& id, Container& lightList)
	{
		unsigned int counter = 0;
		if (ImGui::TreeNode(id.c_str()))
		{
			for (auto& light : lightList)
			{
				const std::string id("Light" + std::to_string(counter++));
				const bool useRed = light.IsEnabled() == false;
				if (useRed)
				{
					ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 255.0f, 0.0f, 0.0f, 255.0f });
				}

				if (ImGui::TreeNode(id.c_str()))
				{
					light.AddToImgui();
					ImGui::TreePop();
				}

				if (useRed)
				{
					ImGui::PopStyleColor();
				}
			}

			ImGui::TreePop();
		}
	}
}

LightManager::LightManager()
{
}

LightManager::~LightManager()
{
}

//-------------------------------------------------------------------------------------------------
void LightManager::LoadLights()
{
	//Directional Lights
	m_directionalLights[0] = DirectionalLight{ glm::vec4{ 1.0f, 0.0f, 0.0f, 0.0f }, glm::vec4{ 0.1f, 0.1f, 0.1f, 0.0f }, glm::vec4{ 0.6f, 0.6f, 0.6f, 0.0f }, glm::vec4{ 1.0f } };

	//Point Lights
	m_pointLights[0] = PointLight{ glm::vec4{ 0.0f, 10.0f, 0.0f, 0.0f }, 1.0f, 0.22f, 0.2f, 0, glm::vec4{ 1.0f, 1.0f, 0.2f, 0.0f }, glm::vec4{ 1.0f, 1.0f, 0.2f, 0.0f }, glm::vec4{ 0.3f, 0.3f, 0.06f, 0.0f } };
	//m_pointLights[1] = PointLight{ glm::vec4{ 0.0f, -10.0f, 0.0f, 0.0f }, 1.0f, 0.22f, 0.2f, 1, glm::vec4{ 1.0f, 1.0f, 0.2f, 0.0f }, glm::vec4{ 1.0f, 1.0f, 0.2f, 0.0f }, glm::vec4{ 0.3f, 0.3f, 0.06f, 0.0f } };
}

//-------------------------------------------------------------------------------------------------
void LightManager::AddToImGui()
{
	AddLightsToImgui("Directional Lights", m_directionalLights);
	AddLightsToImgui("Point Lights", m_pointLights);
}

//-------------------------------------------------------------------------------------------------
const DirectionalLight* LightManager::GetDirectionalLight(const unsigned int idx) const
{
	return FindLightInArray(m_directionalLights, idx);
}

//-------------------------------------------------------------------------------------------------
const PointLight* LightManager::GetPointLight(const unsigned int idx) const
{
	return FindLightInArray(m_pointLights, idx);
}

