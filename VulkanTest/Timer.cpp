#include "Timer.h"

Timer::Timer() :
	m_startTime(std::chrono::high_resolution_clock::now()),
	m_prevFrameTime(std::chrono::high_resolution_clock::now()),
	m_deltaTime(0.0)
{
}

Timer::~Timer()
{
}

//-------------------------------------------------------------------------------------------------
void Timer::Update()
{
	const std::chrono::high_resolution_clock::time_point currentTime = std::chrono::high_resolution_clock::now();
	m_deltaTime = std::chrono::duration<double, std::chrono::seconds::period>(currentTime - m_prevFrameTime).count();

	m_prevFrameTime = currentTime;
}
//-------------------------------------------------------------------------------------------------
double Timer::DeltaTime() const
{
	return m_deltaTime;
}