#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColour;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 viewPos;
layout(location = 4) in vec3 fragPos;

layout(binding = 1) uniform Light
{
    vec4 dir;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
} light;

layout(binding = 2) uniform Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} material;

layout(binding = 3) uniform sampler2D texSampler;

layout(location = 0) out vec4 outColour;

vec3 CalcDirectionalLighting(vec3 viewDir)
{
    vec3 nNormal = normalize(normal);

    vec3 dir = normalize(-light.dir.rgb);
    float diff = max(dot(nNormal, dir), 0.3f);

    vec3 reflect = reflect(-light.dir.rgb, nNormal);
    float spec = pow(max(dot(reflect, viewDir), 0.0f), material.shininess);

    vec3 ambient = light.ambient.rgb * material.ambient.rgb * texture(texSampler, texCoords).rgb;
    vec3 diffuse = light.diffuse.rgb * diff * texture(texSampler, texCoords).rgb;
    vec3 specular = light.specular.rgb * spec * material.specular.rgb;

    //outColour = diff * material.diffuse * texture(texSampler, texCoords);

    return ambient + diffuse;
}

void main() 
{
    //outColour = texture(texSampler, texCoords);
    //outColour *= vec4(fragColour, 0.0f);

    //outColour = vec4(texCoords, 0.0f, 0.0f);
    //outColour = texture(texSampler, texCoords);

    vec3 newNormal = normalize(normal);
    vec3 viewDir = normalize(viewPos - fragPos);

    outColour = vec4(CalcDirectionalLighting(viewDir), 1.0f);
}