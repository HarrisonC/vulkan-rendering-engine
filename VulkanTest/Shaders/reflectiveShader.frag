#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 vPosition;
layout(location = 1) in vec3 vNormal;
layout(location = 2) in vec3 viewPos;

layout(binding = 1) uniform Light
{
    vec4 dir;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
} light;

layout(binding = 2) uniform Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} material;

layout(binding = 3) uniform samplerCube cubeSampler;

layout(location = 0) out vec4 outColour;

void main() 
{
    vec3 viewVector = vPosition - viewPos;
    float angle = smoothstep( 0.3f, 0.7f, dot(normalize(-viewVector), vNormal));

    vec3 reflectVector = reflect(viewVector, vNormal);
    vec4 reflectColour = texture(cubeSampler, reflectVector);

    vec3 refractVector = refract(viewVector, vNormal, 0.3f);
    vec4 refractColour = texture(cubeSampler, refractVector);

    outColour = mix(reflectColour, refractColour, angle);
}