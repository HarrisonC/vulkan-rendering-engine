#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform ModelMatrix
{
    mat4 matrix;
    vec3 colour;
    double dt;
} model;

layout(binding = 0) uniform VP
{
    mat4 view;
    mat4 proj;
} vp;

const int MAX_BONES = 50;
layout(binding = 5) uniform Bones
{ 
    mat4 transforms[MAX_BONES];
} bones;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColour;
layout(location = 3) in vec2 inTexCoords;
layout(location = 4) in ivec4 inBoneIDs;
layout(location = 5) in vec4 inWeights;

layout(location = 0) out vec3 fragColour;
layout(location = 1) out vec2 texCoords;
layout(location = 2) out vec3 normal;
layout(location = 3) out vec3 viewPos;
layout(location = 4) out vec3 fragPos;

void main() 
{
   mat4 boneMatrix = bones.transforms[inBoneIDs[0]] * inWeights[0];
   boneMatrix += bones.transforms[inBoneIDs[1]] * inWeights[1];
   boneMatrix += bones.transforms[inBoneIDs[2]] * inWeights[2];
   boneMatrix += bones.transforms[inBoneIDs[3]] * inWeights[3];

   gl_Position = vp.proj * vp.view * model.matrix * boneMatrix * vec4(inPosition, 1.0);

   fragPos = vec3(model.matrix * vec4(inPosition, 1.0f));  
   fragColour = inWeights.rgb;
   texCoords = inTexCoords;
   normal = mat3(transpose(inverse(model.matrix))) * mat3(boneMatrix) * inNormal;

   mat4 viewModel = inverse(vp.view * model.matrix);
   viewPos = vec3(viewModel[3]);
}