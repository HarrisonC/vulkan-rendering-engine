#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform ModelMatrix
{
    mat4 matrix;
    vec3 colour;
    double dt;
} model;

layout(binding = 0) uniform VP
{
    mat4 view;
    mat4 proj;
} vp;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColour;
layout(location = 3) in vec2 inTexCoords;

layout(location = 0) out vec3 vPosition;
layout(location = 1) out vec3 vNormal;
layout(location = 2) out vec3 viewPos;

void main() 
{
    gl_Position = vp.proj * vp.view * model.matrix * vec4(inPosition, 1.0f);
    
    vPosition = inPosition;
    vNormal = inNormal;

    mat4 viewModel = inverse(vp.view * model.matrix);
    viewPos = vec3(viewModel[3]);
}