#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColour;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 viewPos;
layout(location = 4) in vec3 fragPos;

#define NUM_OF_DIRECTIONAL_LIGHTS 4
struct DirectionalLight
{
    vec4 dir;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};
layout(binding = 1) uniform DirectionalLights
{
    DirectionalLight dLights[NUM_OF_DIRECTIONAL_LIGHTS];
} directionalLights;

#define NUM_OF_POINT_LIGHTS 12
struct PointLight
{
    vec4 position;

    float constant;
    float linear;
    float quadratic;
    int   enabled;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
};
layout(binding = 2) uniform PointLights
{
    PointLight pLights[NUM_OF_POINT_LIGHTS];
} pointLights;

layout(binding = 3) uniform Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} material;
layout(binding = 4) uniform sampler2D texSampler;


layout(location = 0) out vec4 outColour;

vec3 CalcDirectionalLighting(DirectionalLight light, vec3 nNormal, vec3 viewDir)
{
    vec3 dir = normalize(-light.dir.rgb);
    float diff = max(dot(nNormal, dir), 0.0f);

    vec3 reflectDir = reflect(-light.dir.rgb, nNormal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);

    vec3 ambient = light.ambient.rgb * texture(texSampler, texCoords).rgb;
    vec3 diffuse = light.diffuse.rgb * diff * texture(texSampler, texCoords).rgb;
    vec3 specular = light.specular.rgb * spec * texture(texSampler, texCoords).rgb; 

    return (ambient + diffuse); //Something is up with the specular... Fix this later
}

vec3 CalcPointLighting(PointLight light, vec3 nNormal, vec3 viewDir, vec3 fragPos)
{
    vec3 lightDir = normalize(light.position.rgb - fragPos);
    float diff = max(dot(nNormal, lightDir), 0.0f);

    vec3 reflectDir = reflect(-lightDir, nNormal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);

    float distance = length(light.position.rgb - fragPos);
    float attenuation = 1.0f / ( light.constant + light.linear * distance + light.quadratic * (distance * distance) );

    vec3 ambient = light.ambient.rgb * material.ambient.rgb * texture(texSampler, texCoords).rgb;
    vec3 diffuse = light.diffuse.rgb * diff * texture(texSampler, texCoords).rgb;
    vec3 specular = light.specular.rgb * spec * material.specular.rgb;
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;

    return (ambient + diffuse);
}

void main() 
{
   vec3 viewDir = normalize(viewPos - fragPos);
   vec3 normalisedNormal = normalize(normal);

   vec3 result = CalcDirectionalLighting(directionalLights.dLights[0], normalisedNormal, viewDir);
   for(int i = 1; i < NUM_OF_DIRECTIONAL_LIGHTS; i++)
   {
       if(length(directionalLights.dLights[i].dir) > 0.0f)
       {
           result += CalcDirectionalLighting(directionalLights.dLights[i], normalisedNormal, viewDir);
       }
   }
   
   for(int i = 0; i < NUM_OF_POINT_LIGHTS; i++)
   {
       if(pointLights.pLights[i].enabled > 0)
       {
           result += CalcPointLighting(pointLights.pLights[i], normalisedNormal, viewDir, fragPos);
       }
   }

    outColour = vec4(result, 1.0f);
}