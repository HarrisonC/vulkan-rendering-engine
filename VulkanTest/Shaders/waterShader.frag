#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragColour;
layout(location = 1) in vec2 texCoords;
layout(location = 2) in vec3 normal;
layout(location = 3) in vec3 viewPos;
layout(location = 4) in vec3 fragPos;
layout(location = 5) in vec3 vPosition;

layout(binding = 1) uniform Light
{
    vec4 dir;
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
} light;

layout(binding = 2) uniform Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
} material;

layout(binding = 3) uniform sampler2D reflectSampler;
//layout(binding = 4) uniform sampler2D refractSampler;

layout(location = 0) out vec4 outColour;

void main() 
{
    //outColour = texture(texSampler, texCoords);

    //outColour = vec4(texCoords, 0.0f, 0.0f);
    //outColour = texture(texSampler, texCoords);

    //vec3 newNormal = normalize(normal);
    //vec3 viewDir = normalize(viewPos - fragPos);

    //outColour = vec4(CalcDirectionalLighting(viewDir), 1.0f);

    //outColour = vec4(texCoords, 0.0f, 1.0f);

    //vec3 viewVector = vPosition - viewPos;
    //float angle = smoothstep( 0.3f, 0.7f, dot(normalize(-viewVector), normal));
    //
    //vec3 reflectVector = reflect(viewVector, normal);
    //vec4 reflectColour = texture(cubeSampler, reflectVector);
    //
    //vec3 refractVector = refract(viewVector, normal, 0.3f);
    //vec4 refractColour = texture(cubeSampler, refractVector);
    //
    //outColour = mix(reflectColour, refractColour, angle);

    outColour = texture(reflectSampler, texCoords);
}