#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform ModelMatrix
{
    mat4 matrix;
    vec3 colour;
    double dt;
} model;

layout(binding = 0) uniform VP
{
    mat4 view;
    mat4 proj;
} vp;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColour;
layout(location = 3) in vec2 inTexCoords;

layout(location = 0) out vec3 texCoords;

void main() 
{
   gl_Position = vp.proj * vp.view * model.matrix *  vec4(inPosition, 1.0);
   texCoords = inPosition;
}