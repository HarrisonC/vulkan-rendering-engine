#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform ModelMatrix
{
    mat4 matrix;
    vec3 colour;
	double dt;
} model;

layout(binding = 0) uniform VP
{
    mat4 view;
    mat4 proj;
} vp;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColour;
layout(location = 3) in vec2 inTexCoords;

layout(location = 0) out vec3 fragColour;
layout(location = 1) out vec2 texCoords;
layout(location = 2) out vec3 normal;
layout(location = 3) out vec3 viewPos;
layout(location = 4) out vec3 fragPos;
layout(location = 5) out vec3 vPosition;

float UpdateWave(vec2 pos, double currentTime, float wavelength, float speed, float height, vec2 direction)
{
	float A = height;
	float L = wavelength;
	float S = speed;
	vec2  D = direction;

	float w = 2 / L;
	float phi = S * w;
	float t = float(currentTime);

	return A * sin(dot(D, pos) * w + t * phi);

	//float k = 1.5f;
	//float start = 2 * A;
	//float inner = sin(dot(D, pos) * w + t * phi) + 1;
	//
	//return pow(start * (inner / 2), k);
	
}

void main() 
{
   //gl_Position = mvp.proj * vp.view * vp.model * vec4(inPosition, 1.0);
   //fragColour = model.colour;

   vec4 newPos = vec4(inPosition, 1.0f);
   newPos.z += UpdateWave(vec2(inPosition.xy), model.dt, 5.0f, 10.0f, 4.0f, vec2(1.0f, 0.0f));

   fragPos = vec3(model.matrix * newPos);
   gl_Position = vp.proj * vp.view * model.matrix * newPos;
   
   fragColour = inColour;
   texCoords = inTexCoords;
   normal = mat3(transpose(inverse(model.matrix))) * inNormal;

   mat4 viewModel = inverse(vp.view * model.matrix);
   viewPos = vec3(viewModel[3]);

   vPosition = inPosition;
}