#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 texCoords;
layout(binding = 2) uniform samplerCube cubeMap;

layout(location = 0) out vec4 outColour;

void main()
{
	outColour = texture(cubeMap, texCoords);
}
