#ifndef _DEAR_IMGUI_H_
#define _DEAR_IMGUI_H_

#include <vulkan/vulkan.h>
#include <vector>

class VulkanCore;
class ImGuiShader;
struct ImGuiComponent;

class DearImGui
{
private:
	bool CreateCommandPools		();
	bool CreateCommandBuffers	();
	bool CreateFrameBuffers		();
	bool CreateRenderPass		();
	void LoadFonts				();

private:
	using ComponentList = std::vector<ImGuiComponent*>;

	VulkanCore*		m_core;
	ImGuiShader*	m_shader;
	ComponentList	m_componentsToDraw;

	//Arguably, this pool isn't needed, we could just allocate from the pool in core
	//The only reason I'm not doing this is for future-proofing and for practise
	VkCommandPool					m_commandPool;
	std::vector<VkCommandBuffer>	m_commandBuffers;
	std::vector<VkFramebuffer>		m_frameBuffers;
	VkRenderPass					m_renderPass;

public:
	DearImGui();

	bool Initialise			();
	void CleanUp			();
	void CleanUpSwapchain	();

	void StartNewImGuiFrame	();
	void Draw				(const uint32_t currentImage);

	void AddComponent		(ImGuiComponent& c) { m_componentsToDraw.push_back(&c); }

	//----- Getters -----
	const VkCommandBuffer&	GetCommandBuffer(const uint32_t currentImage) { return m_commandBuffers[currentImage]; }
};

#endif
