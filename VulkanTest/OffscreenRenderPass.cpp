#include "OffscreenRenderPass.h"
#include "VulkanCore.h"
#include "Texture.h"

OffscreenRenderPass::OffscreenRenderPass(const VkExtent2D& extents, const std::vector<OffscreenRenderPass::FrameBufferImageInfo>& imageInfos) :
	m_extents(extents)
{
	if (imageInfos.empty() == false)
	{
		CreateImagesFromInfo(imageInfos);
		CreateImageSampler();
		CreateRenderPass();
		CreateFrameBuffer();

		m_descriptorInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		m_descriptorInfo.imageView = m_images[0].m_imageView;
		m_descriptorInfo.sampler = m_sampler;

		m_texture = new Texture(m_images[0].m_image, m_images[0].m_imageMemory, m_images[0].m_imageView, m_sampler);
	}
	else
	{
		printf("FAILED TO CREATE OffscreenRenderPass - No Info Given! \n");
	}
}

OffscreenRenderPass::OffscreenRenderPass(OffscreenRenderPass&& rhs)
{
	//Copy/move values
	m_extents = std::move(rhs.m_extents);
	m_images = std::move(rhs.m_images);

	m_frameBuffer = rhs.m_frameBuffer;
	m_renderPass = rhs.m_renderPass;
	m_sampler = rhs.m_sampler;
	m_descriptorInfo = std::move(rhs.m_descriptorInfo);

	m_colourFormat = rhs.m_colourFormat;
	m_depthFormat = m_depthFormat;

	//Null rhs
	rhs.m_images.clear();

	rhs.m_frameBuffer = nullptr;
	rhs.m_renderPass = nullptr;
	rhs.m_sampler = nullptr;
}

OffscreenRenderPass& OffscreenRenderPass::operator=(OffscreenRenderPass&& rhs)
{
	//Copy/move values
	m_extents = std::move(rhs.m_extents);
	m_images = std::move(rhs.m_images);

	m_frameBuffer = rhs.m_frameBuffer;
	m_renderPass = rhs.m_renderPass;
	m_sampler = rhs.m_sampler;
	m_descriptorInfo = std::move(rhs.m_descriptorInfo);
	m_texture = rhs.m_texture;

	m_colourFormat = rhs.m_colourFormat;
	m_depthFormat = m_depthFormat;

	//Null rhs
	rhs.m_images.clear();

	rhs.m_frameBuffer = nullptr;
	rhs.m_renderPass = nullptr;
	rhs.m_sampler = nullptr;
	rhs.m_texture = nullptr;

	return *this;
}

OffscreenRenderPass::~OffscreenRenderPass()
{
	CleanUp();
}

//-------------------------------------------------------------------------------------------------
bool OffscreenRenderPass::CreateImagesFromInfo(const std::vector<OffscreenRenderPass::FrameBufferImageInfo>& infos)
{
	bool anyFailed = false;
	for (const auto& info : infos)
	{
		FrameBufferImage& currentImage = m_images.emplace_back();

		//Assumes order is always color then depth - this is hellah bad!
		if (m_colourFormat == VK_FORMAT_UNDEFINED)
		{
			m_colourFormat = info.format;
		}
		else if (m_depthFormat == VK_FORMAT_UNDEFINED)
		{
			m_depthFormat = info.format;
		}

		if (!VulkanCore::Singleton().CreateImage(m_extents.width, m_extents.height, info.format, info.tiling, info.usageFlags, info.memFlags, currentImage.m_image, currentImage.m_imageMemory))
		{
			printf("Failed to create image for offscreen pass, this ain't good! \n");
			anyFailed = true;
		}

		if (!VulkanCore::Singleton().CreateImageView(currentImage.m_imageView, currentImage.m_image, info.format, info.aspectFlags))
		{
			printf("Failed to create image view for offscreen pass, this ain't good! \n");
			anyFailed = true;
		}
	}

	return anyFailed;
}

//-------------------------------------------------------------------------------------------------
bool OffscreenRenderPass::CreateImageSampler()
{
	VkSamplerCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	createInfo.addressModeU = VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	createInfo.addressModeV = VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	createInfo.addressModeW = VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
	createInfo.anisotropyEnable = true;
	createInfo.maxAnisotropy = 16;
	createInfo.mipmapMode = VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_LINEAR;
	createInfo.minFilter = VkFilter::VK_FILTER_LINEAR;
	createInfo.magFilter = VkFilter::VK_FILTER_LINEAR;
	createInfo.mipLodBias = 0.0f;
	createInfo.minLod = 0.0f;
	createInfo.maxLod = 1.0f;
	createInfo.compareEnable = false;
	createInfo.compareOp = VK_COMPARE_OP_ALWAYS;

	if (vkCreateSampler(VulkanCore::Singleton().Device(), &createInfo, nullptr, &m_sampler) != VK_SUCCESS)
	{
		printf("Failed to create sampler for offscreen pass! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool OffscreenRenderPass::CreateRenderPass()
{
	//Create attachments
	constexpr uint8_t attachmentCount = 2;
	VkAttachmentDescription attachments[attachmentCount];
	//This is the swapchain rendering attachment
	attachments[0].format = m_colourFormat;
	attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[0].finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	attachments[0].flags = 0;

	//This is the depth buffer attachment
	attachments[1].format = m_depthFormat;
	attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
	attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
	attachments[1].flags = 0;

	VkAttachmentReference colourReference = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
	VkAttachmentReference depthReference = { 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };

	//Create subpass and link to attachments
	VkSubpassDescription subpassDesc = {};
	subpassDesc.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDesc.colorAttachmentCount = 1;
	subpassDesc.pColorAttachments = &colourReference;
	subpassDesc.pDepthStencilAttachment = &depthReference;

	constexpr uint8_t dependencyCount = 2;
	VkSubpassDependency subpassDependencies[dependencyCount];
	subpassDependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependencies[0].dstSubpass = 0;
	subpassDependencies[0].srcStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	subpassDependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[0].srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
	subpassDependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	subpassDependencies[1].srcSubpass = 0;
	subpassDependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
	subpassDependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	subpassDependencies[1].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	subpassDependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	subpassDependencies[1].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	subpassDependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	//Create the render pass
	VkRenderPassCreateInfo renderInfo = {};
	renderInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderInfo.subpassCount = 1;
	renderInfo.pSubpasses = &subpassDesc;
	renderInfo.dependencyCount = dependencyCount;
	renderInfo.pDependencies = subpassDependencies;
	renderInfo.attachmentCount = attachmentCount;
	renderInfo.pAttachments = attachments;

	if (vkCreateRenderPass(VulkanCore::Singleton().Device(), &renderInfo, nullptr, &m_renderPass) != VK_SUCCESS)
	{
		printf("Failed to create render pass for offscreen pass! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
bool OffscreenRenderPass::CreateFrameBuffer()
{
	std::vector<VkImageView> attachments;
	attachments.reserve(m_images.size());

	for (const auto& image : m_images)
	{
		attachments.push_back(image.m_imageView);
	}

	VkFramebufferCreateInfo framebufferInfo{};
	framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	framebufferInfo.renderPass = m_renderPass;
	framebufferInfo.pAttachments = attachments.data();
	framebufferInfo.attachmentCount = (uint32_t)attachments.size();
	framebufferInfo.width = m_extents.width;
	framebufferInfo.height = m_extents.height;
	framebufferInfo.layers = 1;

	if (vkCreateFramebuffer(VulkanCore::Singleton().Device(), &framebufferInfo, nullptr, &m_frameBuffer) != VK_SUCCESS)
	{
		printf("Failed to create framebuffer for offscreen pass! \n");
		return false;
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
VkRenderPassBeginInfo OffscreenRenderPass::GetRenderPassInfo() const
{
	VkRenderPassBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	beginInfo.renderPass = m_renderPass;
	beginInfo.framebuffer = m_frameBuffer;
	beginInfo.renderArea.offset = { 0, 0 };
	beginInfo.renderArea.extent = m_extents;

	return beginInfo;
}

//-------------------------------------------------------------------------------------------------
void OffscreenRenderPass::CleanUp()
{
	if (m_cleanedUp == false)
	{
		m_cleanedUp = true;

		auto& device = VulkanCore::Singleton().Device();

		vkDestroyFramebuffer(device, m_frameBuffer, nullptr);
		vkDestroyRenderPass(device, m_renderPass, nullptr);
		vkDestroySampler(device, m_sampler, nullptr);

		for (auto& image : m_images)
		{
			vkDestroyImageView(device, image.m_imageView, nullptr);

			if (image.m_imageMemory != nullptr)
			{
				vkDestroyImage(device, image.m_image, nullptr);
				vkFreeMemory(device, image.m_imageMemory, nullptr);
			}
		}
		m_images.clear();
	}
}