#ifndef _LIGHT_H_
#define _LIGHT_H_ 

#include <glm/vec4.hpp>
#include "ImGui/imgui_helper_functions.h"
#include "ImGui/imgui.h"

struct DirectionalLight 
{
public:
	glm::vec4	m_direction;
	glm::vec4	m_ambient;
	glm::vec4	m_diffuse;
	glm::vec4	m_specular;

	DirectionalLight() = default;
	DirectionalLight(const glm::vec4& direction, const glm::vec4& ambient, const glm::vec4& diffuse, const glm::vec4& spec) :
		m_direction(direction),
		m_ambient(ambient),
		m_diffuse(diffuse),
		m_specular(spec)
	{
	}

	bool IsEnabled	() const { return m_direction != glm::vec4(0.0f); }
	void AddToImgui	()
	{
		auto CreateEntry = [](const char* txt, glm::vec4& data) 
		{
			ImGui::Text(txt);
			ImGui::NextColumn();
			ImGuiHelper::CreateSameLineBoxForVector(txt, data);
			ImGui::NextColumn();
			ImGui::Separator();
		};

		ImGui::Columns(2);
		ImGui::SetColumnWidth(0, 96);
		ImGui::SetColumnWidth(1, 384);
		ImGui::Separator();

		CreateEntry("Direction", m_direction);
		CreateEntry("Ambient",	 m_ambient);
		CreateEntry("Diffuse",	 m_diffuse);
		CreateEntry("Specular",	 m_specular);

		ImGui::Columns(1);
	}
};

//-------------------------------------------------------------------------------------------------
struct PointLight
{
public:
	glm::vec4	m_position;

	float		m_constant;
	float		m_linear;
	float		m_quadratic;
	int			m_enabled;

	glm::vec4	m_ambient;
	glm::vec4	m_diffuse;
	glm::vec4	m_specular;


	PointLight() = default;
	PointLight(const glm::vec4& pos, const float constant, const float linear, const float quadratic, const int enabled, const glm::vec4& ambient, const glm::vec4& diffuse, const glm::vec4& spec) :
		m_position(pos),
		m_constant(constant),
		m_linear(linear),
		m_quadratic(quadratic),
		m_enabled(enabled),
		m_ambient(ambient),
		m_diffuse(diffuse),
		m_specular(spec)
	{
	}

	bool IsEnabled	() const { return m_enabled > 0; }
	void AddToImgui	()
	{
		ImGui::InputInt("Enabled", &m_enabled);
		ImGuiHelper::CreateSameLineHeaderForVector("Position", m_position);
		ImGuiHelper::CreateSameLineHeaderForVector("Ambient", m_ambient);
		ImGuiHelper::CreateSameLineHeaderForVector("Diffuse", m_diffuse);
		ImGuiHelper::CreateSameLineHeaderForVector("Specular", m_specular);

		if (ImGui::TreeNode("Attenuation"))
		{
			ImGui::InputFloat("constant", &m_constant);
			ImGui::InputFloat("linear", &m_linear);
			ImGui::InputFloat("quadratic", &m_quadratic);
			ImGui::TreePop();
		}
	}
};

#endif