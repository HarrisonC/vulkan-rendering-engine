#include "RenderObject.h"

#include <stb_image.h>
#include <algorithm>
#include <array>

#include "VulkanCore.h"
#include "Shader.h"
#include "Texture.h"
#include "Model.h"
#include "AnimatedModel.h"

RenderObject::RenderObject(const Model& model, const Texture& texture, Shader& shader, const bool skipInit) :
	m_model(model),
	m_shader(shader)
{
	if (skipInit == false)
	{
		Initialise(texture);
	}
}

RenderObject::RenderObject(const Model& model, std::vector<const Texture*> textures, Shader& shader) :
	m_model(model),
	m_shader(shader)
{
	m_material = Material{ glm::vec4{ 1.0f }, glm::vec4{ 0.0f, 0.8f, 0.8f, 1.0f }, glm::vec4{ 1.0f }, 0.1f };

	CreateUniformBuffers();
	CreateDescriptorSets(textures);
}

RenderObject::RenderObject(const Model& model, const CubeTexture& texture, Shader& shader) :
	m_model(model),
	m_shader(shader),
	m_material(std::nullopt)
{
}

RenderObject::~RenderObject()
{
	CleanUp();
}

void RenderObject::Initialise(const Texture& texture)
{
	m_material = Material{ glm::vec4{ 1.0f }, glm::vec4{ 0.0f, 0.8f, 0.8f, 1.0f }, glm::vec4{ 1.0f }, 0.1f };

	CreateUniformBuffers();
	CreateDescriptorSets(texture);
}

//-------------------------------------------------------------------------------------------------
void RenderObject::Draw(const uint32_t currentImage, VkCommandBuffer& buffer, const VkPipelineLayout& pipelineLayout) const
{
	m_model.Draw(m_descriptorSets[currentImage], buffer, pipelineLayout);
}


//-------------------------------------------------------------------------------------------------
void RenderObject::ChangeTexture(const Texture& texture)
{
	UpdateDescriptorSets(texture);
}
//-------------------------------------------------------------------------------------------------
void RenderObject::AddToImGui()
{
	ImGui::Columns(2);
	ImGui::SetColumnWidth(0, 96);
	ImGui::SetColumnWidth(1, 384);

	ImGui::Text("Change Texture");
	ImGui::NextColumn();
	const Texture* chosenTexture = VulkanCore::Singleton().TextureManager().AddToImGui();
	if (chosenTexture != nullptr)
	{
		ChangeTexture(*chosenTexture);
	}
	ImGui::NextColumn();
}

//-------------------------------------------------------------------------------------------------
void RenderObject::CreateUniformBuffers()
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();
	VkDeviceSize size = sizeof(Material);

	m_materialUniformBuffers.resize(swapchainImageCount);
	m_materialUniformMemories.resize(swapchainImageCount);

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VulkanCore::Singleton().CreateBuffer(size, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_materialUniformBuffers[i], m_materialUniformMemories[i]);
		UpdateUniformBuffers(i);
	}
}

//-------------------------------------------------------------------------------------------------
void RenderObject::UpdateUniformBuffers(const uint32_t currentImage)
{
	if (m_material.has_value())
	{
		VkDevice& device = VulkanCore::Singleton().Device();

		void* data;
		vkMapMemory(device, m_materialUniformMemories[currentImage], 0, sizeof(Material), VK_NULL_HANDLE, &data);
		memcpy(data, &m_material, sizeof(Material));
		vkUnmapMemory(device, m_materialUniformMemories[currentImage]);
	}
}

//-------------------------------------------------------------------------------------------------
bool RenderObject::CreateDescriptorSets(const Texture& texture)
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();

	std::vector<VkDescriptorSetLayout> layouts(swapchainImageCount, m_shader.GetDescriptorSetLayout());
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.pSetLayouts = layouts.data();
	allocInfo.descriptorPool = m_shader.GetDescriptorPool();
	allocInfo.descriptorSetCount = swapchainImageCount;

	m_descriptorSets.resize(swapchainImageCount);
	if (vkAllocateDescriptorSets(VulkanCore::Singleton().Device(), &allocInfo, m_descriptorSets.data()) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to allocate descriptor sets! Have we run out of sets (Check max set on descriptor pool) \n");
		return false;
	}

	UpdateDescriptorSets(texture);

	return true;
}

bool RenderObject::CreateDescriptorSets(std::vector<const Texture*> textures)
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();

	std::vector<VkDescriptorSetLayout> layouts(swapchainImageCount, m_shader.GetDescriptorSetLayout());
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.pSetLayouts = layouts.data();
	allocInfo.descriptorPool = m_shader.GetDescriptorPool();
	allocInfo.descriptorSetCount = swapchainImageCount;

	m_descriptorSets.resize(swapchainImageCount);
	if (vkAllocateDescriptorSets(VulkanCore::Singleton().Device(), &allocInfo, m_descriptorSets.data()) != VK_SUCCESS)
	{
		printf("FATAL ERROR - Failed to allocate descriptor sets! \n");
		return false;
	}

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VPLightShader& shader = static_cast<VPLightShader&>(m_shader);

		VkMemoryRequirements memReqs = {};
		vkGetBufferMemoryRequirements(VulkanCore::Singleton().Device(), shader.GetVpUniformBuffers()[i], &memReqs);

		VkDescriptorBufferInfo vpBufferInfo = {};
		vpBufferInfo.buffer = shader.GetVpUniformBuffers()[i];
		vpBufferInfo.offset = 0;
		vpBufferInfo.range = sizeof(VPShader::VP);

		VkDescriptorBufferInfo directionalLightBufferInfo = {};
		directionalLightBufferInfo.buffer = shader.GetDirectionalLightBuffers()[i];
		directionalLightBufferInfo.offset = 0;
		directionalLightBufferInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo pointLightBufferInfo = {};
		pointLightBufferInfo.buffer = shader.GetPointLightBuffers()[i];
		pointLightBufferInfo.offset = 0;
		pointLightBufferInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo materialBufferInfo = {};
		materialBufferInfo.buffer = m_materialUniformBuffers[i];
		materialBufferInfo.offset = 0;
		materialBufferInfo.range = sizeof(Material);

		std::vector<VkDescriptorImageInfo> imageInfos;
		for (const auto& texture : textures)
		{
			VkDescriptorImageInfo imageInfo = {};
			imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo.imageView = (*texture).m_textureImageView;
			imageInfo.sampler = (*texture).m_textureSampler;

			imageInfos.push_back(std::move(imageInfo));
		}

		std::vector<VkWriteDescriptorSet> sets;
		sets.resize(4);

		sets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[0].dstSet = m_descriptorSets[i];
		sets[0].dstBinding = 0;
		sets[0].dstArrayElement = 0;
		sets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[0].descriptorCount = 1;
		sets[0].pBufferInfo = &vpBufferInfo;
		sets[0].pImageInfo = nullptr;
		sets[0].pTexelBufferView = nullptr;

		sets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[1].dstSet = m_descriptorSets[i];
		sets[1].dstBinding = 1;
		sets[1].dstArrayElement = 0;
		sets[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[1].descriptorCount = 1;
		sets[1].pBufferInfo = &directionalLightBufferInfo;
		sets[1].pImageInfo = nullptr;
		sets[1].pTexelBufferView = nullptr;

		sets[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[2].dstSet = m_descriptorSets[i];
		sets[2].dstBinding = 2;
		sets[2].dstArrayElement = 0;
		sets[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[2].descriptorCount = 1;
		sets[2].pBufferInfo = &pointLightBufferInfo;
		sets[2].pImageInfo = nullptr;
		sets[2].pTexelBufferView = nullptr;

		sets[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[3].dstSet = m_descriptorSets[i];
		sets[3].dstBinding = 3;
		sets[3].dstArrayElement = 0;
		sets[3].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[3].descriptorCount = 1;
		sets[3].pBufferInfo = &materialBufferInfo;
		sets[3].pImageInfo = nullptr;
		sets[3].pTexelBufferView = nullptr;

		uint32_t binding = 0;
		for (const auto& imageInfo : imageInfos)
		{
			sets.emplace_back(VkWriteDescriptorSet{});

			sets.back().sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			sets.back().dstSet = m_descriptorSets[i];
			sets.back().dstBinding = 4 + binding;
			sets.back().dstArrayElement = 0;
			sets.back().descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			sets.back().descriptorCount = 1;
			sets.back().pBufferInfo = nullptr;
			sets.back().pImageInfo = &imageInfo;
			sets.back().pTexelBufferView = nullptr;

			binding++;
		}

		vkUpdateDescriptorSets(VulkanCore::Singleton().Device(), (uint32_t)sets.size(), sets.data(), 0, nullptr);
	}

	return true;
}

//-------------------------------------------------------------------------------------------------
void RenderObject::UpdateDescriptorSets(const Texture& texture)
{
	//#TODO: FIX ME - This is dumb - Why update the lights for every render object? We're only really interested in the texture and material
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();
	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VPLightShader& shader = static_cast<VPLightShader&>(m_shader);

		VkMemoryRequirements memReqs = {};
		vkGetBufferMemoryRequirements(VulkanCore::Singleton().Device(), shader.GetVpUniformBuffers()[i], &memReqs);

		VkDescriptorBufferInfo vpBufferInfo = {};
		vpBufferInfo.buffer = shader.GetVpUniformBuffers()[i];
		vpBufferInfo.offset = 0;
		vpBufferInfo.range = sizeof(VPShader::VP);

		VkDescriptorBufferInfo directionalLightBufferInfo = {};
		directionalLightBufferInfo.buffer = shader.GetDirectionalLightBuffers()[i];
		directionalLightBufferInfo.offset = 0;
		directionalLightBufferInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo pointLightBufferInfo = {};
		pointLightBufferInfo.buffer = shader.GetPointLightBuffers()[i];
		pointLightBufferInfo.offset = 0;
		pointLightBufferInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo materialBufferInfo = {};
		materialBufferInfo.buffer = m_materialUniformBuffers[i];
		materialBufferInfo.offset = 0;
		materialBufferInfo.range = sizeof(Material);

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = texture.m_textureImageView;
		imageInfo.sampler = texture.m_textureSampler;

		std::array<VkWriteDescriptorSet, 5> sets = {};
		sets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[0].dstSet = m_descriptorSets[i];
		sets[0].dstBinding = 0;
		sets[0].dstArrayElement = 0;
		sets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[0].descriptorCount = 1;
		sets[0].pBufferInfo = &vpBufferInfo;
		sets[0].pImageInfo = nullptr;
		sets[0].pTexelBufferView = nullptr;

		sets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[1].dstSet = m_descriptorSets[i];
		sets[1].dstBinding = 1;
		sets[1].dstArrayElement = 0;
		sets[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[1].descriptorCount = 1;
		sets[1].pBufferInfo = &directionalLightBufferInfo;
		sets[1].pImageInfo = nullptr;
		sets[1].pTexelBufferView = nullptr;

		sets[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[2].dstSet = m_descriptorSets[i];
		sets[2].dstBinding = 2;
		sets[2].dstArrayElement = 0;
		sets[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[2].descriptorCount = 1;
		sets[2].pBufferInfo = &pointLightBufferInfo;
		sets[2].pImageInfo = nullptr;
		sets[2].pTexelBufferView = nullptr;

		sets[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[3].dstSet = m_descriptorSets[i];
		sets[3].dstBinding = 3;
		sets[3].dstArrayElement = 0;
		sets[3].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[3].descriptorCount = 1;
		sets[3].pBufferInfo = &materialBufferInfo;
		sets[3].pImageInfo = nullptr;
		sets[3].pTexelBufferView = nullptr;

		sets[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[4].dstSet = m_descriptorSets[i];
		sets[4].dstBinding = 4;
		sets[4].dstArrayElement = 0;
		sets[4].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		sets[4].descriptorCount = 1;
		sets[4].pBufferInfo = nullptr;
		sets[4].pImageInfo = &imageInfo;
		sets[4].pTexelBufferView = nullptr;

		vkUpdateDescriptorSets(VulkanCore::Singleton().Device(), (uint32_t)sets.size(), sets.data(), 0, nullptr);
	}
}


//-------------------------------------------------------------------------------------------------
void RenderObject::CleanUp()
{
	VkDevice& device = VulkanCore::Singleton().Device();

	for (int32_t i = 0; i < VulkanCore::Singleton().SwampchainImageCount(); ++i)
	{
		vkFreeMemory(device, m_materialUniformMemories[i], nullptr);
		vkDestroyBuffer(device, m_materialUniformBuffers[i], nullptr);
	}
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
AnimRenderObject::AnimRenderObject(const Model& model, const Texture& texture, Shader& shader) :
	RenderObject(model, texture, shader, true)
{
	Initialise(texture);
}

//-------------------------------------------------------------------------------------------------
void AnimRenderObject::Initialise(const Texture& texture)
{
	m_material = Material{ glm::vec4{ 1.0f }, glm::vec4{ 0.0f, 0.8f, 0.8f, 1.0f }, glm::vec4{ 1.0f }, 0.1f };

	CreateUniformBuffers();
	CreateDescriptorSets(texture);
}

void AnimRenderObject::Update(const double dt, const uint32_t currentImage)
{
	if (m_isAnimPaused == false)
	{
		m_animTime += dt;

		const AnimatedModel& model = static_cast<const AnimatedModel&>(m_model);
		if (m_animTime > model.GetAnimation().GetDuration())
		{
			m_animTime = 0.0;
		}

		UpdateUniformBuffers(currentImage);
	}
}

//-------------------------------------------------------------------------------------------------
void AnimRenderObject::AddToImGui()
{
	RenderObject::AddToImGui();

	ImGui::Columns(2);
	ImGui::SetColumnWidth(0, 96);
	ImGui::SetColumnWidth(1, 384);
	ImGui::Separator();

	ImGui::Text("Pause Anim");
	ImGui::NextColumn();
	ImGui::Checkbox("##Pause Anim", &m_isAnimPaused);
	ImGui::NextColumn();
	ImGui::Separator();

	ImGui::Text("Anim Frame");
	ImGui::NextColumn();
	const AnimatedModel& model = static_cast<const AnimatedModel&>(m_model);
	int frame = model.GetAnimation().GetFrameForTime(m_animTime);
	if (ImGui::SliderInt("##Anim Frame", &frame, 0, model.GetAnimation().GetNumOfFrames()))
	{
		m_animTime = model.GetAnimation().ConvertFrameToMS(frame);
		UpdateUniformBuffers(0);
		UpdateUniformBuffers(1);
	}
	ImGui::NextColumn();
	ImGui::Separator();

	ImGui::Columns(1);
}

//-------------------------------------------------------------------------------------------------
void AnimRenderObject::CreateUniformBuffers()
{
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();
	VkDeviceSize materialSize = sizeof(Material);
	VkDeviceSize transformsSize = sizeof(glm::mat4) * AnimatedModel::MAX_BONES;

	m_materialUniformBuffers.resize(swapchainImageCount);
	m_materialUniformMemories.resize(swapchainImageCount);

	m_boneTransformsUniformBuffers.resize(swapchainImageCount);
	m_boneTransformsUniformMemories.resize(swapchainImageCount);

	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		VulkanCore::Singleton().CreateBuffer(materialSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_materialUniformBuffers[i], m_materialUniformMemories[i]);
		VulkanCore::Singleton().CreateBuffer(transformsSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, m_boneTransformsUniformBuffers[i], m_boneTransformsUniformMemories[i]);
		UpdateUniformBuffers(i);
	}
}

//-------------------------------------------------------------------------------------------------
void AnimRenderObject::UpdateUniformBuffers(const uint32_t currentImage)
{
	RenderObject::UpdateUniformBuffers(currentImage);

	VkDevice& device = VulkanCore::Singleton().Device();

	const AnimatedModel& model = static_cast<const AnimatedModel&>(m_model);
	std::vector<glm::mat4> transforms = model.GetSkeletonTransformsAtTime(m_animTime);

	const VkDeviceSize bufferSize = sizeof(transforms[0]) * transforms.size();

	void* data;
	vkMapMemory(device, m_boneTransformsUniformMemories[currentImage], 0, bufferSize, VK_NULL_HANDLE, &data);
	memcpy(data, transforms.data(), bufferSize);
	vkUnmapMemory(device, m_boneTransformsUniformMemories[currentImage]);
}

//-------------------------------------------------------------------------------------------------
void AnimRenderObject::UpdateDescriptorSets(const Texture& texture)
{
	//FIX ME - This is dumb - Why update the lights for every render object? We're only really interested in the texture and material
	const uint32_t swapchainImageCount = VulkanCore::Singleton().SwampchainImageCount();
	for (uint32_t i = 0; i < swapchainImageCount; ++i)
	{
		AnimLightShader& shader = static_cast<AnimLightShader&>(m_shader);

		VkMemoryRequirements memReqs = {};
		vkGetBufferMemoryRequirements(VulkanCore::Singleton().Device(), shader.GetVpUniformBuffers()[i], &memReqs);

		VkDescriptorBufferInfo vpBufferInfo = {};
		vpBufferInfo.buffer = shader.GetVpUniformBuffers()[i];
		vpBufferInfo.offset = 0;
		vpBufferInfo.range = sizeof(VPShader::VP);

		VkDescriptorBufferInfo directionalLightBufferInfo = {};
		directionalLightBufferInfo.buffer = shader.GetDirectionalLightBuffers()[i];
		directionalLightBufferInfo.offset = 0;
		directionalLightBufferInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo pointLightBufferInfo = {};
		pointLightBufferInfo.buffer = shader.GetPointLightBuffers()[i];
		pointLightBufferInfo.offset = 0;
		pointLightBufferInfo.range = VK_WHOLE_SIZE;

		VkDescriptorBufferInfo materialBufferInfo = {};
		materialBufferInfo.buffer = m_materialUniformBuffers[i];
		materialBufferInfo.offset = 0;
		materialBufferInfo.range = sizeof(Material);

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = texture.m_textureImageView;
		imageInfo.sampler = texture.m_textureSampler;

		VkDescriptorBufferInfo boneTransformsInfo = {};
		boneTransformsInfo.buffer = m_boneTransformsUniformBuffers[i];
		boneTransformsInfo.offset = 0;
		boneTransformsInfo.range = VK_WHOLE_SIZE;

		std::array<VkWriteDescriptorSet, 6> sets = {};
		sets[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[0].dstSet = m_descriptorSets[i];
		sets[0].dstBinding = 0;
		sets[0].dstArrayElement = 0;
		sets[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[0].descriptorCount = 1;
		sets[0].pBufferInfo = &vpBufferInfo;
		sets[0].pImageInfo = nullptr;
		sets[0].pTexelBufferView = nullptr;

		sets[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[1].dstSet = m_descriptorSets[i];
		sets[1].dstBinding = 1;
		sets[1].dstArrayElement = 0;
		sets[1].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[1].descriptorCount = 1;
		sets[1].pBufferInfo = &directionalLightBufferInfo;
		sets[1].pImageInfo = nullptr;
		sets[1].pTexelBufferView = nullptr;

		sets[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[2].dstSet = m_descriptorSets[i];
		sets[2].dstBinding = 2;
		sets[2].dstArrayElement = 0;
		sets[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[2].descriptorCount = 1;
		sets[2].pBufferInfo = &pointLightBufferInfo;
		sets[2].pImageInfo = nullptr;
		sets[2].pTexelBufferView = nullptr;

		sets[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[3].dstSet = m_descriptorSets[i];
		sets[3].dstBinding = 3;
		sets[3].dstArrayElement = 0;
		sets[3].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[3].descriptorCount = 1;
		sets[3].pBufferInfo = &materialBufferInfo;
		sets[3].pImageInfo = nullptr;
		sets[3].pTexelBufferView = nullptr;

		sets[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[4].dstSet = m_descriptorSets[i];
		sets[4].dstBinding = 4;
		sets[4].dstArrayElement = 0;
		sets[4].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		sets[4].descriptorCount = 1;
		sets[4].pBufferInfo = nullptr;
		sets[4].pImageInfo = &imageInfo;
		sets[4].pTexelBufferView = nullptr;

		sets[5].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		sets[5].dstSet = m_descriptorSets[i];
		sets[5].dstBinding = 5;
		sets[5].dstArrayElement = 0;
		sets[5].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		sets[5].descriptorCount = 1;
		sets[5].pBufferInfo = &boneTransformsInfo;
		sets[5].pImageInfo = nullptr;
		sets[5].pTexelBufferView = nullptr;

		vkUpdateDescriptorSets(VulkanCore::Singleton().Device(), (uint32_t)sets.size(), sets.data(), 0, nullptr);
	}
}