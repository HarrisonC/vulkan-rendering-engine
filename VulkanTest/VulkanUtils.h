#ifndef	_VULKAN_UTILS_H_
#define _VULKAN_UTILS_H_

#include <vulkan/vulkan.h>

namespace VulkanUtils
{
	struct Buffer
	{
	private:
		VkDevice m_device;

	public:
		VkBuffer		m_buffer;
		VkDeviceMemory	m_memory;
		bool m_cleanedUp;

		Buffer(VkDevice& device) : m_device(device)
		{
			m_buffer = NULL;
			m_memory = NULL;
			m_cleanedUp = false;
		}
		~Buffer()
		{
			CleanUp();
		}

		
		Buffer(Buffer&& rhs)
		{
			m_buffer = rhs.m_buffer;
			m_memory = rhs.m_memory;
			m_device = rhs.m_device;
			m_cleanedUp = false;

			rhs.m_buffer = NULL;
			rhs.m_memory = NULL;
		}

		void CleanUp()
		{
			if (m_cleanedUp == false)
			{
				if (m_memory != VK_NULL_HANDLE) vkFreeMemory(m_device, m_memory, nullptr);
				if (m_buffer != VK_NULL_HANDLE) vkDestroyBuffer(m_device, m_buffer, nullptr);

				m_cleanedUp = true;
			}
		}

		//----- Rule of 5 Nonsense -----
		Buffer(const Buffer& rhs) = delete;
		Buffer operator= (const Buffer&) = delete;
		Buffer operator= (Buffer&&) = delete;
	};
}

#endif

