#ifndef _SCENE_NODE_H_
#define _SCENE_NODE_H_

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <vector>
#include <memory>
#include <vulkan/vulkan.h>
#include <string>

struct Texture;
struct CubeTexture;
struct Ray;
class RenderObject;
class Shader;
class Model;
class Timer;

class SceneNode
{
private:
	void RefreshChildrenMatrices();

	using ChildList = std::vector<std::unique_ptr<SceneNode>>;
	ChildList		m_children;
	RenderObject*	m_renderObject;
	
	std::string					m_id;
	bool						m_isVisible;
	glm::vec3					m_position;
	glm::vec3					m_rotation;
	glm::vec3					m_scale;
	glm::vec3					m_colour;
	glm::mat4					m_modelMatrix;

public:
	 SceneNode(std::string&& id);
	 SceneNode(std::string&& id, const Model& model, const Texture& texture, Shader& shader, const bool isAnimated = false);
	 SceneNode(std::string&& id, const Model& model, std::vector<const Texture*> textures, Shader& shader);
	 SceneNode(std::string&& id, const Model& model, const CubeTexture& texture, Shader& shader);
	virtual ~SceneNode();

	bool isWater = false;

	//----- Process Functions -----
	void AddChild	(std::unique_ptr<SceneNode>&& child);
	void Update		(const double dt, const uint32_t currentImage);
	void Draw		(const uint32_t currentImage, VkCommandBuffer& buffer, const Timer& timer) const;

	//----- Altering Functions -----
	void ChangePosition	(const glm::vec3& delta);
	void SetPosition	(const glm::vec3& newPos);

	void ChangeScale	(const glm::vec3& delta);
	void SetScale		(const glm::vec3& delta);

	void ChangeRotation	(const glm::vec3& delta);

	void SetVisiblity		(const bool isVisible);
	void ToggleVisibility	();

	void CalculateModelMatrix(const glm::mat4& parentMatrix);

	void UpdateDescriptorSets(const Texture& texture);
	void ChangeTexture(const Texture& texture);

	void AddToImGui();
	SceneNode* AddHierarchyToImGui();

	//---- Getter Functions -----
	const std::string&	Id			() const { return m_id; }
	const glm::vec3&	Position	() const { return m_position; }
	const glm::vec3		Dimensions	() const;
	const glm::mat4&	ModelMatrix	() const;

	const ChildList& Children() const { return m_children; }
	const SceneNode* ChildAtIndex(const uint32_t index) const;
	SceneNode*		 ChildAtIndex(const uint32_t index);

	//Messing around
	void SetColliding		(const bool isColliding);
	bool RayIntersectsAABB	(Ray& ray) const;

};

#endif