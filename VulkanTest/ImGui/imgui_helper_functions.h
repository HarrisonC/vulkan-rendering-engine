#pragma once

#include <string>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace ImGuiHelper
{
	[[maybe_unused]] bool CreateFloatInputBoxWithArrows		(float& n,  const std::string& label, const float step);
	[[maybe_unused]] bool CreateDoubleInputBoxWithArrows	(double& n, const std::string& label, const double step);

	[[maybe_unused]] bool CreateSameLineBoxForVector		(const std::string& id, glm::vec3& v);
	[[maybe_unused]] bool CreateSameLineBoxForVector		(const std::string& id, glm::vec4& v);

	[[maybe_unused]] bool CreateBoxWithArrowsForVector		(glm::vec3& v, const float step);
	[[maybe_unused]] bool CreateBoxWithArrowsForVector		(glm::vec4& v, const float step);

	[[maybe_unused]] bool CreateTextBoxWithArrowsForVector	(const std::string& text, glm::vec3& v, const float step);
	[[maybe_unused]] bool CreateTextBoxWithArrowsForVector	(const std::string& text, glm::vec4& v, const float step);

	[[maybe_unused]] bool CreateSameLineHeaderForVector		(const std::string& text, glm::vec3& v);
	[[maybe_unused]] bool CreateSameLineHeaderForVector		(const std::string& text, glm::vec4& v);
}