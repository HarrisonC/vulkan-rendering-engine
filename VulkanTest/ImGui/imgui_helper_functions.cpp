#pragma once

#include "imgui_helper_functions.h"
#include "imgui.h"

namespace ImGuiHelper
{
	bool CreateFloatInputBoxWithArrows(float& n, const std::string& label, const float step)
	{
		bool valueChanged = false;

		const std::string hiddenLabel{ "##" + label };
		ImGui::Text(label.c_str());
		ImGui::SameLine();
		if (ImGui::ArrowButton(std::string{ hiddenLabel + "##left" }.c_str(), ImGuiDir_Left)) { n -= step; valueChanged = true; };
		ImGui::SameLine();
		valueChanged |= ImGui::InputFloat(hiddenLabel.c_str(), &n, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		if (ImGui::ArrowButton(std::string{ label + "##right" }.c_str(), ImGuiDir_Right)) { n += step; valueChanged = true; };

		return valueChanged;
	}

	bool CreateDoubleInputBoxWithArrows(double& n, const std::string& label, const double step)
	{
		bool valueChanged = false;

		const std::string hiddenLabel{ "##" + label };
		ImGui::Text(label.c_str());
		ImGui::SameLine();
		if (ImGui::ArrowButton(std::string{ hiddenLabel + "##left" }.c_str(), ImGuiDir_Left)) { n -= step; valueChanged = true; };
		ImGui::SameLine();
		valueChanged |= ImGui::InputDouble(hiddenLabel.c_str(), &n, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		if (ImGui::ArrowButton(std::string{ label + "##right" }.c_str(), ImGuiDir_Right)) { n += step; valueChanged = true; };

		return valueChanged;
	}

	bool CreateSameLineBoxForVector(const std::string& id, glm::vec3& v)
	{
		bool valueChanged = false;

		ImGui::PushItemWidth(64.0f);
		valueChanged |= ImGui::InputFloat(std::string("X##" + id).c_str(), &v.x, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		valueChanged |= ImGui::InputFloat(std::string("Y##" + id).c_str(), &v.y, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		valueChanged |= ImGui::InputFloat(std::string("Z##" + id).c_str(), &v.z, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::PopItemWidth();

		return valueChanged;
	}

	bool CreateSameLineBoxForVector(const std::string& id, glm::vec4& v)
	{
		bool valueChanged = false;

		ImGui::PushItemWidth(64.0f);
		valueChanged |= ImGui::InputFloat(std::string("X##" + id).c_str(), &v.x, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		valueChanged |= ImGui::InputFloat(std::string("Y##" + id).c_str(), &v.y, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		valueChanged |= ImGui::InputFloat(std::string("Z##" + id).c_str(), &v.z, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::SameLine();
		valueChanged |= ImGui::InputFloat(std::string("W##" + id).c_str(), &v.w, 0.0f, 0.0f, "%.3f", ImGuiInputTextFlags_EnterReturnsTrue);
		ImGui::PopItemWidth();

		return valueChanged;
	}

	bool CreateBoxWithArrowsForVector(glm::vec3& v, const float step)
	{
		bool valueChanged = false;

		valueChanged |= CreateFloatInputBoxWithArrows(v.x, "X", step);
		valueChanged |= CreateFloatInputBoxWithArrows(v.y, "Y", step);
		valueChanged |= CreateFloatInputBoxWithArrows(v.z, "Z", step);

		return true;
	}

	bool CreateBoxWithArrowsForVector(glm::vec4& v, const float step)
	{
		bool valueChanged = false;

		valueChanged |= CreateFloatInputBoxWithArrows(v.x, "X", step);
		valueChanged |= CreateFloatInputBoxWithArrows(v.y, "Y", step);
		valueChanged |= CreateFloatInputBoxWithArrows(v.z, "Z", step);
		valueChanged |= CreateFloatInputBoxWithArrows(v.w, "W", step);

		return valueChanged;
	}

	bool CreateTextBoxWithArrowsForVector(const std::string& text, glm::vec3& v, const float step)
	{
		bool valueChanged = false;

		if (ImGui::TreeNode(text.c_str()))
		{
			valueChanged |= CreateBoxWithArrowsForVector(v, step);
			ImGui::TreePop();
		}

		return valueChanged;
	}

	bool CreateTextBoxWithArrowsForVector(const std::string& text, glm::vec4& v, const float step)
	{
		bool valueChanged = false;

		if (ImGui::TreeNode(text.c_str()))
		{
			valueChanged |= CreateBoxWithArrowsForVector(v, step);
			ImGui::TreePop();
		}

		return valueChanged;
	}

	bool CreateSameLineHeaderForVector(const std::string& text, glm::vec3& v)
	{
		bool valueChanged = false;

		if (ImGui::TreeNode(text.c_str()))
		{
			valueChanged |= CreateSameLineBoxForVector(text, v);
			ImGui::TreePop();
		}

		return valueChanged;
	}

	bool CreateSameLineHeaderForVector(const std::string& text, glm::vec4& v)
	{
		bool valueChanged = false;

		if (ImGui::TreeNode(text.c_str()))
		{
			valueChanged |= CreateSameLineBoxForVector(text, v);
			ImGui::TreePop();
		}

		return valueChanged;
	}
}